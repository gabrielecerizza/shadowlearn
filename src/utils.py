import json
import logging
import os
import time
import random

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from scipy.stats import norm
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

import src.metrics

random_state = 42


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class PrintAttributesMixin:
    def __attrnames(self):
        result = ''
        for attr in sorted(self.__dict__):
            result += '\t%s=%s\n' % (attr, self.__dict__[attr])
        return result

    def __str__(self):
        return '%s: %s' % (
            self.__class__.__name__,
            self.__attrnames())


def check_fit_parameters(**params):
    def on_decorator(func):

        def on_call(self, X, y):  # must be named X and y to pass estimator_checks in fit
            for (param, (low, high)) in params.items():
                if param not in dir(self):
                    raise ValueError(f'[{self.__class__.__name__}]: parameter {param} not found')
                else:
                    value = getattr(self, param)
                    if (low is not None and value < low) or (high is not None and value > high):
                        error_msg = f'[{self.__class__.__name__}]: parameter {param} '
                        if low is not None and high is not None:
                            error_msg += f'should be within {low} and {high}. Got {value} instead.'
                        elif low is not None:
                            error_msg += f'should be greater than {low}. Got {value} instead.'
                        else:
                            error_msg += f'should be lesser than {high}. Got {value} instead.'
                        raise ValueError(error_msg)
            return func(self, X, y)

        return on_call

    return on_decorator


def create_logfile(log_filename, starting_log, log=True):
    if log:
        # log_dir = os.path.join(os.getcwd(), 'logs')
        log_dir = 'F:\\Copia HD G\\Universita\\UNIMI\\Th\\PyCharm\\shadowedSetLearn\\logs'
        log_filename = os.path.join(log_dir, time.strftime('%Y-%m-%dT%H%M%S') + '_'
                                    + log_filename + '.log')
        logging.basicConfig(filename=log_filename, level=logging.INFO,
                            format='%(asctime)s:%(levelname)s:%(message)s')
        logging.info(starting_log)


def log_last_execution(closing_log, log=True):
    last = 'Last complete execution: ' + time.strftime('%Y-%m-%d %H:%M:%S')
    print(last)
    if log:
        logging.info(last)
        logging.info(closing_log)


def add_random_dots(X, y, blue_anomaly_num, red_anomaly_num):
    random.seed(random_state)
    np.random.seed(random_state)
    red_mu, red_sigma = [-0.1, -0.25], [0.05, 0.05]
    red_norm = norm(red_mu, red_sigma)
    blue_mu, blue_sigma = [0.8, 0.5], [0.05, 0.05]
    blue_norm = norm(blue_mu, blue_sigma)
    if red_anomaly_num is not None:
        X = np.concatenate((X, red_norm.rvs([red_anomaly_num, 2])))
        y = np.concatenate((y, [0] * red_anomaly_num))
    if blue_anomaly_num is not None:
        X = np.concatenate((X, blue_norm.rvs([blue_anomaly_num, 2])))
        y = np.concatenate((y, [1] * blue_anomaly_num))
    return X, y


def get_random_dots(blue_anomaly_num, red_anomaly_num):
    random.seed(random_state)
    np.random.seed(random_state)
    red_mu, red_sigma = [-0.1, -0.25], [0.05, 0.05]
    red_norm = norm(red_mu, red_sigma)
    blue_mu, blue_sigma = [0.8, 0.5], [0.05, 0.05]
    blue_norm = norm(blue_mu, blue_sigma)
    X = np.array([])
    y = np.array([])
    if red_anomaly_num is not None:
        reds = red_norm.rvs([red_anomaly_num, 2])
        X = np.concatenate((X, reds)) if X.size else reds
        y = np.concatenate((y, [0] * red_anomaly_num))
    if blue_anomaly_num is not None:
        blues = blue_norm.rvs([blue_anomaly_num, 2])
        X = np.concatenate((X, blues)) if X.size else blues
        y = np.concatenate((y, [1] * blue_anomaly_num))
    return X, y


def get_trained_estimator(dataset_loader, estimator):
    X_train, y_train, _, _, _, _ = get_train_test_split(dataset_loader)

    return estimator.fit(X_train, y_train)


def get_train_test_split(dataset_loader, n_splits=1, test_size=0.3):
    try:
        X, y, data_labels, target, _ = dataset_loader()
    except:
        X, y, data_labels, target = dataset_loader()
    splitter = StratifiedShuffleSplit(n_splits=n_splits, test_size=test_size, random_state=random_state)
    for train_index, test_index in splitter.split(X, data_labels):
        X_train = X[train_index]
        y_train = y[train_index]
        label_train = data_labels[train_index]
        X_test = X[test_index]
        y_test = y[test_index]
        label_test = data_labels[test_index]
    return X, X_train, y_train, X_test, y_test, label_train, label_test


def append_anomalies_and_apply_target(X, y, label_train, target, red_anomaly_num=None, blue_anomaly_num=None):
    X_anomalies, y_anomalies = get_random_dots(blue_anomaly_num=blue_anomaly_num, red_anomaly_num=red_anomaly_num)
    X = np.concatenate((X, X_anomalies))
    label_train = np.concatenate((label_train, ['red'] * red_anomaly_num))
    label_train = np.concatenate((label_train, ['blue'] * blue_anomaly_num))

    if target == 'blue':
        y_anomalies = pd.Series(y_anomalies).apply(lambda x: x if x == 1 else -1)
    elif target == 'red':
        y_anomalies = pd.Series(y_anomalies).apply(lambda x: 1 if x == 0 else -1)

    y = np.concatenate((y, y_anomalies))

    return X, np.array(y), label_train


def get_plot_dfs(dataset_loader, estimator, metric, simplified=True, anomalies=False, red_anomaly_num=None,
                 blue_anomaly_num=None, scaled=False, dim=False):
    try:
        _, y, data_labels, target, _ = dataset_loader()
    except:
        _, y, data_labels, target = dataset_loader()
    X, X_train, y_train, X_test, y_test, label_train, label_test = get_train_test_split(dataset_loader)

    if anomalies:
        X_train, y_train, label_train = append_anomalies_and_apply_target(X_train, y_train, label_train, target,
                                                                          red_anomaly_num=red_anomaly_num,
                                                                          blue_anomaly_num=blue_anomaly_num)

    if scaled is not False:
        scaler = StandardScaler()
        scaler.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)
        X = scaler.transform(X)

    if dim is not False:
        pca = PCA(n_components=dim)
        pca.fit(X_train)
        X_train = pca.transform(X_train)
        X_test = pca.transform(X_test)
        X = pca.transform(X)
    estimator.fit(X_train, y_train)

    X0, X1 = X[:, 0], X[:, 1]
    if simplified:
        xm = np.linspace(min(X0) - 1, max(X0) + 1, 150)
        ym = np.linspace(min(X1) - 1, max(X1) + 1, 150)
    else:
        x_min, x_max = min(X0) - 1, max(X0) + 1
        y_min, y_max = min(X1) - 1, max(X1) + 1
        xm = np.concatenate((np.arange(x_min, x_max, .02), X0))
        ym = np.concatenate((np.arange(y_min, y_max, .02), X1))
    xx, yy = np.meshgrid(xm, ym)
    Zs = np.array(estimator.predict_trace(np.c_[xx.ravel(), yy.ravel()], True))
    Z = Zs.reshape(xx.shape)
    coord_dict = {'x': xx.ravel(), 'y': yy.ravel(), 'Z': Zs}
    coord_df = pd.DataFrame(coord_dict)
    train_pred = estimator.predict(X_train)
    d_train = {'x': X_train[:, 0], 'y': X_train[:, 1], 'label': label_train,
               'pred': train_pred}
    df_train = pd.DataFrame(d_train)
    test_pred = estimator.predict(X_test)
    d_test = {'x': X_test[:, 0], 'y': X_test[:, 1], 'label': label_test,
              'pred': test_pred}
    df_test = pd.DataFrame(d_test)

    if metric == 'conservative':
        score = src.metrics.conservative_score(y_test, test_pred)
    elif metric == 'non-conservative':
        score = src.metrics.non_conservative_score(y_test, test_pred)
    elif metric == 'svc-shadowed':
        score = src.metrics.svc_shadowed_score(estimator, X, y)
    elif metric == 'mean distance':
        score = src.metrics.mean_distance_score(X, data_labels)(estimator, X, y)
    elif metric == 'min distance':
        score = src.metrics.min_distance_score(X, data_labels)(estimator, X, y)
    elif metric == 'mean or min distance':
        score = src.metrics.mean_or_min_distance_score(X, data_labels)(estimator, X, y)
    elif metric == 'freq distance':
        score = src.metrics.freq_mean_distance_score(X, data_labels)(estimator, X, y)
    elif metric == 'silhouette distance':
        score = src.metrics.silhouette_distance_score(X, data_labels)(estimator, X, y)
    elif metric == 'entropy distance':
        score = src.metrics.entropy_distance_score(X, data_labels)(estimator, X, y)
    elif metric == 'homogeneity':
        score = src.metrics.homogeneity_score(y_test, test_pred)

    return df_test, df_train, coord_df, score


def get_test_scoring(metric):
    if metric == 'conservative':
        return src.metrics.conservative_score
    elif metric == 'non-conservative':
        return src.metrics.non_conservative_score
    else:
        raise ValueError('get_test_scoring: unknown metric')


def load_stored_csv(filename, directory='\\csv\\', separate_targets=False):
    filename = os.getcwd() + directory + filename
    filename += '_separate.csv' if separate_targets else '.csv'
    return pd.read_csv(filename)


def intersection_3(*sets):
    return intersection_n(*sets, n=3)


def intersection_n(*sets, n):
    res = set()
    for mset in sets:
        for el in mset:
            count = 0
            for inner_set in sets:
                if el in inner_set:
                    count += 1
                if count == n:
                    res.add(el)
                    break
    return res
