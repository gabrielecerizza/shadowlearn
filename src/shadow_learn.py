import logging

import numpy as np
from scipy.spatial.distance import euclidean
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.cluster import SpectralClustering
from sklearn.utils.validation import check_array, check_is_fitted, check_X_y
from tqdm import tqdm

from .kernel import GaussianKernel, Kernel
from .solver import GurobiTwoPhasesSolver, GurobiSinglePhaseSolver, GurobiTwoPhasesSingleCSolver, \
    GurobiTwoPhasesDoubleConstraintSolver, GurobiTwoPhasesTwoCoresSolver, TensorFlowTwoPhasesSolver, \
    CPLEXTwoPhasesSolver, CPLEXSinglePhaseSolver, GurobiTwoPhasesDualSolver, GurobiTwoPhasesShadowSolver, \
    GurobiTwoPhasesDoubleConstraintShadowSolver, GurobiTwoPhasesTwoCoresShadowSolver
from .utils import check_fit_parameters

from wilf2018.shadowlearn import shadowed_learn
from wilf2018.shadowlearn.kernel import GaussianKernel as Wilf2018GaussianKernel


class ShadowedSetEstimator(ClassifierMixin, BaseEstimator):
    solver_types = ['gurobi-two-phases', 'gurobi-single-phase', 'gurobi-two-phases-single-C',
                    'gurobi-two-phases-double-constraint', 'gurobi-two-phases-two-cores', 'tensorflow-two-phases',
                    'cplex-two-phases', 'cplex-single-phase', 'gurobi-two-phases-dual', 'gurobi-two-phases-shadow',
                    'gurobi-two-phases-double-constraint-shadow', 'gurobi-two-phases-two-cores-shadow']
    kernel_types = ['gaussian']

    def __init__(self, *, C0=0.1, C1=1, C=1, psi=0.2, k='gaussian', sigma=0.17, solver='gurobi-two-phases', tol=1e-6):
        self.C0 = C0
        self.C1 = C1
        self.C = C
        self.psi = psi
        self.k = k
        self.sigma = sigma
        self.solver = solver
        self.tol = tol

    # y_shadow has three values
    # TODO: need to add full_X etc. to check_fit_parameters
    # @check_fit_parameters(C0=(0, None), C1=(0, None), sigma=(0, None), tol=(0, None))
    def fit(self, X, y, full_X=(None, None), full_y=(None, None), full_y_shadow=(None, None), cv_num=None, random_state=42):
        X, y = check_X_y(X, y)
        self.X_train_ = X
        self.y_train_ = y
        self.full_y_shadow_, _ = full_y_shadow
        self.full_X_, _ = full_X
        self.full_y_, _ = full_y
        X_c = X[y == 1]  # core
        self.X_c_ = X_c
        X_nc = X[y == -1]  # exclusion
        self.n_features_in_ = X.shape[1]
        X_s = X[y == 0]  # shadow
        self.cv_num_ = cv_num
        self.random_state_ = random_state

        if isinstance(self.k, str):
            if self.k == 'gaussian':
                self.k_ = GaussianKernel(self.sigma)  # use k_ ?
            else:
                raise ValueError(
                    f"When 'k' is a string, it should be one of {ShadowedSetEstimator.kernel_types}."
                    f"Got '{self.k}' instead."
                )
        elif not isinstance(self.k, Kernel):
            raise ValueError(
                f"When 'k' is not a string, it should be a sub-class of Kernel"
            )

        if isinstance(self.solver, str):
            if self.solver == 'gurobi-two-phases':
                self.solver_ = GurobiTwoPhasesSolver(self.C0, self.C1, self.k_, self.tol, X_c, X_nc)
            elif self.solver == 'gurobi-two-phases-single-C':
                self.solver_ = GurobiTwoPhasesSingleCSolver(self.C, self.k_, self.tol, X_c, X_nc)
            elif self.solver == 'gurobi-single-phase':
                self.solver_ = GurobiSinglePhaseSolver(self.C, self.psi, self.k_, self.tol, X_c, X_nc)
            elif self.solver == 'gurobi-two-phases-double-constraint':
                self.solver_ = GurobiTwoPhasesDoubleConstraintSolver(self.C0, self.C1, self.k_, self.tol, X_c,
                                                                     X_nc, X_s)
            elif self.solver == 'gurobi-two-phases-two-cores':
                self.solver_ = GurobiTwoPhasesTwoCoresSolver(self.C0, self.C1, self.k_, self.tol, X_c, X_nc)
            elif self.solver == 'tensorflow-two-phases':
                # TODO: add parameters for optimizer etc.
                self.solver_ = TensorFlowTwoPhasesSolver(self.C0, self.C1, self.k_, self.tol, X_c, X_nc)
            elif self.solver == 'cplex-two-phases':
                self.solver_ = CPLEXTwoPhasesSolver(self.C0, self.C1, self.k_, self.tol, X_c, X_nc)
            elif self.solver == 'cplex-single-phase':
                self.solver_ = CPLEXSinglePhaseSolver(self.C, self.psi, self.k_, self.tol, X_c, X_nc)
            elif self.solver == 'gurobi-two-phases-dual':
                self.solver_ = GurobiTwoPhasesDualSolver(self.C0, self.C1, self.k_, self.tol, X_c, X_nc)
            elif self.solver == 'gurobi-two-phases-shadow':
                self.solver_ = GurobiTwoPhasesShadowSolver(self.C0, self.C1, self.k_, self.tol,
                                                           X_c=X_c, X_nc=X_nc, X_s=X_s)
            elif self.solver == 'gurobi-two-phases-double-constraint-shadow':
                self.solver_ = GurobiTwoPhasesDoubleConstraintShadowSolver(self.C0, self.C1, self.k_, self.tol,
                                                                           X_c=X_c, X_nc=X_nc, X_s=X_s)
            elif self.solver == 'gurobi-two-phases-two-cores-shadow':
                self.solver_ = GurobiTwoPhasesTwoCoresShadowSolver(self.C0, self.C1, self.k_, self.tol,
                                                                   X_c=X_c, X_nc=X_nc, X_s=X_s)
            else:
                raise ValueError(
                    f"When 'solver' is a string, it should be one of {ShadowedSetEstimator.solver_types}."
                    f"Got '{self.solver}' instead."
                )
        else:
            # check type?
            self.solver_ = self.solver

        self.estimation_function_ = self.solver_.solve()

        return self

    def predict_trace(self, X, trace=False):
        check_is_fitted(self)
        X = check_array(X)

        if trace:
            result = []
            with tqdm(total=len(X), ascii=True) as pbar:
                for x in X:
                    result.append(self.estimation_function_(x))
                    pbar.update(1)
            return np.array(result)
        else:
            return np.array([self.estimation_function_(x) for x in X])

    def predict(self, X):

        check_is_fitted(self)
        X = check_array(X)

        return np.array([self.estimation_function_(x) for x in X])

    def my_set_params(self, params_dict):
        for param, value in params_dict.items():
            self.__setattr__(param, value)

    def get_plot_title(self):
        # check_is_fitted(self)
        return str(self.solver_)

    def get_plotly_parameters(self):
        return '$' + self.solver_.get_plotly_string() + '$'

    def get_plotly_solver_name(self):
        return '$' + r'\text{' + self.get_solver_name() + r'}$'

    def get_solver_type(self):
        if isinstance(self.solver, str):
            return self.solver
        else:
            raise TypeError('get_solver_type can be called only when solver attribute is a string')

    def get_solver_name(self):
        if self.solver == 'gurobi-two-phases':
            # return 'GurobiTwoPhases'
            return 'GurobiDueFasi'
        elif self.solver == 'gurobi-two-phases-single-C':
            # return 'GurobiTwoPhasesSingleC'
            return 'GurobiDueFasiSingolaC'
        elif self.solver == 'gurobi-single-phase':
            # return 'GurobiSinglePhase'
            return 'GurobiSingolaFase'
        elif self.solver == 'gurobi-two-phases-double-constraint':
            # return 'GurobiTwoPhasesDoubleConstraint'
            return 'GurobiDoppioVincolo'
        elif self.solver == 'gurobi-two-phases-two-cores':
            # return 'GurobiTwoPhasesTwoCores'
            return 'GurobiDueCore'
        elif self.solver == 'tensorflow-two-phases':
            # return 'TensorFlowTwoPhases'
            return 'TensorFlowDueFasi'
        elif self.solver == 'cplex-two-phases':
            # return 'CPLEXTwoPhases'
            return 'CPLEXDueFasi'
        elif self.solver == 'cplex-single-phase':
            # return 'CPLEXSinglePhase'
            return 'CPLEXSingolaFase'
        elif self.solver == 'gurobi-two-phases-dual':
            return 'GurobiTwoPhasesDual'
        elif self.solver == 'gurobi-two-phases-shadow':
            return 'GurobiTwoPhasesShadow'
        elif self.solver == 'gurobi-two-phases-double-constraint-shadow':
            return 'GurobiTwoPhasesDoubleConstraintShadow'
        elif self.solver == 'gurobi-two-phases-two-cores-shadow':
            return 'GurobiTwoPhasesTwoCoresShadow'
        else:
            return self.solver.__class__.__name__


class WILF2018Adapter(BaseEstimator):

    def __init__(self, *, c=1.0, d=1.0, phi=1.0, sigma=0.2, adjustment=0):
        self.c = c
        self.d = d
        self.phi = phi
        self.sigma = sigma
        self.adjustment = adjustment

    def fit(self, X, y, full_X=(None, None), full_y=(None, None), full_y_shadow=(None, None), cv_num=None, random_state=42):
        X, y = check_X_y(X, y)
        self.X_train_ = X
        self.y_train_ = y
        self.full_y_shadow_, _ = full_y_shadow
        self.full_X_, _ = full_X
        self.full_y_, _ = full_y
        self.n_features_in_ = X.shape[1]
        self.cv_num_ = cv_num
        self.random_state_ = random_state

        self.k = Wilf2018GaussianKernel(self.sigma)
        mu = self.to_membership_values(y)
        fuzzy_membership, train_err, shadowed_membership = shadowed_learn(X, mu, self.c, self.d, self.phi, self.k, self.adjustment)
        if train_err == np.inf:
            self.shadowed_membership_ = lambda x: 2
        else:
            self.shadowed_membership_ = shadowed_membership

        return self

    # changed
    def to_membership_values(self, y):
        return [1 if label == 1 else 0 for label in y]

    def estimation_function(self, x):
        pred = self.shadowed_membership_(x)
        if pred == 0:
            return -1
        elif pred == 0.5:
            return 0
        else:
            return pred

    def predict(self, X):

        check_is_fitted(self)
        X = check_array(X)
        return np.array([self.estimation_function(x) for x in X])

    def predict_trace(self, X, trace=False):
        check_is_fitted(self)
        X = check_array(X)

        if trace:
            result = []
            with tqdm(total=len(X), ascii=True) as pbar:
                for x in X:
                    result.append(self.estimation_function(x))
                    pbar.update(1)
            return np.array(result)
        else:
            return np.array([self.estimation_function(x) for x in X])

    def get_solver_name(self):
        return 'WILF2018ShadowedLearn'

    def get_solver_type(self):
        return self

    def get_plot_title(self):
        return str(self.k) + ', c = ' + str(self.c) + ', d = ' + str(self.d) + ', phi = ' + str(self.phi)

    def my_set_params(self, params_dict):
        for param, value in params_dict.items():
            self.__setattr__(param, value)


class CompoundedShadowedSetEstimator(BaseEstimator):
    sse_types = ['standard']

    def __init__(self, *, sse1='standard', sse2='standard', C_sse1=1, C_sse2=1, C_both=None, k='gaussian', sigma_sse1=0.17,
                 sigma_sse2=0.17, tol_sse1=1e-6, tol_sse2=1e-6):
        self.sse1 = sse1
        self.sse2 = sse2
        # TODO: should remove sse1_ and sse2_ here
        self.sse1_ = sse1
        self.sse2_ = sse2
        self.C_sse1 = C_sse1
        self.C_sse2 = C_sse2
        self.C_both = C_both
        self.k = k
        self.sigma_sse1 = sigma_sse1
        self.sigma_sse2 = sigma_sse2
        self.tol_sse1 = tol_sse1
        self.tol_sse2 = tol_sse2

    def fit(self, X, y, **kwargs):
        X, y = check_X_y(X, y)

        if 'y1' in kwargs:
            _, y1 = check_X_y(X, kwargs['y1'])
        else:
            # logging.warning('Called fit on CompoundedShadowedSetEstimator without y1')
            # y1 = y
            raise ValueError('Called fit on CompoundedShadowedSetEstimator without y1')

        if 'y2' in kwargs:
            _, y2 = check_X_y(X, kwargs['y2'])
        else:
            # logging.warning('Called fit on CompoundedShadowedSetEstimator without y2')
            # y2 = y
            raise ValueError('Called fit on CompoundedShadowedSetEstimator without y2')

        if self.C_both is not None:
            self.C_sse1_ = self.C_both
            self.C_sse2_ = self.C_both
        else:
            self.C_sse1_ = self.C_sse1
            self.C_sse2_ = self.C_sse2

        self.n_features_in_ = X.shape[1]

        if self.sse1 == 'standard':
            self.sse1_ = ShadowedSetEstimator(C=self.C_sse1_, k=self.k, sigma=self.sigma_sse1,
                                              solver='gurobi-two-phases-single-C', tol=self.tol_sse1)
        else:
            self.sse1_ = self.sse1

        if self.sse2 == 'standard':
            self.sse2_ = ShadowedSetEstimator(C=self.C_sse2_, k=self.k, sigma=self.sigma_sse2,
                                              solver='gurobi-two-phases-single-C', tol=self.tol_sse2)
        else:
            self.sse2_ = self.sse2

        self.sse1_.fit(X, y1)
        self.sse2_.fit(X, y2)

        return self

    def predict1(self, X):
        return self.sse1_.predict(X)

    def predict2(self, X):
        return self.sse2_.predict(X)

    def _estimation_function(self, x):
        p1 = self.sse1_.estimation_function_(x)
        p2 = self.sse2_.estimation_function_(x)

        if p1 == 1 and p2 == -1: return 1  # 1 for surely virginica
        if p1 == -1 and p2 == 1: return 2  # 2 for surely versicolor
        if p1 == -1 and p2 == -1: return -1  # -1 for surely not virginica or versicolor

        if p1 == 1 and p2 == 0: return 1  # prioritize core from the respective estimator
        if p1 == 0 and p2 == 1: return 2

        if p1 == 0 and p2 == 0: return 0  # 0 for maybe virginica or versicolor, 0 if both are uncertain
        if p1 == 1 and p2 == 1: return 0  # 0 if both are core

        if p1 == 0 and p2 == -1: return 1  # if one is maybe core1 and the other is not core2, core1
        if p1 == -1 and p2 == 0: return 2

    def predict(self, X):
        X = check_array(X)

        return np.array([self._estimation_function(x) for x in X])

    def predict_trace(self, X, trace=False):
        check_is_fitted(self)
        X = check_array(X)

        if trace:
            result = []
            with tqdm(total=len(X), ascii=True) as pbar:
                for x in X:
                    result.append(self._estimation_function(x))
                    pbar.update(1)
            return np.array(result)
        else:
            return np.array([self._estimation_function(x) for x in X])

    def predict_labels(self, X):
        X = check_array(X)

        res = []
        for x in X:
            p1 = self.sse1_.estimation_function_(x)
            p1 = 'virginica' if p1 == 1 else 'not virginica' if p1 == -1 else 'maybe virginica'
            p2 = self.sse2_.estimation_function_(x)
            p2 = 'versicolor' if p2 == 1 else 'not versicolor' if p2 == -1 else 'maybe versicolor'

            res.append((p1, p2))

        return res

    def get_plot_title(self):
        return r'$C_{both} = ' + f'{self.C_both:.2f}' + r'$, $\sigma_{sse1} = ' + f'{self.sigma_sse1:.2f}' + \
               r'$, $\sigma_{sse2} = ' + f'{self.sigma_sse2:.2f}' + '$'

    def get_plotly_parameters(self):
        return '$' + r'C_{both} = ' + f'{self.C_both:.2f}' + r', \sigma_{sse1} = ' + f'{self.sigma_sse1:.2f}' \
               + r', \sigma_{sse2} = ' + f'{self.sigma_sse2:.2f}' + '$'

    def get_plotly_solver_name(self):
        return '$' + r'\text{' + 'CompoundedShadowedSetEstimator' + r'}$'

    def get_solver_type(self):
        return self

    def my_set_params(self, params_dict):
        for param, value in params_dict.items():
            self.__setattr__(param, value)


class FCM:

    def __init__(self, *, c=3, m=2, epsilon=0.005, n_iter=1000):
        self.c = c
        self.m = m
        self.epsilon = epsilon
        self.n_iter = n_iter

    def fit(self, X, y, full_X=(None, None), full_y=(None, None), full_y_shadow=(None, None), cv_num=None,
            random_state=42):
        X, y = check_X_y(X, y)
        self.X_train_ = X
        self.y_train_ = y
        self.full_y_shadow_, _ = full_y_shadow
        self.full_X_, _ = full_X
        self.full_y_, _ = full_y
        self.n_features_in_ = X.shape[1]
        self.cv_num_ = cv_num
        self.random_state_ = random_state

        np.random.seed(seed=self.random_state_)
        mu = np.random.rand(self.c, X.shape[0])
        mu = mu / np.sum(mu, axis=0, keepdims=1)

        for l in range(self.n_iter):
            centers = self.find_centers(X, mu, self.m, self.c)
            new_mu = self.update_mu(X, self.m, self.c, centers)
            if np.linalg.norm(mu - new_mu) < self.epsilon:
                break
            mu = new_mu

        self.mu_ = new_mu
        self.centers_ = centers

        return self

    def distance(self, center, x):
        return euclidean(center, x)

    def find_centers(self, X, mu, m, c):
        v = np.zeros(shape=(c, X.shape[1]))
        for i in range(c):
            for j in range(X.shape[1]):
                sum1 = 0
                for k in range(X.shape[0]):
                    sum1 += (mu[i][k] ** m) * X[k][j]

                sum2 = 0
                for k in range(X.shape[0]):
                    sum2 += mu[i][k] ** m

                v[i][j] = sum1 / sum2
        return np.array(v)

    def update_mu(self, X, m, c, centers):
        distances = np.zeros(shape=(c, X.shape[0]))
        for i in range(c):
            for k in range(len(X)):
                distances[i][k] = self.distance(centers[i], X[k])
        distances = np.array(distances)

        new_mu = np.zeros(shape=(c, X.shape[0]))
        for i in range(c):
            for k in range(len(X)):
                new_mu[i][k] = 1 / sum([(distances[i][k] / distances[j][k]) ** (2 / (m - 1))
                                        for j in range(c)])

        return new_mu

    def predict(self, X):
        X = check_array(X)

        np.random.seed(seed=self.random_state_)
        mu = np.random.rand(self.c, X.shape[0])
        mu = mu / np.sum(mu, axis=0, keepdims=1)

        for l in range(self.n_iter):
            new_mu = self.update_mu(X, self.m, self.c, self.centers_)
            if np.linalg.norm(mu - new_mu) < self.epsilon:
                break
            mu = new_mu

        return new_mu, self.centers_


class SCM(FCM):

    def __init__(self, *, c=3, m=2, epsilon=0.005, n_iter=1000):
        FCM.__init__(self, c=c, m=m, epsilon=epsilon, n_iter=n_iter)

    def fit(self, X, y, full_X=(None, None), full_y=(None, None), full_y_shadow=(None, None), cv_num=None,
            random_state=42):
        X, y = check_X_y(X, y)
        self.X_train_ = X
        self.y_train_ = y
        self.full_y_shadow_, _ = full_y_shadow
        self.full_X_, _ = full_X
        self.full_y_, _ = full_y
        self.n_features_in_ = X.shape[1]
        self.cv_num_ = cv_num
        self.random_state_ = random_state

        np.random.seed(seed=self.random_state_)
        mu = np.random.rand(self.c, X.shape[0])
        mu = mu / np.sum(mu, axis=0, keepdims=1)
        centers = self.find_centers(X, mu, self.m, self.c)

        for l in range(self.n_iter):
            new_mu = self.update_mu(X, self.m, self.c, centers)
            lambdas = self.compute_thresholds(mu, self.c)
            centers = self.update_means(X, new_mu, self.c, lambdas, self.m)
            if np.linalg.norm(mu - new_mu) < self.epsilon:
                break
            mu = new_mu

        self.mu_ = new_mu
        self.centers_ = centers
        self.lambdas_ = lambdas

        return self

    def compute_thresholds(self, mu, c):
        lambdas = []
        for i in range(c):
            mu_i = mu[i]
            # il / 2 è sia su min che su max, v. articoli Pedrycz
            feasible_lambdas = np.linspace(np.min(mu_i), (np.min(mu_i) + np.max(mu_i)) / 2, 100)
            min_O = np.inf
            lambda_i = np.inf
            for feasible_lambda in feasible_lambdas:
                sum_less = sum([mu_ik for mu_ik in mu_i if mu_ik <= feasible_lambda])
                max_mu_i = max(mu_i)
                sum_greater = sum(
                    [max_mu_i - feasible_lambda for mu_ik in mu_i if mu_ik >= (max_mu_i - feasible_lambda)])
                res = abs(sum_less + sum_greater - len(
                    [1 for mu_ik in mu_i if feasible_lambda < mu_ik < (max_mu_i - feasible_lambda)]))
                if res < min_O:
                    min_O = res
                    lambda_i = feasible_lambda
            lambdas.append(lambda_i)
        return lambdas

    def update_means(self, X, mu, c, lambdas, m):
        v = np.zeros(shape=(c, X.shape[1]))
        for i in range(c):
            max_mu_i = max(mu[i])

            for j in range(X.shape[1]):
                sum1 = np.array([X[k][j] for k, mu_ik in enumerate(mu[i]) if mu_ik >= (max_mu_i - lambdas[i])]).sum()
                sum2 = np.array([X[k][j] * (mu_ik ** m)
                                 for k, mu_ik in enumerate(mu[i]) if
                                 lambdas[i] < mu_ik < (max_mu_i - lambdas[i])]).sum()
                sum3 = np.array([X[k][j] * (mu_ik ** (m ** m))
                                 for k, mu_ik in enumerate(mu[i]) if mu_ik <= lambdas[i]]).sum()
                phi = len([1 for mu_ik in mu[i] if mu_ik >= (max_mu_i - lambdas[i])])
                eta = np.array([(mu_ik ** m)
                                for k, mu_ik in enumerate(mu[i]) if lambdas[i] < mu_ik < (max_mu_i - lambdas[i])]).sum()
                psi = np.array([(mu_ik ** (m ** m))
                                for k, mu_ik in enumerate(mu[i]) if mu_ik <= lambdas[i]]).sum()
                v[i][j] = (sum1 + sum2 + sum3) / (phi + eta + psi)

        return v

    def predict(self, X):
        X = check_array(X)

        np.random.seed(seed=self.random_state_)
        mu = np.random.rand(self.c, X.shape[0])
        mu = mu / np.sum(mu, axis=0, keepdims=1)

        for l in range(self.n_iter):
            new_mu = self.update_mu(X, self.m, self.c, self.centers_)
            lambdas = self.compute_thresholds(new_mu, self.c)
            if np.linalg.norm(mu - new_mu) < self.epsilon:
                break
            mu = new_mu

        return new_mu, self.centers_, lambdas

    def get_plotly_solver_name(self):
        return 'SCM'


class SRFCM(SCM):

    def __init__(self, *, c=3, m=2, epsilon=0.005, n_iter=1000, wl=0.95, wb=0.05):
        FCM.__init__(self, c=c, m=m, epsilon=epsilon, n_iter=n_iter)
        self.wl = wl
        self.wb = wb

    def fit(self, X, y, full_X=(None, None), full_y=(None, None), full_y_shadow=(None, None), cv_num=None,
            random_state=42):
        X, y = check_X_y(X, y)
        self.X_train_ = X
        self.y_train_ = y
        self.full_y_shadow_, _ = full_y_shadow
        self.full_X_, _ = full_X
        self.full_y_, _ = full_y
        self.n_features_in_ = X.shape[1]
        self.cv_num_ = cv_num
        self.random_state_ = random_state

        np.random.seed(seed=self.random_state_)
        mu = np.random.rand(self.c, X.shape[0])
        mu = mu / np.sum(mu, axis=0, keepdims=1)
        centers = self.find_centers(X, mu, self.m, self.c)

        for l in range(self.n_iter):
            alphas = self.compute_thresholds(mu, self.c)
            lower_bounds, boundary_regions = self.compute_bounds(X, mu, self.c, alphas)
            centers = self.compute_prototypes(X, mu, self.c, self.m, lower_bounds, boundary_regions, self.wl, self.wb)
            new_mu = self.update_mu(X, self.m, self.c, centers)
            if np.linalg.norm(mu - new_mu) < self.epsilon:
                break
            mu = new_mu

        self.mu_ = new_mu
        self.centers_ = centers
        self.alphas_ = alphas

        return self

    def compute_bounds(self, X, mu, c, alphas):
        lower_bounds = []
        boundary_regions = []
        for i in range(c):
            mu_i = mu[i]
            max_mu_i = max(mu_i)
            lower_bound = [k for k in range(len(X)) if mu_i[k] >= (max_mu_i - alphas[i])]
            boundary_region = [k for k in range(len(X)) if alphas[i] < mu_i[k] < (max_mu_i - alphas[i])]
            lower_bounds.append(lower_bound)
            boundary_regions.append(boundary_region)

        return lower_bounds, boundary_regions

    def compute_prototypes(self, X, mu, c, m, lower_bounds, boundary_regions, wl, wb):
        prototypes = np.zeros(shape=(c, X.shape[1]))
        A2s = np.zeros(shape=(c, X.shape[1]))
        B2s = np.zeros(shape=(c, X.shape[1]))
        for i in range(c):
            mu_i = mu[i]
            for j in range(X.shape[1]):
                if len(lower_bounds[i]) != 0 and len(boundary_regions[i]) != 0:
                    A2s[i][j] = np.array([(mu_i[k] ** m) * X[k][j] for k in lower_bounds[i]]).sum() / np.array(
                        [(mu_i[k] ** m) for k in lower_bounds[i]]).sum()
                    B2s[i][j] = np.array([(mu_i[k] ** m) * X[k][j] for k in boundary_regions[i]]).sum() / np.array(
                        [(mu_i[k] ** m) for k in boundary_regions[i]]).sum()
                    prototypes[i][j] = wl * A2s[i][j] + wb * B2s[i][j]
                elif len(lower_bounds[i]) == 0:
                    B2s[i][j] = np.array([(mu_i[k] ** m) * X[k][j] for k in boundary_regions[i]]).sum() / np.array(
                        [(mu_i[k] ** m) for k in boundary_regions[i]]).sum()
                    prototypes[i][j] = B2s[i][j]
                else:
                    A2s[i][j] = np.array([(mu_i[k] ** m) * X[k][j] for k in lower_bounds[i]]).sum() / np.array(
                        [(mu_i[k] ** m) for k in lower_bounds[i]]).sum()
                    prototypes[i][j] = A2s[i][j]

        return prototypes

    def predict(self, X):
        X = check_array(X)

        np.random.seed(seed=self.random_state_)
        mu = np.random.rand(self.c, X.shape[0])
        mu = mu / np.sum(mu, axis=0, keepdims=1)

        for l in range(self.n_iter):
            alphas = self.compute_thresholds(mu, self.c)
            new_mu = self.update_mu(X, self.m, self.c, self.centers_)
            if np.linalg.norm(mu - new_mu) < self.epsilon:
                break
            mu = new_mu

        return new_mu, self.centers_, alphas

    def get_plotly_solver_name(self):
        return 'SRFCM'


class ThreeWayReclustering:

    def __init__(self, *, n_clusters=3, n_spectrals=10, min_gamma=-1, max_gamma=1, random_state=42):
        self.n_clusters = n_clusters
        self.n_spectrals = n_spectrals
        self.min_gamma = min_gamma
        self.max_gamma = max_gamma
        self.random_state = random_state

    def fit(self, X, y, full_X=(None, None), full_y=(None, None), full_y_shadow=(None, None), cv_num=None,
            random_state=42):
        X, y = check_X_y(X, y)
        self.X_train_ = X
        self.y_train_ = y
        self.full_y_shadow_, _ = full_y_shadow
        self.full_X_, _ = full_X
        self.full_y_, _ = full_y
        self.n_features_in_ = X.shape[1]
        self.cv_num_ = cv_num
        self.random_state_ = random_state

        return self

    def predict(self, X):
        X = check_array(X)

        spectrals = []
        preds = []
        for gamma in np.logspace(self.min_gamma, self.max_gamma, self.n_spectrals):
            spectral = SpectralClustering(n_clusters=self.n_clusters, gamma=gamma, random_state=self.random_state)
            spectral.fit(X)
            spectrals.append(spectral)
            preds.append(spectral.labels_)

        overlaps = []
        for i in range(1, self.n_spectrals):
            overlap = np.zeros(shape=(self.n_clusters, self.n_clusters))
            for j in range(self.n_clusters):
                for p in range(self.n_clusters):
                    for k in range(len(preds[0])):
                        if preds[0][k] == j and preds[i][k] == p:
                            overlap[j][p] += 1
            overlaps.append(overlap)

        new_preds = []
        new_preds.append(preds[0])
        for pred, overlap in zip(preds[1:], overlaps):
            new_pred = np.zeros(shape=(1, len(pred)))[0]
            for j in range(self.n_clusters):
                k = np.argmax(overlap[j])
                for i in range(len(pred)):
                    if pred[i] == k:
                        new_pred[i] = j
            new_preds.append(new_pred)

        clusters = []
        for pred in new_preds:
            cl = []
            for k in range(self.n_clusters):
                indices, = np.where(pred == k)
                cl.append(set(indices))
            clusters.append(cl)

        res = []
        for k in range(self.n_clusters):
            ls = [cluster[k] for cluster in clusters]
            intersection = set.intersection(*ls)
            union = set.union(*ls)
            res.append((intersection, union - intersection))

        preds = []
        for k in range(self.n_clusters):
            all_indices = set(list(range(len(X))))
            core_indices = res[k][0]
            shadow_indices = res[k][1]
            exclusion_indices = all_indices - (set.union(core_indices, shadow_indices))
            pred = np.zeros(shape=(1, len(X)))[0]

            pred[list(core_indices)] = 1
            pred[list(shadow_indices)] = 0
            pred[list(exclusion_indices)] = -1
            preds.append(pred)

        return preds

    def get_plotly_solver_name(self):
        return 'ThreeWayReclustering'