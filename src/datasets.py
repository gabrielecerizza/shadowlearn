import datetime
import random

import numpy as np
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.datasets import load_breast_cancer, load_wine, make_circles
from sklearn.decomposition import PCA
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import OneHotEncoder

from src.utils import add_random_dots

random_state = 42


def load_synthetic_dataset(target, anomalies='both'):
    X, y = make_circles(n_samples=200, factor=.3, noise=.05, random_state=random_state)
    if anomalies == 'both':
        X = np.concatenate((X, [[-0.15, -0.3], [-0.22, -0.33], [0.7, 0.6], [0.8, 0.4]]), axis=0)
        y = np.append(y, [0, 0, 1, 1])
    elif anomalies == 'red':
        X = np.concatenate((X, [[-0.15, -0.3], [-0.22, -0.33]]), axis=0)
        y = np.append(y, [0, 0])
    elif anomalies == 'blue':
        X = np.concatenate((X, [[0.7, 0.6], [0.8, 0.4]]), axis=0)
        y = np.append(y, [1, 1])
    else:
        raise ValueError('anomalies must be either "both", "inside" or "outside"')

    labels = pd.Series(y).apply(lambda x: 'blue' if x == 1 else 'red')

    if target == 'blue':
        y = pd.Series(y).apply(lambda x: x if x == 1 else -1)
    elif target == 'red':
        y = pd.Series(y).apply(lambda x: 1 if x == 0 else -1)
    return X, y, labels, target, [None]


def load_synthetic_dataset_with_proba(target, red_anomaly_proba=0, blue_anomaly_proba=0, both_anomaly_proba=None,
                                      red_anomaly_num=None, blue_anomaly_num=None):
    X, y = make_circles(n_samples=200, factor=.3, noise=.05, random_state=random_state)

    X, y = add_random_dots(X, y, blue_anomaly_num, red_anomaly_num)

    new_y = []
    for i in range(len(y)):
        # red dots
        if y[i] == 0:
            r = random.uniform(0, 1)
            if r <= blue_anomaly_proba:
                new_y.append(1)
            elif both_anomaly_proba is not None:
                if r <= both_anomaly_proba:
                    new_y.append(1)
            else:
                new_y.append(0)
        # blue dots
        elif y[i] == 1:
            r = random.uniform(0, 1)
            if r <= red_anomaly_proba:
                new_y.append(0)
            elif both_anomaly_proba is not None:
                if r <= both_anomaly_proba:
                    new_y.append(0)
            else:
                new_y.append(1)

    y = np.array(new_y)
    labels = pd.Series(y).apply(lambda x: 'blue' if x == 1 else 'red')

    if target == 'blue':
        y = pd.Series(y).apply(lambda x: x if x == 1 else -1)
    elif target == 'red':
        y = pd.Series(y).apply(lambda x: 1 if x == 0 else -1)

    return X, y, labels, target


def load_from_scikit(dataset, d, target, without):
    if dataset == 'breast_cancer':
        X, y = load_breast_cancer(return_X_y=True)
        data = load_breast_cancer()
    elif dataset == 'wine':
        X, y = load_wine(return_X_y=True)
        data = load_wine()
    else:
        raise ValueError('Unknown dataset name in load_from_scikit')
    target_names = data.target_names
    data_target = target_names.tolist().index(target)
    labels = pd.Series(y).apply(lambda x: target_names[x])
    y = pd.Series(y).apply(lambda x: 1 if x == data_target else -1)

    if without is not None:
        X = X[labels != without]
        y = y[labels != without]
        labels = labels[labels != without]

    if isinstance(d, str):
        if d != 'all':
            raise ValueError(
                f"When 'd' is a string, it should be 'all'"
            )
    else:
        raise ValueError(
            'Use dimensionality reduction inside grid_search'
        )
        # pca = PCA(n_components=d)
        # X = pca.fit_transform(X)

    return np.array(X), np.array(y), np.array(labels), target, None


def load_wine_dataset(d='all', target='class_0', without=None):
    return load_from_scikit('wine', d, target, without)


def load_breast_cancer_dataset(d='all', target='malignant'):
    return load_from_scikit('breast_cancer', d, target, without=None)


def load_iris(d=2, target='Iris-virginica', without=None):
    source = 'https://archive.ics.uci.edu/ml/' \
             'machine-learning-databases/iris/iris.data'
    iris_df = pd.read_csv(source, names=["sepal length", "sepal width", "petal length", "petal width", "class"])

    # df.pop()
    iris_values = iris_df.iloc[:, 0:4].values
    iris_labels = iris_df.iloc[:, 4].values

    if d != 'all':
        pca = PCA(n_components=d)
        iris_values = pca.fit_transform(iris_values)

    if without is not None:
        X = iris_values[iris_labels != without]
        y = iris_df['class'][iris_labels != without].apply(lambda x: 1 if x == target else -1)
        iris_labels = iris_labels[iris_labels != without]
        return X, np.array(y), iris_labels, target, iris_df.columns
    else:
        X = iris_values
        y = iris_df['class'].apply(lambda x: 1 if x == target else -1)
        return X, np.array(y), iris_labels, target, iris_df.columns


def load_compounded_iris(d=2, target1='Iris-virginica', target2='Iris-versicolor', without=None):
    X, y1, iris_labels, _, columns = load_iris(d, target1, without)
    _, y2, _, _, _ = load_iris(d, target2, without)
    return X, y1, y2, iris_labels, columns


def load_census_dataset(d='all', target='<=50K', target_column='income', length=150, rand=random_state):
    # source = 'https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data'
    source = 'F:\\Copia HD G\\Universita\\UNIMI\\Th\\PyCharm\\shadowedSetLearn\\datasets\\census\\adult.data'
    names = ['age', 'workclass', 'fnlwgt', 'education', 'education-num', 'marital-status', 'occupation',
             'relationship', 'race', 'sex', 'capital-gain', 'capital-loss', 'hours-per-week',
             'native-country', 'income']
    census_df = pd.read_csv(source, names=names, skipinitialspace=True)
    census_df = census_df.mask(census_df.eq('?')).dropna()
    census_df.reset_index(drop=True, inplace=True)
    # census_df = census_df.sample(length, weights=census_df.groupby(target_column)[target_column].transform('count'),
    #                             random_state=rand, axis=0)
    labels = census_df[target_column]
    census_df[target_column] = census_df[target_column].apply(lambda x: 1 if x == target else -1)
    sss = StratifiedShuffleSplit(n_splits=1, test_size=length, random_state=42)
    for train_index, test_index in sss.split(census_df['income'], census_df['income']):
        census_df = census_df.iloc[test_index]
    # census_df = census_df.sample(length, random_state=random_state)
    labels = labels[test_index]
    y = census_df[target_column]

    census_df_without_y = census_df.drop(target_column, 1)
    cat_names = [name for name in names if name != target_column
                 and census_df_without_y[name].dtype == 'O']
    column_transformer = ColumnTransformer([('encoder', OneHotEncoder(drop='if_binary'), cat_names)],
                                           remainder='passthrough')
    X = pd.DataFrame(column_transformer.fit_transform(census_df_without_y).todense())
    columns_names = X.columns

    if isinstance(d, str):
        if d != 'all':
            raise ValueError(
                f"When 'd' is a string, it should be 'all'"
            )
    else:
        pca = PCA(n_components=d)
        X = pca.fit_transform(X)
    
    return np.array(X), np.array(y), np.array(labels), target, columns_names


def load_student_dataset(d='all', target='A', target_column='final_grade_cat'):
    source = 'F:\\Copia HD G\\Universita\\UNIMI\\Th\\PyCharm\\shadowedSetLearn\\datasets\\student\\student-mat.csv'
    df = pd.read_csv(source, sep=';', skipinitialspace=True)
    df['final_grade_cat'] = pd.cut(df['G3'], bins=[-1, 6, 9, 12, 15, 17, 20], labels=['F', 'E', 'D', 'C', 'B', 'A'])
    labels = df[target_column]
    y = labels.apply(lambda x: 1 if x == target else -1)

    # dropping marks to make the other features meaningful
    df_without_y = df.drop([target_column, 'G1', 'G2', 'G3'], 1)
    cat_names = [name for name in df_without_y.columns if df_without_y[name].dtype == 'O']

    column_transformer = ColumnTransformer([('encoder', OneHotEncoder(drop='if_binary'), cat_names)],
                                           remainder='passthrough')
    X = pd.DataFrame(column_transformer.fit_transform(df_without_y))

    if isinstance(d, str):
        if d != 'all':
            raise ValueError(
                f"When 'd' is a string, it should be 'all'"
            )
    else:
        pca = PCA(n_components=d)
        X = pca.fit_transform(X)

    return np.array(X), np.array(y), np.array(labels), target, X.columns


def load_covid_dataset(target, y_column='LABEL-2', shadow_label=None, only_shadow=False):
    source = 'F:\\Copia HD G\\Universita\\UNIMI\\Th\\Datasets\\covnet\\data_covnet_score-imputed_missRF_increasing_selected.csv'
    df = pd.read_csv(source)

    X = df.iloc[:, 7:]
    labels = df[y_column]
    y = labels.apply(lambda x: 1 if x == target else -1)

    if shadow_label is None:
        return np.array(X), np.array(y), np.array(labels), target, X.columns
    elif shadow_label == 'LABEL-3':
        shadow_labels = df[shadow_label]
        if target == 0:
            y_shadow = shadow_labels.apply(lambda x: 1 if x == 0 else 0 if x == 1 else -1)
        elif target == 1:
            y_shadow = shadow_labels.apply(lambda x: 1 if x == 2 else 0 if x == 1 else -1)
        else:
            raise ValueError(
                'load_covid_dataset: unknown target'
            )
        if only_shadow:
            return np.array(X), np.array(y_shadow), np.array(shadow_labels), target, X.columns
        else:
            return np.array(X), np.array(y), np.array(y_shadow), np.array(labels), np.array(shadow_labels), \
                   target, X.columns
    else:
        raise ValueError(
            'load_covid_dataset: unknown shadow_label'
        )


def load_run_over_dataset(target, template, without=None, from_year=None):
    template_types = ['global-total', 'macro-totals', 'macro-districts', 'macro-head', 'macro-thorax',
                      'macro-abdomen', 'macro-skeleton', 'all-details', 'hand-picked', 'hand-picked-var',
                      'var-selected']

    source = 'F:\\Copia HD G\\Universita\\UNIMI\\Th\\Datasets\\run_over\\run-over-dataset.xlsx'
    df = pd.read_excel(source)
    if from_year is not None:
        df = df[df['DATA'] >= datetime.datetime(from_year, 1, 1)]

    df['DATA'] = df['DATA'].apply(lambda x: (x - datetime.datetime(1970, 1, 1)).days)

    labels = df.pop('Mezzo')
    y = labels.apply(lambda x: 1 if x == target else -1)

    if template == 'global-total':
        X = df[['DATA', 'SESSO', 'ANNI', 'PESO', 'ALTEZZA', 'BMI', 'Totale']]
    elif template == 'macro-totals':
        X = df[['DATA', 'SESSO', 'ANNI', 'PESO', 'ALTEZZA', 'BMI', 'Tot Testa', 'Tot Torace', 'Tot Addome',
                'Tot Scheletro']]
    elif template == 'hand-picked':
        '''
        X = df[['DATA', 'PESO', 'BMI', 'Testa:Splancnocranio', 'Torace:Polmoni', 'Torace:Trachea/bronchi',
                'Torace:Cuore', 'Torace:Diaframma', 'Addome:Milza', 'Addome:Aorta addominale',
                'Addome:Reni', 'Addome:Mesentere', 'Scheletro:Rachide lombare']]
        '''
        X = df[['DATA', 'PESO', 'BMI', 'Testa:Splancnocranio', 'Torace:Polmoni', 'Torace:Trachea/bronchi',
                'Torace:Cuore', 'Addome:Fegato', 'Addome:Milza',
                'Addome:Reni', 'Addome:Mesentere']]
    elif template == 'hand-picked-var':  # hand-picked-var = explained-var in latex report
        X = df[['DATA', ' Totale coste', 'ANNI', 'PESO', 'Totale', 'Tot Testa', 'Tot Testa.1']]
    elif template == 'hand-picked-var-2008':  # hand-picked-var-2008 = explained-var-2008 in latex report
        X = df[['DATA', ' Totale coste', 'ANNI', 'PESO', 'Totale']]
    elif template == 'macro-districts':
        X = df.iloc[:, 1:27]
    elif template == 'macro-head':
        X = df.iloc[:, 1:12]
    elif template == 'macro-thorax':
        X = df.iloc[:, list(range(1, 7)) + list(range(12, 17))]
    elif template == 'macro-abdomen':
        X = df.iloc[:, list(range(1, 7)) + list(range(17, 22))]
    elif template == 'macro-skeleton':
        X = df.iloc[:, list(range(1, 7)) + list(range(22, 27))]
    elif template == 'all-details':
        X = df.iloc[:, list(range(1, 7)) + list(range(32, 50)) + list(range(55, 79)) + list(range(83, 281))
                       + list(range(282, 284)) + list(range(285, 295)) + list(range(296, 366))]
        X = X[[col for col in X.columns if 'costa' not in col]]
    elif template == 'var-selected':
        X = df[['DATA', 'SESSO', 'ANNI', 'PESO', 'ALTEZZA', 'BMI', 'Testa:Splancnocranio', 'Testa:Cervelletto', 'Testa:Tronco encefalico',
                'Torace:Polmoni', 'Torace:Trachea/bronchi', 'Torace:Cuore', 'Torace:Aorta toracica', 'Addome:Aorta addominale', 'Scheletro:Rachide cervicale',
                'Scheletro:Rachide lombare']]
    else:
        raise ValueError(
            f"'template' should be one of {template_types}."
            f"Got '{template}' instead."
        )

    if without is not None:
        X = X.drop(without, axis='columns')

    load_run_over_dataset.column_names_ = X.columns

    return np.array(X), np.array(y), np.array(labels), target, X.columns



