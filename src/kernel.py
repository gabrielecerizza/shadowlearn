import numpy as np
from numpy import linalg as la
from abc import ABCMeta, abstractmethod


class Kernel(metaclass=ABCMeta):

    def __call__(self, *args):
        return self.compute(*args)

    @abstractmethod
    def compute(self, x, y):
        pass


class GaussianKernel(Kernel):

    def __init__(self, sigma=0.5):
        self.sigma = sigma

    def compute(self, x, y):
        y = np.array(y)
        x, y = (np.array(arg) if type(arg) is not np.array else arg for arg in (x, y))
        try:
            numerator = -1. * (la.norm(x - y) ** 2)
        except ValueError as ve:
            raise ve
        denominator = 2 * (self.sigma ** 2)
        return np.exp(numerator / denominator)

    def __str__(self):
        return fr'Gaussian Kernel with $\sigma = {self.sigma:.2f}$'

    def get_plotly_string(self):
        return fr'\sigma = {self.sigma:.2f}'
