import logging

import numpy as np
import pandas as pd
from scipy.spatial.distance import euclidean, sqeuclidean
from sklearn.metrics import calinski_harabasz_score, davies_bouldin_score, silhouette_score, accuracy_score, \
    roc_auc_score, f1_score, recall_score, precision_score
from sklearn.model_selection import StratifiedShuffleSplit, StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.utils import check_consistent_length
from sklearn.decomposition import PCA

import src.utils

random_state = 42


def uncertain_number_score(y_true, y_pred):
    y_pred = np.array(y_pred)
    indices, = np.where(y_pred == 'uncertain')
    return len(indices)


def my_auc_score(y_true, y_pred, main_metric_name):
    new_y_pred, y_true = _my_score_pred_preparation(main_metric_name, y_pred, y_true)
    return roc_auc_score(y_true, new_y_pred)


def proba_auc_score(y_true, y_pred):
    y_true = np.array(y_true)
    check_consistent_length(y_true, y_pred)
    new_y_pred = np.array([el if el == 1 else 0 if el == -1 else 0.5 for el in y_pred])
    return roc_auc_score(y_true, new_y_pred)


def multiclass_auc_score(y_true, y_pred):
    y_true = np.array(y_true)
    check_consistent_length(y_true, y_pred)
    new_y_pred = np.array([[0, 0, 1] if el == 1 else [1, 0, 0] if el == -1 else [0, 1, 0] for el in y_pred])
    return roc_auc_score(y_true, new_y_pred, average='weighted', multi_class='ovo')


def _my_score_pred_preparation(main_metric_name, y_pred, y_true):
    y_true = np.array(y_true)
    check_consistent_length(y_true, y_pred)
    if main_metric_name == 'conservative':
        new_y_pred = np.array([el if el == 1 else -1 for el in y_pred])
    elif main_metric_name == 'non-conservative':
        new_y_pred = np.array([el if el == -1 else 1 for el in y_pred])
    return new_y_pred, y_true


def my_f1_score(y_true, y_pred, main_metric_name):
    new_y_pred, y_true = _my_score_pred_preparation(main_metric_name, y_pred, y_true)
    return f1_score(y_true, new_y_pred)


def my_sensitivity_score(y_true, y_pred, main_metric_name):
    new_y_pred, y_true = _my_score_pred_preparation(main_metric_name, y_pred, y_true)
    return recall_score(y_true, new_y_pred, labels=[1], average='weighted')


def my_specificity_score(y_true, y_pred, main_metric_name):
    new_y_pred, y_true = _my_score_pred_preparation(main_metric_name, y_pred, y_true)
    return recall_score(y_true, new_y_pred, labels=[-1], average='weighted')


def shadow_f1_score(estimator, X, y):
    f = lambda y_true, y_preds: f1_score(y_true, y_preds, average='weighted')
    return shadow_accuracy(estimator, X, y, f)


def my_precision_score(y_true, y_pred, main_metric_name):
    new_y_pred, y_true = _my_score_pred_preparation(main_metric_name, y_pred, y_true)
    return precision_score(y_true, new_y_pred, labels=[1], average='weighted')


def shadow_ratio(y_true, y_pred):
    return shadow_count(y_true, y_pred) / len(y_pred)


def shadow_count(y_true, y_pred):
    y_pred = np.array(y_pred)
    return len(y_pred[y_pred == 0])


def entropy(series, normalize=True):
    freq = series.value_counts(normalize=True)
    # mia aggiunta, per i casi in cui c'è solo una classe o non c'è shadow
    if len(freq) == 1 or len(freq) == 0:
        return 0
    e = sum((freq.map(lambda f: -f * np.log2(f))))
    if normalize:
        e /= np.log2(len(freq))
    return e


def get_clusters(X, y_labels):
    unique_labels = np.unique(y_labels)
    clusters = {}
    for ul in unique_labels:
        i = np.where(y_labels == ul)
        cluster = np.array(X[i])
        clusters[ul] = cluster
    return clusters


def conservative_score(y_true, y_pred, *, normalize=True):
    y_true = np.array(y_true)
    check_consistent_length(y_true, y_pred)
    new_y_pred = np.array([el if el == 1 else -1 for el in y_pred])
    if normalize:
        return np.mean(y_true == new_y_pred)
    else:
        return sum(y_true == new_y_pred)


def non_conservative_score(y_true, y_pred, *, normalize=True):
    y_true = np.array(y_true)
    check_consistent_length(y_true, y_pred)
    new_y_pred = np.array([el if el == -1 else 1 for el in y_pred])
    if normalize:
        return np.mean(y_true == new_y_pred)
    else:
        return sum(y_true == new_y_pred)


def compounded_score_with_bias(y_true, y_pred, *, normalize=True, target_bias=None, track=False):
    y_true = np.array(y_true)
    check_consistent_length(y_true, y_pred)
    if target_bias is not None:
        y_pred = np.array([target_bias if el == 0 else el for el in y_pred])
    else:
        raise ValueError('Target bias was None')
    if normalize:
        if track:
            print('y_true: \n' + y_true)
            print('y_pred: \n' + y_pred)
        return np.mean(y_true == y_pred)
    else:
        return sum(y_true == y_pred)


def shadow_indifferent_score(y_true, y_pred):
    y_true = np.array(y_true)
    check_consistent_length(y_true, y_pred)

    return sum(y_true == y_pred) / len([pred for pred in y_pred if pred != 0])


def shadow_half_score(y_true, y_pred):
    y_true = np.array(y_true)
    check_consistent_length(y_true, y_pred)

    result = []
    for true, pred in zip(y_true, y_pred):
        if true == pred:
            result.append(1)
        elif pred == 0:
            result.append(0.5)
        else:
            result.append(0)

    return np.mean(np.array(result))


def two_to_three_values_custom_scorer(estimator, X, y, metric=accuracy_score):
    y = np.array(y)
    if isinstance(estimator, Pipeline):
        X_train = estimator.named_steps['estimator'].X_train_
        y_train = estimator.named_steps['estimator'].y_train_
        full_y_shadow = estimator.named_steps['estimator'].full_y_shadow_
        cv_num = estimator.named_steps['estimator'].cv_num_
        # cv = estimator.named_steps['estimator'].cv_
        internal_random_state = estimator.named_steps['estimator'].random_state_
        full_X = estimator.named_steps['estimator'].full_X_
        full_y = estimator.named_steps['estimator'].full_y_
    else:
        X_train = estimator.X_train_
        y_train = estimator.y_train_
        full_y_shadow = estimator.full_y_shadow_
        cv_num = estimator.cv_num_
        # cv = estimator.cv_
        internal_random_state = estimator.random_state_
        full_X = estimator.full_X_
        full_y = estimator.full_y_

    splitter = StratifiedKFold(cv_num, random_state=internal_random_state, shuffle=True)
    found = False
    for i, (train_index, test_index) in enumerate(splitter.split(full_X, full_y)):
        splitted_test_y = np.array(full_y[test_index])

        if len(splitted_test_y) != len(y):
            continue

        comparison = splitted_test_y == y

        if isinstance(comparison, bool):
            raise ValueError(
                'shadow_accuracy: error in comparison'
            )

        if comparison.all():
            found = True
            # print('rotation: ', i)
            shadow_y_test = full_y_shadow[test_index]
            break

    if not found:
        if X_train.shape == full_X.shape:
            shadow_y_test = y
        else:
            raise ValueError(
                'shadow_accuracy: could not find test_index'
            )

    estimator_preds = estimator.predict(X)

    return return_custom_score(shadow_y_test, estimator_preds, metric)


def return_custom_score(y_test, estimator_preds, metric):
    if metric == 'custom-exclusion-recall':
        return recall_score(y_test, estimator_preds, labels=[-1], average='weighted')
    elif metric == 'custom-shadow-recall':
        return recall_score(y_test, estimator_preds, labels=[0], average='weighted')
    elif metric == 'custom-core-recall':
        return recall_score(y_test, estimator_preds, labels=[1], average='weighted')
    elif metric == 'custom-exclusion-precision':
        return precision_score(y_test, estimator_preds, labels=[-1], average='weighted')
    elif metric == 'custom-shadow-precision':
        return precision_score(y_test, estimator_preds, labels=[0], average='weighted')
    elif metric == 'custom-core-precision':
        return precision_score(y_test, estimator_preds, labels=[1], average='weighted')
    elif metric == 'custom-multi-class-auc':
        new_estimator_preds = np.array([[0, 0, 1] if el == 1 else [1, 0, 0] if el == -1 else [0, 1, 0]
                                        for el in estimator_preds])
        return roc_auc_score(y_test, new_estimator_preds, average='weighted', multi_class='ovo')
    elif metric == 'custom-f1-score':
        return f1_score(y_test, estimator_preds, average='weighted')
    else:
        return metric(y_test, estimator_preds)


def shadow_accuracy(estimator, X, y, metric=accuracy_score):
    return two_to_three_values_custom_scorer(estimator, X, y, metric)


def get_svc_uncertain_elements(X_train, y_train, X_test, y_test):
    clf_SVC = SVC(C=10)
    clf_SVC.fit(X_train, y_train)
    all_X = np.concatenate((X_train, X_test))
    all_y = np.concatenate((y_train, y_test))
    svc_res = clf_SVC.predict(all_X)

    uncertain_elements = [x for y_true, y_pred, x in zip(all_y, svc_res, all_X) if y_true != y_pred]
    return uncertain_elements


def svc_shadowed_score(estimator, X, y):
    # More weight to uncertain points
    pca = None
    if isinstance(estimator, Pipeline):
        X_train = estimator.named_steps['estimator'].X_train_
        y_train = estimator.named_steps['estimator'].y_train_
        pca = estimator.named_steps['pca']
        svc_X = pca.transform(X)
    else:
        X_train = estimator.X_train_
        y_train = estimator.y_train_
        svc_X = X
    # print(X.shape)
    # print(X_train.shape)
    clf_SVC = SVC(C=10)
    clf_SVC.fit(X_train, y_train)
    svc_res = clf_SVC.predict(svc_X)
    svc_res = np.array([a if a == b else 0 for a, b in zip(svc_res, y)])

    estimator_res = estimator.predict(X)

    # res = np.mean(svc_res == estimator_res)
    res = np.array([1 if s == e else -2 if s == 0 else 0 for s, e in zip(svc_res, estimator_res)])
    return np.mean(res)


def get_uncertain_elements(clusters, type):
    uncertain_elements = []
    for key, cluster in clusters.items():
        for i, element in enumerate(cluster):
            element_mean_dict = {}
            element_min_dict = {}
            for inner_key, inner_cluster in clusters.items():
                distances = []
                for j, inner_element in enumerate(inner_cluster):
                    if inner_key != key or i != j:
                        dist = np.linalg.norm(element - inner_element)
                        distances.append(dist)
                element_mean_dict[inner_key] = np.mean(np.array(distances))
                element_min_dict[inner_key] = np.min(np.array(distances))

            my_cluster_mean = element_mean_dict[key]
            my_cluster_min = element_min_dict[key]

            radius_zone = []
            entropy_radius_zone = []
            entropy_radius_min = min([v for k, v in element_mean_dict.items()])
            for inner_key, inner_cluster in clusters.items():
                for j, inner_element in enumerate(inner_cluster):
                    if inner_key != key or i != j:
                        dist = np.linalg.norm(element - inner_element)
                        if dist < my_cluster_mean:
                            radius_zone.append(inner_key)
                        if dist < entropy_radius_min:
                            entropy_radius_zone.append(inner_key)

            key_counts = pd.Series(radius_zone).value_counts(normalize=True)
            ent = entropy(pd.Series(entropy_radius_zone))

            if type == 'mean':
                for dict_key, other_cluster_mean in element_mean_dict.items():
                    if dict_key != key and other_cluster_mean < my_cluster_mean:
                        uncertain_elements.append(element)
                        break
            elif type == 'min':
                for dict_key, other_cluster_min in element_min_dict.items():
                    if dict_key != key and other_cluster_min < my_cluster_min:
                        uncertain_elements.append(element)
                        break
            elif type == 'mean-or-min':
                for dict_key in element_mean_dict:
                    if dict_key != key and \
                            (element_mean_dict[dict_key] < my_cluster_mean
                             or element_min_dict[dict_key] < my_cluster_min):
                        uncertain_elements.append(element)
                        break
            elif type == 'freq-mean':
                for dict_key in element_mean_dict:
                    if key != dict_key and dict_key in key_counts \
                            and key_counts[dict_key] > key_counts[key]:
                        uncertain_elements.append(element)
                        break
            elif type == 'entropy':
                if ent > 0.9:
                    uncertain_elements.append(element)
            elif type == 'silhouette':
                other_min = np.inf
                b_i = False
                for dict_key in element_mean_dict:
                    if key != dict_key and element_mean_dict[dict_key] < other_min:
                        b_i = element_mean_dict[dict_key]
                        other_min = element_mean_dict[dict_key]
                a_i = my_cluster_mean
                if b_i:
                    # s_i = silhouette
                    s_i = (b_i - a_i) / max(b_i, a_i)
                    if s_i < -0.2:
                        # print(key, i, b_i, a_i, s_i)
                        uncertain_elements.append(element)
            else:
                raise ValueError('wrong type in get_uncertain_elements')

    return uncertain_elements


def inner_method_template(X, estimator, uncertain_elements, y, weight=1, bias=1, trace=False):
    estimator_res = estimator.predict(X)
    i_zero = np.where(estimator_res == 0)
    i_not_zero = np.where(estimator_res != 0)
    not_zero_res = np.array(np.array(estimator_res[i_not_zero]) == np.array(y[i_not_zero]))
    zero_res = []
    for el in X[i_zero]:
        if el in uncertain_elements:
            for i in range(bias):
                zero_res.append(True)
        else:
            zero_res.append(False)
    zero_res = np.array(zero_res)
    zero_res = np.tile(zero_res, weight)
    res = np.concatenate([not_zero_res, zero_res])
    if trace:
        X_train = estimator.X_train_
        un_train = [el for el in X_train if el in uncertain_elements]
        un_test = [el for el in X if el in uncertain_elements]
        print(f'{len(un_train)} uncertain elements in X_train - {len(un_test)} uncertain elements in X_test')
    return np.mean(res)


def mean_distance_score(all_X, y_labels, weight=1, bias=1, trace=False):
    def inner_score(estimator, X, y):
        clusters = get_clusters(all_X, y_labels)
        uncertain_elements = np.array(get_uncertain_elements(clusters, type='mean'))
        return inner_method_template(X, estimator, uncertain_elements, y, weight, bias, trace)

    return inner_score


def min_distance_score(all_X, y_labels, weight=1, bias=1, trace=False):
    def inner_score(estimator, X, y):
        clusters = get_clusters(all_X, y_labels)
        uncertain_elements = np.array(get_uncertain_elements(clusters, type='min'))
        return inner_method_template(X, estimator, uncertain_elements, y, weight, bias, trace)

    return inner_score


def mean_or_min_distance_score(all_X, y_labels, weight=1, bias=1, trace=False):
    def inner_score(estimator, X, y):
        clusters = get_clusters(all_X, y_labels)
        uncertain_elements = np.array(get_uncertain_elements(clusters, type='mean-or-min'))
        return inner_method_template(X, estimator, uncertain_elements, y, weight, bias, trace)

    return inner_score


def freq_mean_distance_score(all_X, y_labels, weight=1, bias=1, trace=False):
    def inner_score(estimator, X, y):
        clusters = get_clusters(all_X, y_labels)
        uncertain_elements = np.array(get_uncertain_elements(clusters, type='freq-mean'))
        return inner_method_template(X, estimator, uncertain_elements, y, weight, bias, trace)

    return inner_score


def silhouette_distance_score(all_X, y_labels, weight=1, bias=1, trace=False):
    def inner_score(estimator, X, y):
        clusters = get_clusters(all_X, y_labels)
        uncertain_elements = np.array(get_uncertain_elements(clusters, type='silhouette'))
        return inner_method_template(X, estimator, uncertain_elements, y, weight, bias, trace)

    return inner_score


def entropy_distance_score(all_X, y_labels, weight=1, bias=1, trace=False):
    def inner_score(estimator, X, y):
        clusters = get_clusters(all_X, y_labels)
        uncertain_elements = np.array(get_uncertain_elements(clusters, type='entropy'))
        return inner_method_template(X, estimator, uncertain_elements, y, weight, bias, trace)

    return inner_score


def homogeneity_score(y_true, y_pred):
    y_pred = np.array(y_pred)
    y_true = np.array(y_true)
    i_zero = np.where(y_pred == 0)
    i_not_zero = np.where(y_pred != 0)

    not_zero_res = np.array(y_pred[i_not_zero] == y_true[i_not_zero])
    zero_res = entropy(pd.Series(y_true[i_zero]))
    zero_res_filled = np.full(len(y_true[i_zero]), zero_res)
    res = np.concatenate([not_zero_res, zero_res_filled])

    return np.mean(res)


def get_clusters(X, y_pred):
    clusters_labels = np.unique(y_pred)
    clusters = {}
    for cluster_label in clusters_labels:
        clusters[cluster_label] = X[y_pred == cluster_label]
    return clusters


def get_centroids(clusters):
    centroids = {}
    for cluster_label, cluster_X in clusters.items():
        matrix = np.array(cluster_X)
        centroids[cluster_label] = matrix.mean(0)
    return centroids


def sum_distance_from_clusters_centroids(clusters, centroids):
    clusters_sum = 0
    for cluster_label, cluster_X in clusters.items():
        clusters_sum += sum([sqeuclidean(element, centroids[cluster_label]) for element in cluster_X])
    return clusters_sum


def root_mean_square_std_dev(X, y_pred):
    X = np.array(X)
    clusters = get_clusters(X, y_pred)
    centroids = get_centroids(clusters)
    p = X.shape[-1]

    clusters_sum = sum_distance_from_clusters_centroids(clusters, centroids)
    length_sum = 0
    for cluster_label, cluster_X in clusters.items():
        length_sum += len(cluster_X) - 1

    return (clusters_sum / (p * length_sum)) ** 0.5


def r_squared(X, y_pred):
    X = np.array(X)
    clusters = get_clusters(X, y_pred)
    centroids = get_centroids(clusters)
    all_data_centroid = X.mean()

    all_data_distances_sum = sum([sqeuclidean(x, all_data_centroid) for x in X])
    clusters_sum = sum_distance_from_clusters_centroids(clusters, centroids)

    return (all_data_distances_sum - clusters_sum) / all_data_distances_sum


def modified_hubert_gamma(X, y_pred):
    X = np.array(X)
    clusters = get_clusters(X, y_pred)
    centroids = get_centroids(clusters)
    n = len(X)

    res = 0
    for x1, x1_pred in zip(X, y_pred):
        for x2, x2_pred in zip(X, y_pred):
            dist_x1_x2 = euclidean(x1, x2)
            dist_centroids = euclidean(centroids[x1_pred], centroids[x2_pred])
            res += dist_x1_x2 * dist_centroids

    return 2 * res / (n * (n - 1))


def dunn_index(X, y_pred):
    X = np.array(X)
    clusters = get_clusters(X, y_pred)

    min_between_cluster_distance = get_min_between_clusters_distance(clusters)

    max_inter_cluster_distance = -np.inf
    for cluster_label, cluster_X in clusters.items():
        distances = []
        for i, x1 in enumerate(cluster_X):
            for j, x2 in enumerate(cluster_X):
                if i != j:
                    distance = euclidean(x1, x2)
                    distances.append(distance)

        max_distance = max(distances)
        if max_distance > max_inter_cluster_distance:
            max_inter_cluster_distance = max_distance

    return min_between_cluster_distance / max_inter_cluster_distance


def get_min_between_clusters_distance(clusters):
    min_between_clusters_distance = np.inf
    for cluster_label1, cluster_X1 in clusters.items():
        for cluster_label2, cluster_X2 in clusters.items():
            if cluster_label1 == cluster_label2:
                continue
            distances = []
            # TODO: redoing things twice here... should consider cluster couples only once
            for x1 in cluster_X1:
                for x2 in cluster_X2:
                    distance = euclidean(x1, x2)
                    distances.append(distance)
            min_distance = min(distances)
            if min_distance < min_between_clusters_distance:
                min_between_clusters_distance = min_distance
    return min_between_clusters_distance


def xie_beni_index(X, y_pred):
    X = np.array(X)
    clusters = get_clusters(X, y_pred)
    centroids = get_centroids(clusters)

    squared_distances = []
    for x, x_label in zip(X, y_pred):
        centroid = centroids[x_label]
        distance = sqeuclidean(x, centroid)
        squared_distances.append(distance)

    mean_squared_distance = np.array(squared_distances).mean()

    min_between_cluster_distance = get_min_between_clusters_distance(clusters)
    return mean_squared_distance / (min_between_cluster_distance ** 2)


def sd_validity_index(X, y_pred):
    X = np.array(X)
    clusters = get_clusters(X, y_pred)
    centroids = get_centroids(clusters)

    average_scattering = get_average_scattering(X, clusters)

    min_centroids_distance = np.inf
    max_centroids_distance = -np.inf
    for cluster_label1, centroid1 in centroids.items():
        for cluster_label2, centroid2 in centroids.items():
            if cluster_label1 != cluster_label2:
                distance = euclidean(centroid1, centroid2)
                if distance < min_centroids_distance:
                    min_centroids_distance = distance
                if distance > max_centroids_distance:
                    max_centroids_distance = distance

    temp_sum = 0
    for cluster_label1, centroid1 in centroids.items():
        internal_sum = 0
        for cluster_label2, centroid2 in centroids.items():
            if cluster_label1 != cluster_label2:
                distance = euclidean(centroid1, centroid2)
                internal_sum += distance
        temp_sum += 1 / internal_sum

    total_separation = max_centroids_distance / min_centroids_distance * temp_sum

    # in our case alpha is always equal to average scattering (D)
    return average_scattering * total_separation + average_scattering


def get_average_scattering(X, clusters):
    temp_sum = 0
    for cluster_label, cluster_X in clusters.items():
        var = np.var(np.array(cluster_X), axis=0)
        norm = np.linalg.norm(var)
        temp_sum += norm
    average_scattering = (temp_sum / len(clusters)) / np.linalg.norm(np.var(X, axis=0))
    return average_scattering


def s_dbw_index(X, y_pred):
    X = np.array(X)
    clusters = get_clusters(X, y_pred)
    centroids = get_centroids(clusters)

    temp_sum = 0
    for cluster_label, cluster_X in clusters.items():
        var = np.var(np.array(cluster_X), axis=0)
        norm = np.linalg.norm(var)
        temp_sum += norm
    sigma = (temp_sum ** 0.5) / len(clusters)

    temp_sum = 0
    for centroid_label1, centroid1 in centroids.items():
        for centroid_label2, centroid2 in centroids.items():
            if centroid_label1 != centroid_label2:
                midpoint = np.mean([centroid1, centroid2], axis=0)
                numerator = gamma_density(midpoint, clusters[centroid_label1], clusters[centroid_label2], sigma)
                denominator = max(gamma_density(centroid1, clusters[centroid_label1], clusters[centroid_label2], sigma),
                                  gamma_density(centroid2, clusters[centroid_label1], clusters[centroid_label2], sigma))
                temp_sum += numerator / denominator

    between_clusters_density = (2 / (len(clusters) * (len(clusters) - 1))) * temp_sum
    average_scattering = get_average_scattering(X, clusters)

    return between_clusters_density + average_scattering


def gamma_density(point, cluster_X1, cluster_X2, sigma):
    all_points = np.concatenate((cluster_X1, cluster_X2))
    res = 0
    for x in all_points:
        distance = euclidean(x, point)
        if distance < sigma:
            res += 1
    return res


def clustering_metrics_analysis(dataset_loader, estimator, score_name='Score', log=True, dim=None):
    X, y, data_labels, target, _ = dataset_loader()
    svc = SVC()

    splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=random_state)
    for i, (train_index, test_index) in enumerate(splitter.split(X, data_labels)):
        X_train = X[train_index]
        y_train = y[train_index]
        label_train = data_labels[train_index]
        X_test = X[test_index]
        y_test = y[test_index]
        label_test = data_labels[test_index]
        if dim is not None:
            pca = PCA(n_components=dim)
            pca.fit(X_train, y_train)
            X_train = pca.transform(X_train)
            X_test = pca.transform(X_test)
            X = pca.transform(X)

    estimator.fit(X_train, y_train)
    svc.fit(X_train, label_train)

    # Predict all, not just test. Training only on train
    y_pred = estimator.predict(X)
    svc_pred = svc.predict(X)

    frames = []

    # optimal value: max
    score = silhouette_score(X, y_pred, metric='euclidean')
    svc_score = silhouette_score(X, svc_pred, metric='euclidean')
    default_score = silhouette_score(X, data_labels, metric='euclidean')
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Max (the closer to 1, the better)'}
    frames.append(pd.DataFrame(d, index=['Silhouette Index (S)']))

    # optimal value: max
    score = calinski_harabasz_score(X, y_pred)
    svc_score = calinski_harabasz_score(X, svc_pred)
    default_score = calinski_harabasz_score(X, data_labels)
    d = {score_name: score,'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Max'}
    frames.append(pd.DataFrame(d, index=['Calinski-Harabasz Index (CH)']))

    # optimal value: min
    score = davies_bouldin_score(X, y_pred)
    svc_score = davies_bouldin_score(X, svc_pred)
    default_score = davies_bouldin_score(X, data_labels)
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Min (the closer to 0, the better)'}
    frames.append(pd.DataFrame(d, index=['Davies-Bouldin Index (DB)']))

    # optimal value: elbow
    # Elbow: optimal
    # cluster number is reached at the shift point of the score curve.
    # The judgement of the shift point is very subjective and hard
    # to determine
    score = root_mean_square_std_dev(X, y_pred)
    svc_score = root_mean_square_std_dev(X, svc_pred)
    default_score = root_mean_square_std_dev(X, data_labels)
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Elbow'}
    frames.append(pd.DataFrame(d, index=['Root-Mean-Square Std Dev (RMSSTD)']))

    # optimal value: elbow
    score = r_squared(X, y_pred)
    svc_score = r_squared(X, svc_pred)
    default_score = r_squared(X, data_labels)
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Elbow'}
    frames.append(pd.DataFrame(d, index=['R-Squared (RS)']))

    # optimal value: elbow
    score = modified_hubert_gamma(X, y_pred)
    svc_score = modified_hubert_gamma(X, svc_pred)
    default_score = modified_hubert_gamma(X, data_labels)
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Elbow'}
    gamma = r"$\Gamma$"
    frames.append(pd.DataFrame(d, index=['Modified Hubert ' + gamma + ' Statistic (' + gamma + ')']))

    # optimal value: Max
    score = dunn_index(X, y_pred)
    svc_score = dunn_index(X, svc_pred)
    default_score = dunn_index(X, data_labels)
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Max'}
    frames.append(pd.DataFrame(d, index=['Dunn Index (D)']))

    # optimal value: Min
    score = xie_beni_index(X, y_pred)
    svc_score = xie_beni_index(X, svc_pred)
    default_score = xie_beni_index(X, data_labels)
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Min'}
    frames.append(pd.DataFrame(d, index=['Xie-Beni Index (XB)']))

    # optimal value: Min
    score = sd_validity_index(X, y_pred)
    svc_score = sd_validity_index(X, svc_pred)
    default_score = sd_validity_index(X, data_labels)
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Min'}
    frames.append(pd.DataFrame(d, index=['SD Validity Index (SD)']))

    # optimal value: Min
    score = s_dbw_index(X, y_pred)
    svc_score = s_dbw_index(X, svc_pred)
    default_score = s_dbw_index(X, data_labels)
    d = {score_name: score, 'SVC Score': svc_score, 'Default Labels Score': default_score, 'Optimal value': 'Min'}
    frames.append(pd.DataFrame(d, index=['S_Dbw Validity Index (S_Dbw)']))

    res_df = pd.concat(frames)

    if log:
        logging.info('clustering_metrics_analysis on ' +
                     str(dataset_loader) + ' dataset with ' + str(target) + ' target')
        logging.info('dataset loader: ' + str(dataset_loader))
        logging.info('estimator: ' + str(estimator))
        logging.info('splitter: ' + str(splitter))
        logging.info('res_df: ' + str(res_df))

    return res_df


def from_list_to_set(ls):
    res = set()
    for el in ls:
        set_el = (el[0], el[1])
        res.add(set_el)
    return res


def get_uncertain_ratios(dataset_loader, estimator, estimator_name):
    tol = 1e-6
    X, y, data_labels, target, _ = dataset_loader()
    splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=42)
    gen = splitter.split(X, data_labels)
    train_index, test_index = next(gen)

    pca = PCA(n_components=2)
    X_train = X[train_index]
    X_test = X[test_index]
    y_train = y[train_index]
    y_test = y[test_index]
    pca.fit(X_train, y_train)
    X_train = pca.transform(X_train)
    X_test = pca.transform(X_test)
    X = pca.transform(X)
    labels_train = data_labels[train_index]
    labels_test = data_labels[test_index]

    clusters = get_clusters(X, data_labels)

    metrics_ls = ['SVC', 'Mean', 'Min', 'Mean or min', 'Freq', 'Entropy', 'Silhouette', 'Union', 'Intersection',
                  'Intersection 3']
    all_uncertain_ls = []
    test_uncertain_ls = []

    svc_all_uncertain = from_list_to_set(get_svc_uncertain_elements(X_train, labels_train, X_test, labels_test))
    svc_test_uncertain = from_list_to_set([x for unc in svc_all_uncertain for x in X_test
                                           if unc[0] == x[0] and unc[1] == x[1]])
    all_uncertain_ls.append(svc_all_uncertain)
    test_uncertain_ls.append(svc_test_uncertain)

    mean_all_uncertain = from_list_to_set(get_uncertain_elements(clusters, 'mean'))
    mean_test_uncertain = from_list_to_set([x for unc in mean_all_uncertain for x in X_test
                                            if unc[0] == x[0] and unc[1] == x[1]])
    all_uncertain_ls.append(mean_all_uncertain)
    test_uncertain_ls.append(mean_test_uncertain)

    min_all_uncertain = from_list_to_set(get_uncertain_elements(clusters, 'min'))
    min_test_uncertain = from_list_to_set([x for unc in min_all_uncertain for x in X_test
                                           if unc[0] == x[0] and unc[1] == x[1]])
    all_uncertain_ls.append(min_all_uncertain)
    test_uncertain_ls.append(min_test_uncertain)

    mean_or_min_all_uncertain = from_list_to_set(get_uncertain_elements(clusters, 'mean-or-min'))
    mean_or_min_test_uncertain = from_list_to_set([x for unc in mean_or_min_all_uncertain for x in X_test
                                                   if unc[0] == x[0] and unc[1] == x[1]])
    all_uncertain_ls.append(mean_or_min_all_uncertain)
    test_uncertain_ls.append(mean_or_min_test_uncertain)

    freq_all_uncertain = from_list_to_set(get_uncertain_elements(clusters, 'freq-mean'))
    freq_test_uncertain = from_list_to_set([x for unc in freq_all_uncertain for x in X_test
                                            if unc[0] == x[0] and unc[1] == x[1]])
    all_uncertain_ls.append(freq_all_uncertain)
    test_uncertain_ls.append(freq_test_uncertain)

    entropy_all_uncertain = from_list_to_set(get_uncertain_elements(clusters, 'entropy'))
    entropy_test_uncertain = from_list_to_set([x for unc in entropy_all_uncertain for x in X_test
                                               if unc[0] == x[0] and unc[1] == x[1]])
    all_uncertain_ls.append(entropy_all_uncertain)
    test_uncertain_ls.append(entropy_test_uncertain)

    silhouette_all_uncertain = from_list_to_set(get_uncertain_elements(clusters, 'silhouette'))
    silhouette_test_uncertain = from_list_to_set([x for unc in silhouette_all_uncertain for x in X_test
                                                  if unc[0] == x[0] and unc[1] == x[1]])
    all_uncertain_ls.append(silhouette_all_uncertain)
    test_uncertain_ls.append(silhouette_test_uncertain)

    test_union = set.union(svc_test_uncertain, mean_test_uncertain, min_test_uncertain, mean_or_min_test_uncertain,
                           freq_test_uncertain, entropy_test_uncertain, silhouette_test_uncertain)
    all_union = set.union(svc_all_uncertain, mean_all_uncertain, min_all_uncertain, mean_or_min_all_uncertain,
                          freq_all_uncertain, entropy_all_uncertain, silhouette_all_uncertain)
    test_uncertain_ls.append(test_union)
    all_uncertain_ls.append(all_union)

    test_intersection = set.intersection(svc_test_uncertain, mean_test_uncertain, min_test_uncertain,
                                         mean_or_min_test_uncertain, freq_test_uncertain, entropy_test_uncertain,
                                         silhouette_test_uncertain)
    all_intersection = set.intersection(svc_all_uncertain, mean_all_uncertain, min_all_uncertain,
                                        mean_or_min_all_uncertain, freq_all_uncertain, entropy_all_uncertain,
                                        silhouette_all_uncertain)
    test_uncertain_ls.append(test_intersection)
    all_uncertain_ls.append(all_intersection)

    test_intersection_3 = src.utils.intersection_3(svc_test_uncertain, mean_test_uncertain, min_test_uncertain,
                                         mean_or_min_test_uncertain, freq_test_uncertain, entropy_test_uncertain,
                                         silhouette_test_uncertain)
    all_intersection_3 = src.utils.intersection_3(svc_all_uncertain, mean_all_uncertain, min_all_uncertain,
                                        mean_or_min_all_uncertain, freq_all_uncertain, entropy_all_uncertain,
                                        silhouette_all_uncertain)
    test_uncertain_ls.append(test_intersection_3)
    all_uncertain_ls.append(all_intersection_3)

    estimator.fit(X_train, y_train)
    all_preds = estimator.predict(X)
    all_preds_index, = np.where(all_preds == 0)
    all_core_index, = np.where(all_preds == 1)
    all_preds = from_list_to_set(X[all_preds_index])
    all_core = from_list_to_set(X[all_core_index])
    test_preds = estimator.predict(X_test)
    test_preds_index, = np.where(test_preds == 0)
    test_core_index, = np.where(test_preds == 1)
    test_preds = from_list_to_set(X[test_preds_index])
    test_core = from_list_to_set(X[test_core_index])

    frames_accuracy = []
    frames_precision = []
    frames_core = []
    test_accuracy_ratios = []
    test_precision_ratios = []
    test_core_ratios = []
    all_accuracy_ratios = []
    all_precision_ratios = []
    all_core_ratios = []

    for metric_test_uncertain, metric_all_uncertain, metric_name in zip(test_uncertain_ls, all_uncertain_ls, metrics_ls):
        if len(metric_test_uncertain) == 0:
            ratio_accuracy = np.nan
            ratio_core = np.nan
        else:
            ratio_accuracy = len([pred for pred in test_preds if pred in metric_test_uncertain]) / len(metric_test_uncertain)
            ratio_core = len([pred for pred in test_core if pred in metric_test_uncertain]) / len(metric_test_uncertain)
        if metric_name != 'Union' and metric_name != 'Intersection' and metric_name != 'Intersection 3':
            test_accuracy_ratios.append(ratio_accuracy)
            test_core_ratios.append(ratio_core)
        d = {estimator_name: ratio_accuracy}
        df = pd.DataFrame(d, index=[metric_name + ' (test)'])
        frames_accuracy.append(df)

        d = {estimator_name: ratio_core}
        df = pd.DataFrame(d, index=[metric_name + ' (test)'])
        frames_core.append(df)

        if len(test_preds) == 0:
            ratio_precision = np.nan
        else:
            ratio_precision = len([pred for pred in test_preds if pred in metric_test_uncertain]) / len(test_preds)
        if metric_name != 'Union' and metric_name != 'Intersection' and metric_name != 'Intersection 3':
            test_precision_ratios.append(ratio_precision)
        d = {estimator_name: ratio_precision}
        df = pd.DataFrame(d, index=[metric_name + ' (test)'])
        frames_precision.append(df)

        if len(metric_all_uncertain) == 0:
            ratio_accuracy = np.nan
            ratio_core = np.nan
        else:
            ratio_accuracy = len([pred for pred in all_preds if pred in metric_all_uncertain]) / len(metric_all_uncertain)
            ratio_core = len([pred for pred in all_core if pred in metric_all_uncertain]) / len(metric_all_uncertain)
        if metric_name != 'Union' and metric_name != 'Intersection' and metric_name != 'Intersection 3':
            all_accuracy_ratios.append(ratio_accuracy)
            all_core_ratios.append(ratio_core)
        d = {estimator_name: ratio_accuracy}
        df = pd.DataFrame(d, index=[metric_name + ' (all)'])
        frames_accuracy.append(df)

        d = {estimator_name: ratio_core}
        df = pd.DataFrame(d, index=[metric_name + ' (all)'])
        frames_core.append(df)

        if len(all_preds) == 0:
            ratio_precision = np.nan
        else:
            ratio_precision = len([pred for pred in all_preds if pred in metric_all_uncertain]) / len(all_preds)
        if metric_name != 'Union' and metric_name != 'Intersection' and metric_name != 'Intersection 3':
            all_precision_ratios.append(ratio_precision)
        d = {estimator_name: ratio_precision}
        df = pd.DataFrame(d, index=[metric_name + ' (all)'])
        frames_precision.append(df)

    d = {estimator_name: np.array(test_accuracy_ratios).mean()}
    df = pd.DataFrame(d, index=['Test Avg'])
    frames_accuracy.insert(-6, df)

    d = {estimator_name: np.array(all_accuracy_ratios).mean()}
    df = pd.DataFrame(d, index=['All Avg'])
    frames_accuracy.insert(-6, df)

    d = {estimator_name: np.array(test_precision_ratios).mean()}
    df = pd.DataFrame(d, index=['Test Avg'])
    frames_precision.insert(-6, df)

    d = {estimator_name: np.array(all_precision_ratios).mean()}
    df = pd.DataFrame(d, index=['All Avg'])
    frames_precision.insert(-6, df)

    d = {estimator_name: np.array(test_core_ratios).mean()}
    df = pd.DataFrame(d, index=['Test Avg'])
    frames_core.insert(-6, df)

    d = {estimator_name: np.array(all_core_ratios).mean()}
    df = pd.DataFrame(d, index=['All Avg'])
    frames_core.insert(-6, df)

    return pd.concat(frames_accuracy), pd.concat(frames_precision), pd.concat(frames_core)
