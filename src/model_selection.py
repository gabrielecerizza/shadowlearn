import json
import logging
import os
import re
import warnings
from copy import deepcopy
from datetime import timedelta
from timeit import default_timer as timer

import numpy as np
import pandas as pd
from docplex.mp.utils import DOcplexException
from tqdm import trange
from sklearn.decomposition import PCA
from sklearn.exceptions import FitFailedWarning
from sklearn.metrics import make_scorer, accuracy_score, precision_score, f1_score, recall_score
from sklearn.model_selection import cross_val_score, GridSearchCV,\
    StratifiedKFold, StratifiedShuffleSplit, cross_validate
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from src.datasets import load_iris, load_breast_cancer_dataset, load_synthetic_dataset, load_wine_dataset
from src.metrics import conservative_score, non_conservative_score, svc_shadowed_score, homogeneity_score, \
    shadow_indifferent_score, shadow_half_score, shadow_accuracy, my_auc_score, my_f1_score, shadow_f1_score, \
    multiclass_auc_score, my_sensitivity_score, my_specificity_score, shadow_count, \
    shadow_ratio, my_precision_score, proba_auc_score, two_to_three_values_custom_scorer, return_custom_score
from src.shadow_learn import ShadowedSetEstimator, CompoundedShadowedSetEstimator
import src.plot
from src.utils import NumpyEncoder

my_random_state = 42


def _internal_grid_search(X_train, y_train, X_test, y_test, estimator, cv, param_grid, metric, verbose,
                          n_jobs=2, display=True, log=True, main_metric_name=None, scaled=False, variance_ratio=None,
                          dim=None, compounded=False, y1_train=None, y2_train=None, y_shadow_train=None,
                          y_shadow_test=None, cv_num=10, refit=True, target=None, additional_test_metrics=None):

    # scaling inside CV, otherwise data might leak into validation sets
    # scaling data before applying PCA
    gs, scoring = _prepare_grid_search(cv, dim, estimator, main_metric_name, metric, n_jobs, param_grid, refit, scaled,
                                       target, variance_ratio, verbose)

    original_X_train = X_train.copy()
    original_X_test = X_test.copy()

    with warnings.catch_warnings():
        warnings.simplefilter('ignore', FitFailedWarning)
        full_X_train = X_train.copy()
        full_y_train = y_train.copy()
        if compounded:
            gs.fit(X_train, y_train, estimator__y1=y1_train, estimator__y2=y2_train)
        else:
            gs.fit(X_train, y_train, estimator__full_y_shadow=(y_shadow_train, 1), estimator__cv_num=cv_num,
                   estimator__random_state=my_random_state, estimator__full_X=(full_X_train, 1), estimator__full_y=(full_y_train, 1))

    if scaled:
        scaler = gs.best_estimator_['scaler']
        # print(scaler)
        # scaler.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)

    pca = None
    if variance_ratio is not None:
        pca = PCA(variance_ratio)
    elif dim is not None:
        pca = PCA(dim)
    if pca is not None:
        pca.fit(X_train)
        X_train = pca.transform(X_train)
        X_test = pca.transform(X_test)

    try:
        final_model = gs.best_estimator_['estimator']
    except AttributeError as e:
        final_model = None

    final_model_preds = final_model.predict(X_test)
    if y_shadow_test is not None:
        # print('y_shadow_testing')
        score = accuracy_score(y_shadow_test, final_model_preds)
    else:
        score = scoring(final_model, X_test, y_test)

    additional_test_scores = {}
    if additional_test_metrics is not None:
        for metric_key, metric_name in additional_test_metrics.items():
            additional_scoring = get_scoring(metric_name, target, main_metric_name)
            additional_test_scores[metric_key] = additional_scoring(final_model, X_test, y_test)

    if log:
        logging.info('cv_results of best_index: ' + str(pd.DataFrame(gs.cv_results_).iloc[gs.best_index_]))
        logging.info('GridSearchCV: ' + str(gs))
        logging.info('best_estimator: ' + str(gs.best_estimator_))
        logging.info('score on test: ' + str(score))
        logging.info('additional test scores: ' + str(additional_test_scores))

    if display:
        print(pd.DataFrame(gs.cv_results_).iloc[gs.best_index_])
        print('score on test: ', score)

    return gs, score, X_train, X_test, original_X_train, original_X_test, additional_test_scores, final_model_preds


def _prepare_grid_search(cv, dim, estimator, main_metric_name, metric, n_jobs, param_grid, refit, scaled, target,
                         variance_ratio, verbose, additional_test_metrics=None):
    pipeline_ls = []
    if scaled:
        scaler = StandardScaler()
        pipeline_ls.append(('scaler', scaler))
    if variance_ratio is not None:
        pca = PCA(variance_ratio)
        pipeline_ls.append(('pca', pca))
    elif dim is not None:
        pca = PCA(dim)
        pipeline_ls.append(('pca', pca))
    pipeline_ls.append(('estimator', estimator))
    pipe = Pipeline(steps=pipeline_ls)
    new_param_grid = {}
    for param_name, param_values in param_grid.items():
        new_param_grid['estimator__' + param_name] = param_values
    main_scoring = get_scoring(metric, target, main_metric_name)

    scorings = {}
    if additional_test_metrics is not None:
        scorings[main_metric_name] = main_scoring
        for metric_key, metric_name in additional_test_metrics.items():
            additional_scoring = get_scoring(metric_name, target, main_metric_name)
            scorings[metric_key] = additional_scoring

    if len(scorings) > 0:
        gs = GridSearchCV(pipe,
                          cv=cv,
                          param_grid=new_param_grid,
                          scoring=scorings,
                          verbose=verbose,
                          n_jobs=n_jobs,
                          refit=main_metric_name)
    else:
        gs = GridSearchCV(pipe,
                          cv=cv,
                          param_grid=new_param_grid,
                          scoring=main_scoring,
                          verbose=verbose,
                          n_jobs=n_jobs,
                          refit=refit)
    return gs, main_scoring


def grid_search(dataset_loader, estimator, metric, param_grid,
                n_splits=1,
                test_size=0.3,
                cv_num=10,
                scaled=False,
                verbose=1,
                n_jobs=2,
                display=True,
                log=True,
                main_metric_name=None,
                variance_ratio=None,
                dim=None,
                compounded=False,
                shadow_test=False,
                additional_test_metrics=None,
                refit=True):
    splitter = StratifiedShuffleSplit(n_splits=n_splits, test_size=test_size, random_state=my_random_state)
    cv = StratifiedKFold(cv_num, random_state=my_random_state, shuffle=True)

    if compounded:
        X, y1, y2, labels, column_names = dataset_loader()
    elif shadow_test:
        X, y, y_shadow, labels, shadow_labels, target, column_names = dataset_loader()
    else:
        X, y, labels, target, column_names = dataset_loader()

    if log:
        logging.info(str(target) + ' ' + str(metric) + ' metric grid search')
        logging.info('dataset_loader: ' + str(dataset_loader))
        logging.info('estimator: ' + str(estimator))
        logging.info('metric: ' + str(metric))
        logging.info('param_grid: ' + str(param_grid))
        logging.info('splitter: ' + str(splitter))
        logging.info('cv: ' + str(cv))
        logging.info('scaled: ' + str(scaled))
        logging.info('verbose: ' + str(verbose))
        logging.info('n_jobs: ' + str(n_jobs))
        logging.info('display: ' + str(display))
        logging.info('log: ' + str(log))
        logging.info('main metric name: ' + str(main_metric_name))
        logging.info('variance ratio: ' + str(variance_ratio))
        logging.info('dim: ' + str(dim))

    scores = []
    additional_test_scores = []
    original_X_trains = []
    original_X_tests = []
    X_trains = []
    y_trains = []
    X_tests = []
    y_tests = []
    label_trains = []
    label_tests = []
    y1_trains = []
    y2_trains = []
    y_shadow_trains = []
    y_shadow_tests = []
    preds = []

    for i, (train_index, test_index) in enumerate(splitter.split(X, labels)):
        if compounded:
            X_train = X[train_index]
            X_test = X[test_index]
            y1_train = y1[train_index]
            y2_train = y2[train_index]
            y_train = pd.Series(labels[train_index]).apply(
                lambda x: 2 if x == 'Iris-versicolor' else 1 if x == 'Iris-virginica' else -1)
            y_train = np.array(y_train)
            y_test = pd.Series(labels[test_index]).apply(
                lambda x: 2 if x == 'Iris-versicolor' else 1 if x == 'Iris-virginica' else -1)
            y_test = np.array(y_test)
            label_train = labels[train_index]
            label_test = labels[test_index]
            y_shadow_train = None
            y_shadow_test = None
            target = 'compounded'
        else:
            if shadow_test:
                y_shadow_train = y_shadow[train_index]
                y_shadow_test = y_shadow[test_index]
            else:
                y_shadow_train = None
                y_shadow_test = None
            X_train = X[train_index]
            y_train = y[train_index]
            label_train = labels[train_index]
            X_test = X[test_index]
            y_test = y[test_index]
            label_test = labels[test_index]
            y1_train = None
            y2_train = None

        if display:
            print('***** Split n. ' + str(i))

        gs, score, X_train, X_test, original_X_train, original_X_test, internal_additional_test_scores, test_preds = _internal_grid_search(X_train, y_train, X_test, y_test, estimator,
                                                           cv, param_grid, metric, verbose, n_jobs, display, log,
                                                           main_metric_name, scaled, variance_ratio, dim,
                                                           compounded=compounded, y1_train=y1_train, y2_train=y2_train,
                                                           cv_num=cv_num, y_shadow_train=y_shadow_train,
                                                           y_shadow_test=y_shadow_test, refit=refit, target=target,
                                                           additional_test_metrics=additional_test_metrics)

        original_X_trains.append(original_X_train)
        original_X_tests.append(original_X_test)
        X_trains.append(X_train)
        y_trains.append(y_train)
        X_tests.append(X_test)
        y_tests.append(y_test)
        label_trains.append(label_train)
        label_tests.append(label_test)
        y1_trains.append(y1_train)
        y2_trains.append(y2_train)
        y_shadow_trains.append(y_shadow_train)
        y_shadow_tests.append(y_shadow_test)
        preds.append(test_preds)

        if log:
            logging.info('X_train: ' + str(X_train))
            logging.info('y_train: ' + str(y_train))
            logging.info('label_train: ' + str(label_train))
            logging.info('X_test: ' + str(X_test))
            logging.info('y_test: ' + str(y_test))
            logging.info('label_test: ' + str(label_test))
            logging.info('***** Split n. ' + str(i))

        if len(scores) == 0:
            best_gs = gs
        elif score > max(scores):
            best_gs = gs

        scores.append(score)
        additional_test_scores.append(internal_additional_test_scores)

    scores = np.array(scores)
    sets = {'X_trains': X_trains, 'y_trains': y_trains, 'X_tests': X_tests,
            'y_tests': y_tests, 'label_trains': label_trains, 'label_tests': label_tests,
            'y1_trains': y1_trains, 'y2_trains': y2_trains, 'y_shadow_trains': y_shadow_trains,
            'y_shadow_tests': y_shadow_tests, 'original_X_trains': original_X_trains,
            'original_X_tests': original_X_tests, 'preds': preds}

    metric_str = 'compounded' if compounded else get_metric_str(metric, main_metric_name)

    # concatenated_X needed when Xs are scaled inside grid_search
    # with one split, scores.mean() is just the best test score
    return {'best_gs': best_gs, 'last_gs': gs, 'scores': (scores.mean(), scores.std(), scores),
            'additional_test_scores': additional_test_scores,
            'sets': sets, 'concatenated_X': np.concatenate((X_trains, X_tests), axis=1),
            'concatenated_y': np.concatenate((y_trains, y_tests), axis=1),
            'concatenated_labels': np.concatenate((label_trains, label_tests), axis=1),
            'metric': metric,
            'metric_str': metric_str,
            'target': target,
            'target_str': str(target),
            'scaled': scaled,
            'variance_ratio': variance_ratio,
            'dim': dim,
            'suptitle': str(target) + ' shadowed set best model for ' + metric_str + ' metric'}


def get_metric_str(metric, main_metric_name):
    if isinstance(metric, str):
        return metric
    elif isinstance(metric, dict):
        return main_metric_name
    else:
        fname, = re.match(r'(.*?)\..*?', metric.__qualname__).groups()
        return fname


def score_zoom(gs_res, fixed=None, param_grid=None, plot=True,
               cv_num=10,
               verbose=1,
               n_jobs=2,
               log=True,
               compounded=False,
               plotly=False):

    metric = gs_res['metric']
    target = gs_res['target']
    best_estimator = gs_res['best_gs'].best_estimator_['estimator']
    best_params = {}
    temp_best_params = gs_res['best_gs'].best_params_
    for param_name, param_values in temp_best_params.items():
        actual_param_name, = re.match(r'estimator__(.*)', param_name).groups()
        best_params[actual_param_name] = param_values

    # print(best_params)

    fixed_params_dict = {}
    if fixed is not None:
        for param in fixed:
            if param in param_grid:
                raise ValueError('A fixed param is also in param_grid')
            fixed_params_dict[param] = best_params[param]
    if compounded:
        estimator = CompoundedShadowedSetEstimator()
        estimator.my_set_params(fixed_params_dict)
    else:
        estimator = ShadowedSetEstimator(solver=best_estimator.get_solver_type())
        estimator.my_set_params(fixed_params_dict)

    for param in param_grid:
        param_grid[param] = np.sort(np.concatenate((param_grid[param], [best_params[param]])))
    # print('param grid: ', param_grid)

    cv = StratifiedKFold(cv_num, random_state=my_random_state, shuffle=True)

    sets = gs_res['sets']
    if param_grid is None:
        # TODO: automatizzazione linspace
        raise NotImplementedError

    X_train = sets['original_X_trains'][0]
    y_train = sets['y_trains'][0]
    X_test = sets['original_X_tests'][0]
    y_test = sets['y_tests'][0]
    y1_train = sets['y1_trains'][0]
    y2_train = sets['y2_trains'][0]
    y_shadow_train = sets['y_shadow_trains'][0]
    y_shadow_test = sets['y_shadow_tests'][0]

    scaled = gs_res['scaled']
    variance_ratio = gs_res['variance_ratio']
    dim = gs_res['dim']

    new_gs, _, _, _, _, _, _, _ = _internal_grid_search(X_train, y_train, X_test, y_test, estimator,
                                            cv, param_grid, metric, verbose, n_jobs=n_jobs, log=log,
                                            scaled=scaled, variance_ratio=variance_ratio, dim=dim,
                                            compounded=compounded, y1_train=y1_train, y2_train=y2_train,
                                            cv_num=cv_num, y_shadow_train=y_shadow_train,
                                            y_shadow_test=y_shadow_test, target=target)

    keys = sorted(param_grid.keys())

    if plot:
        if plotly:
            src.plot.plotly_score_surface(param_grid[keys[0]], param_grid[keys[1]], new_gs, 'score', keys[0], keys[1])
        else:
            src.plot.plot_score_surface(param_grid[keys[0]], param_grid[keys[1]], new_gs, 'score', keys[0], keys[1])
        src.plot.plot_score_heatmap(param_grid[keys[0]], param_grid[keys[1]], new_gs, 'score', keys[1], keys[0])

    if log:
        logging.info(gs_res['target_str'] + ' ' + str(metric) + ' score zoom with fixed ' + str(fixed))
        logging.info('best_params: ' + str(best_params))
        logging.info('param_grid: ' + str(param_grid))
        logging.info('new_gs: ' + str(new_gs))

    return new_gs


def nested_cv(inner_n_splits, outer_n_splits, dataset_loader, estimator, metric, param_grid,
              scaled=False,
              verbose=1,
              n_jobs=2,
              dim=None,
              main_metric_name=None,
              refit=True,
              target=None,
              variance_ratio=None,
              additional_test_metrics=None,
              fit_params=None,
              log=True,
              shadow_test=False,
              feature_selector=None):
    if shadow_test:
        X, y, y_shadow, labels, shadow_labels, target, column_names = dataset_loader()
    else:
        X, y, labels, target, column_names = dataset_loader()
    cv_inner = StratifiedKFold(n_splits=inner_n_splits, shuffle=True, random_state=my_random_state)
    cv_outer = StratifiedKFold(n_splits=outer_n_splits, shuffle=True, random_state=my_random_state)

    '''
    gs, scoring = _prepare_grid_search(cv=cv_inner, dim=dim, estimator=estimator, main_metric_name=main_metric_name,
                                       metric=metric, n_jobs=n_jobs, param_grid=param_grid, refit=refit,
                                       scaled=scaled, additional_test_metrics=additional_test_metrics,
                                       target=target, variance_ratio=variance_ratio, verbose=verbose)
    '''

    scorings = {main_metric_name: get_scoring(metric, target, main_metric_name)}
    if additional_test_metrics is not None:
        for metric_key, metric_name in additional_test_metrics.items():
            scorings[metric_key] = get_scoring(metric_name, target, main_metric_name)

    pipeline_ls = []
    if scaled and feature_selector is None:
        scaler = StandardScaler()
        pipeline_ls.append(('scaler', scaler))
    if variance_ratio is not None:
        pca = PCA(variance_ratio)
        pipeline_ls.append(('pca', pca))
    elif dim is not None:
        pca = PCA(dim)
        pipeline_ls.append(('pca', pca))
    # elif feature_selector is not None:
    #    pipeline_ls.append(('feature_selector', feature_selector))
    pipeline_ls.append(('estimator', estimator))
    pipe = Pipeline(steps=pipeline_ls)
    new_param_grid = {}
    for param_name, param_values in param_grid.items():
        new_param_grid['estimator__' + param_name] = param_values

    gs = GridSearchCV(estimator=pipe,
                      param_grid=new_param_grid, scoring=scorings,
                      cv=cv_inner, refit=main_metric_name, n_jobs=n_jobs, verbose=verbose)

    with warnings.catch_warnings():
        warnings.simplefilter('ignore', FitFailedWarning)

        best_feature_selectors = []
        best_scalers = []

        if shadow_test or feature_selector is not None:
            main_scores = []
            cv_scores = {
                'estimator': []
            }
            for scoring in scorings:
                cv_scores['test_' + scoring] = []

            for train_index, test_index in cv_outer.split(X, labels):
                if shadow_test:
                    y_shadow_train = y_shadow[train_index]
                    y_shadow_test = y_shadow[test_index]
                else:
                    y_shadow_train = None
                    y_shadow_test = None
                X_train = X[train_index]
                y_train = y[train_index]
                label_train = labels[train_index]
                X_test = X[test_index]
                y_test = y[test_index]
                label_test = labels[test_index]
                if feature_selector is not None:
                    scaler = StandardScaler()
                    scaler.fit(X_train)
                    best_scalers.append(deepcopy(scaler))
                    X_train = scaler.transform(X_train)
                    X_test = scaler.transform(X_test)
                    feature_selector.fit(X_train, y_train)
                    X_train = feature_selector.transform(X_train)
                    X_test = feature_selector.transform(X_test)
                    best_feature_selectors.append(deepcopy(feature_selector))
                else:
                    best_feature_selectors.append(None)

                full_X_train = X_train.copy()
                full_y_train = y_train.copy()

                fit_params = {
                    'estimator__full_y_shadow': (y_shadow_train, 1),
                    'estimator__cv_num': inner_n_splits,
                    'estimator__random_state': my_random_state,
                    'estimator__full_X': (full_X_train, 1),
                    'estimator__full_y': (full_y_train, 1)
                }

                gs.fit(X_train, y_train, **fit_params)

                try:
                    final_model = gs.best_estimator_['estimator']
                    cv_scores['estimator'].append(deepcopy(gs))
                except AttributeError as e:
                    final_model = None

                if scaled and feature_selector is None:
                    scaler = gs.best_estimator_['scaler']
                    # X_train = scaler.transform(X_train)
                    X_test = scaler.transform(X_test)
                if variance_ratio is not None or dim is not None:
                    pca = gs.best_estimator_['pca']
                    # X_train = pca.transform(X_train)
                    X_test = pca.transform(X_test)
                # elif feature_selector is not None:
                #    fs = gs.best_estimator_['feature_selector']
                #    X_test = fs.transform(X_test)

                for scoring_name, scoring_metric in scorings.items():
                    if scoring_name == 'shadow-accuracy':
                        final_model_preds = final_model.predict(X_test)
                        cv_scores['test_' + scoring_name].append(accuracy_score(y_shadow_test, final_model_preds))
                    elif scoring_name == 'shadow-f1-score':
                        final_model_preds = final_model.predict(X_test)
                        cv_scores['test_' + scoring_name].append(f1_score(y_shadow_test, final_model_preds,
                                                                          average='weighted'))
                    elif scoring_name == 'custom-exclusion-recall' or scoring_name == 'custom-shadow-recall' \
                            or scoring_name == 'custom-core-recall' or scoring_name == 'custom-exclusion-precision' \
                            or scoring_name == 'custom-shadow-precision' or scoring_name == 'custom-core-precision' \
                            or scoring_name == 'custom-multi-class-auc' or scoring_name == 'custom-f1-score':
                        final_model_preds = final_model.predict(X_test)
                        cv_scores['test_' + scoring_name].append(return_custom_score(y_shadow_test, final_model_preds,
                                                                                     scoring_name))
                    else:
                        cv_scores['test_' + scoring_name].append(scoring_metric(final_model, X_test, y_test))

            for cv_score_key, cv_score_values in cv_scores.items():
                if cv_score_key != 'estimator':
                    cv_scores[cv_score_key] = np.array(cv_score_values)

        else:
            cv_scores = cross_validate(estimator=gs, X=X, y=y,
                                       scoring=scorings,
                                       cv=cv_outer,
                                       verbose=verbose,
                                       fit_params=fit_params,
                                       n_jobs=n_jobs,
                                       return_estimator=refit)

            best_feature_selectors = [None for _ in range(outer_n_splits)]

        print('nested_cv: CV_SCORES')
        print(cv_scores)
        print('nested_cv: TARGET')
        print(target)

        best_estimators = []
        best_pcas = []
        if refit:
            for est in cv_scores['estimator']:
                best_estimators.append(est.best_estimator_['estimator'])
                if feature_selector is None:
                    if scaled:
                        best_scalers.append(est.best_estimator_['scaler'])
                    else:
                        best_scalers.append(None)
                if dim is not None or variance_ratio is not None:
                    best_pcas.append(est.best_estimator_['pca'])
                else:
                    best_pcas.append(None)
                # if feature_selector is not None:
                #    best_feature_selectors.append(est.best_estimator_['feature_selector'])
                # else:
                #    best_feature_selectors.append(None)

        all_shadow_indices = []
        all_preds = []
        all_column_names = []
        if refit:
            for best_est, best_scaler, best_pca, best_feature_selector \
                    in zip(best_estimators, best_scalers, best_pcas, best_feature_selectors):
                new_X = deepcopy(X)
                if best_scaler is not None:
                    new_X = best_scaler.transform(new_X)
                if best_feature_selector is not None:
                    new_X = best_feature_selector.transform(new_X)
                    all_column_names.append(np.array(column_names[best_feature_selector.support_]))
                else:
                    all_column_names.append(np.array(['None']))
                if best_pca is not None:
                    new_X = best_pca.transform(new_X)
                try:
                    preds = np.array(best_est.predict(new_X))
                except AttributeError as ae:
                    print('nested_cv: caught error while making predictions with best estimator: ', str(ae))
                    # preds = np.array([np.nan])
                    raise ae
                all_preds.append(preds)
                shadow_indices, = np.where(preds == 0)
                all_shadow_indices.append(shadow_indices)

    if log:
        logging.info('Called src.model_selection nested_cv')
        logging.info('dataset_loader: ' + str(dataset_loader))
        logging.info('estimator: ' + str(estimator))
        logging.info('metric: ' + str(metric))
        logging.info('param_grid: ' + str(param_grid))
        logging.info('cv_inner: ' + str(cv_inner))
        logging.info('cv_outer: ' + str(cv_outer))
        logging.info('GridSearchCV: ' + str(gs))
        logging.info('X: ' + str(X))
        logging.info('y: ' + str(y))
        logging.info('labels: ' + str(labels))
        logging.info('target: ' + str(target))
        logging.info('scores: ' + str(cv_scores))

    return {'mean': cv_scores['test_' + main_metric_name].mean(), 'std': cv_scores['test_' + main_metric_name].std(),
            'scores': cv_scores, 'best_estimators': best_estimators,
            'all_shadow_indices': np.array(all_shadow_indices, dtype=object), 'all_preds': np.array(all_preds, dtype=object),
            'all_column_names': np.array(all_column_names, dtype=object)}


def get_scoring(metric, target, main_metric_name=None):
    score_types = ['conservative', 'non-conservative', 'svc-shadowed', 'homogeneity', 'shadow-indifferent',
                   'shadow-half', 'shadow-accuracy', 'accuracy', 'precision', 'custom-precision', 'auc',
                   'proba-auc', 'multi-class-auc', 'certain-f1-score', 'full-f1-score', 'f1-score',
                   'specificity', 'sensitivity', 'shadow-recall', 'shadow-ratio', 'shadow-count', 
                   'shadow-f1-score', 'custom-exclusion-recall', 'custom-shadow-recall', 'custom-core-recall',
                   'custom-exclusion-precision', 'custom-shadow-precision', 'custom-core-precision',
                   'custom-multi-class-auc', 'custom-f1-score']
    if isinstance(metric, str):
        if metric == 'conservative':
            scoring = make_scorer(conservative_score)
        elif metric == 'non-conservative':
            scoring = make_scorer(non_conservative_score)
        elif metric == 'svc-shadowed':
            scoring = svc_shadowed_score
        elif metric == 'homogeneity':
            scoring = make_scorer(homogeneity_score)
        elif metric == 'shadow-indifferent':
            scoring = make_scorer(shadow_indifferent_score)
        elif metric == 'shadow-half':
            scoring = make_scorer(shadow_half_score)
        elif metric == 'shadow-accuracy':
            scoring = shadow_accuracy
        elif metric == 'accuracy':
            scoring = make_scorer(accuracy_score)
        elif metric == 'precision':
            # labels=[target] is wrong, must be labels=[1], since target is labeled 1
            scoring = make_scorer(precision_score, labels=[1], average='weighted')
        elif metric == 'custom-precision':
            scoring = make_scorer(my_precision_score, main_metric_name=main_metric_name)
        elif metric == 'auc':
            scoring = make_scorer(my_auc_score, main_metric_name=main_metric_name)
        elif metric == 'proba-auc':
            scoring = make_scorer(proba_auc_score)
        elif metric == 'multi-class-auc':
            scoring = make_scorer(multiclass_auc_score)
        elif metric == 'certain-f1-score':
            # not considering labels 0 (uncertain predictions)
            scoring = make_scorer(f1_score, labels=[-1, 1], average='weighted')
        elif metric == 'full-f1-score':
            scoring = make_scorer(f1_score, labels=[-1, 0, 1], average='weighted')
        elif metric == 'f1-score':
            scoring = make_scorer(my_f1_score, main_metric_name=main_metric_name)
        elif metric == 'sensitivity':
            scoring = make_scorer(my_sensitivity_score, main_metric_name=main_metric_name)
            # scoring = make_scorer(recall_score, labels=[1], average='weighted')
        elif metric == 'specificity':
            scoring = make_scorer(my_specificity_score, main_metric_name=main_metric_name)
            # scoring = make_scorer(recall_score, labels=[-1], average='weighted')
        elif metric == 'shadow-recall':
            scoring = make_scorer(recall_score, labels=[0], average='weighted')
        elif metric == 'custom-exclusion-recall' or metric == 'custom-shadow-recall' or metric == 'custom-core-recall' \
                or metric == 'custom-exclusion-precision' or metric == 'custom-shadow-precision' \
                or metric == 'custom-core-precision' or metric == 'custom-multi-class-auc' \
                or metric == 'custom-f1-score':
            scoring = lambda estimator, X, y: two_to_three_values_custom_scorer(estimator, X, y, metric)
        elif metric == 'shadow-ratio':
            scoring = make_scorer(shadow_ratio)
        elif metric == 'shadow-count':
            scoring = make_scorer(shadow_count)
        elif metric == 'shadow-f1-score':
            scoring = shadow_f1_score
        else:
            raise ValueError(
                f"When 'metric' is a string, it should be one of {score_types}."
                f"Got '{metric}' instead."
            )
    else:
        scoring = metric
    return scoring


def component_analysis(dimensions, inner_n_splits, outer_n_splits, dataset_loader,
                       estimator, metric, param_grid,
                       scaled=False,
                       verbose=1,
                       n_jobs=2,
                       log=True,
                       main_metric_name=None):
    results = []
    for i in trange(len(dimensions), ascii=True):
        result = nested_cv(inner_n_splits, outer_n_splits, dataset_loader,
                           estimator, metric, param_grid, scaled=scaled, verbose=verbose, n_jobs=n_jobs, log=log,
                           dim=dimensions[i], main_metric_name=main_metric_name)
        # del result['scores']
        results.append(result)

    print(results)
    index = pd.Series(dimensions).apply(lambda x: str(x) + 'd')
    return pd.DataFrame(results, index=index)


def model_comparison(dataset, estimators, metric, param_grids,
                     n_splits=1,
                     test_size=0.3,
                     cv_num=10,
                     scaled=False,
                     verbose=1,
                     n_jobs=2,
                     display=False,
                     nested=False,
                     dim=None,
                     main_metric_name=None,
                     refit=True,
                     variance_ratio=None,
                     additional_test_metrics=None,
                     fit_params=None,
                     separate_targets=False,
                     op='w',
                     directory='\\csv\\',
                     filename='',
                     shadow_test=False,
                     feature_selector=None,
                     log=True):

    if op == 'r':
        # TODO: read csv here?
        return None

    results = []
    targets = []
    separate_targets_results = []
    all_shadow_indices_results = {}
    all_preds_results = {}
    all_column_names_results = {}
    for est in estimators:
        all_shadow_indices_results[est.get_solver_name()] = {}
        all_preds_results[est.get_solver_name()] = {}
        all_column_names_results[est.get_solver_name()] = {}

    csv_path = os.getcwd() + directory + filename
    csv_filename = csv_path + ('_separate.csv' if separate_targets else '.csv')
    json_filename = os.getcwd() + '\\json\\' + filename

    if nested:
        for i in trange(len(estimators), ascii=True):
            dataset_loaders, targets = get_dataset_loaders(dataset)

            start = timer()
            estimator = estimators[i]
            param_grid = param_grids[i]

            estimator_results = {}
            estimator_target_results = {}

            for dataset_loader, target in zip(dataset_loaders, targets):
                target_start = timer()

                try:
                    cv_scores = nested_cv(cv_num, n_splits, dataset_loader, estimator, metric, param_grid,
                                          scaled=scaled, verbose=verbose, n_jobs=n_jobs, dim=dim,
                                          main_metric_name=main_metric_name, refit=refit, target=target,
                                          variance_ratio=variance_ratio,
                                          additional_test_metrics=additional_test_metrics,
                                          fit_params=fit_params, shadow_test=shadow_test,
                                          feature_selector=feature_selector, log=log)

                    print('model_comparison: DATASET LOADER, TARGET')
                    print(dataset_loader, target)
                    print('model_comparison: CV_SCORES')
                    print(cv_scores)

                    for cv_scores_key, cv_scores_item in cv_scores['scores'].items():
                        if cv_scores_key.startswith('test_'):
                            estimator_results[str(target) + ' - test ' + cv_scores_key[5:] + ' mean'] = cv_scores_item.mean()
                            estimator_results[str(target) + ' - test ' + cv_scores_key[5:] + ' std'] = cv_scores_item.std()
                            estimator_target_results['test ' + cv_scores_key[5:] + ' mean'] = cv_scores_item.mean()
                            estimator_target_results['test ' + cv_scores_key[5:] + ' std'] = cv_scores_item.std()

                    all_shadow_ratios = []
                    for preds, shadow_indices in zip(cv_scores['all_preds'], cv_scores['all_shadow_indices']):
                        ratio = len(shadow_indices) / len(preds)
                        all_shadow_ratios.append(ratio)
                    all_shadow_ratios = np.array(all_shadow_ratios)

                    len_shadow_indices = np.array([len(shadow_indices) for shadow_indices in cv_scores['all_shadow_indices']])

                    estimator_results[str(target) + ' - ' + 'all shadow ratio mean'] = all_shadow_ratios.mean()
                    estimator_results[str(target) + ' - ' + 'all shadow ratio std'] = all_shadow_ratios.std()
                    estimator_results[str(target) + ' - ' + 'all shadow count mean'] = len_shadow_indices.mean()
                    estimator_results[str(target) + ' - ' + 'all shadow count std'] = len_shadow_indices.std()
                    estimator_target_results['all shadow ratio mean'] = all_shadow_ratios.mean()
                    estimator_target_results['all shadow ratio std'] = all_shadow_ratios.std()
                    estimator_target_results['all shadow count mean'] = len_shadow_indices.mean()
                    estimator_target_results['all shadow count std'] = len_shadow_indices.std()

                    all_shadow_indices_results[estimator.get_solver_name()][target] = cv_scores['all_shadow_indices']
                    all_preds_results[estimator.get_solver_name()][target] = cv_scores['all_preds']
                    all_column_names_results[estimator.get_solver_name()][target] = cv_scores['all_column_names']

                except DOcplexException as de:
                    estimator_results[str(target) + ' - ' + main_metric_name + ' mean'] = np.nan
                    estimator_results[str(target) + ' - ' + main_metric_name + ' std'] = np.nan
                    estimator_target_results[main_metric_name + ' mean'] = np.nan
                    estimator_target_results[main_metric_name + ' std'] = np.nan

                target_end = timer()
                td = timedelta(seconds=target_end - target_start)
                td = timedelta(seconds=td.seconds)
                estimator_target_results['time'] = str(td)
                separate_targets_results.append(estimator_target_results.copy())

            end = timer()
            td = timedelta(seconds=end - start)
            td = timedelta(seconds=td.seconds)  # to remove microseconds
            estimator_results['time'] = str(td)

            results.append(estimator_results)

        if op == 'w':
            with open(json_filename + '_shadow_indices.json', 'w') as f:
                f.write(json.dumps(all_shadow_indices_results, cls=NumpyEncoder))

            with open(json_filename + '_preds.json', 'w') as f:
                f.write(json.dumps(all_preds_results, cls=NumpyEncoder))

            with open(json_filename + '_column_names.json', 'w') as f:
                f.write(json.dumps(all_column_names_results, cls=NumpyEncoder))

        if separate_targets:
            index = np.array([[estimator.get_solver_name() + ' (target = ' + str(target) + ')' for target in targets]
                              for estimator in estimators]).flatten()
            df = pd.DataFrame(separate_targets_results, index=index)
            if op == 'w':
                df.to_csv(csv_filename)
            return df
        else:
            index = [estimator.get_solver_name() for estimator in estimators]
            df = pd.DataFrame(results, index=index)
            if op == 'w':
                df.to_csv(csv_filename)
            return df
    else:
        for i in trange(len(estimators), ascii=True):
            # need to repeat this, because generators are empty after one loop
            dataset_loaders, targets = get_dataset_loaders(dataset)
            start = timer()
            estimator = estimators[i]
            param_grid = param_grids[i]

            gs_results = []
            gs_results_scores = []
            gs_results_std = []
            for dataset_loader in dataset_loaders:
                try:
                    res = grid_search(dataset_loader, estimator, metric, param_grid,
                                      n_splits, test_size, cv_num, scaled,
                                      verbose, n_jobs, display=display, log=log)['scores']
                    res_score = res[0]
                    res_std = res[1]
                    gs_results.append(res)
                    gs_results_scores.append(res_score)
                    gs_results_std.append(res_std)
                except DOcplexException as de:
                    gs_results.append(np.nan)
                    gs_results_scores.append(np.nan)
                    gs_results_std.append(np.nan)

            result = {}
            for j, target in enumerate(targets):
                result[target] = gs_results_scores[j]
                result[target + ' std'] = gs_results_std[j]
            end = timer()
            td = timedelta(seconds=end - start)
            td = timedelta(seconds=td.seconds)  # to remove microseconds
            result['time'] = str(td)
            results.append(result)

    index = [estimator.get_solver_name() for estimator in estimators]
    if log:
        logging.info('model comparison results: ' + str(pd.DataFrame(results, index=index)))
    return pd.DataFrame(results, index=index)


def get_dataset_loaders(dataset):
    dataset_types = ['iris', 'synthetic', 'breast_cancer', 'wine']
    if isinstance(dataset, str):
        if dataset == 'iris':
            targets = ['Iris-virginica', 'Iris-versicolor', 'Iris-setosa']
            dataset_loaders = (lambda: load_iris(d='all', target=target) for target in targets)  # bugs if lists are used
        elif dataset == 'synthetic':
            targets = ['red', 'blue']
            dataset_loaders = (lambda: load_synthetic_dataset(target=target) for target in targets)
        elif dataset == 'breast cancer':
            targets = ['malignant', 'benign']
            dataset_loaders = (lambda: load_breast_cancer_dataset(d='all', target=target) for target in targets)
        elif dataset == 'wine':
            targets = ['class_0', 'class_1', 'class_2']
            dataset_loaders = (lambda: load_wine_dataset(d='all', target=target) for target in targets)
        else:
            raise ValueError(
                f"When 'dataset' is a string, it should be one of {dataset_types}."
                f"Got '{dataset}' instead."
            )
    else:
        dataset_loaders = dataset[0]()
        targets = dataset[1]
    return dataset_loaders, targets
