import logging
import traceback

import docplex.mp.model as cpx
import gurobipy as gp
import numpy as np
import tensorflow as tf
import tqdm
from abc import ABCMeta, abstractmethod

from docplex.mp.utils import DOcplexException
from gurobipy import GRB

from src.exceptions import NoSupportVectorException, SecondPhaseErrorException


class Solver(metaclass=ABCMeta):

    def __init__(self, k, tol, X_c, X_nc, X_s=None):
        self.k = k
        self.tol = tol
        self.X_c = X_c
        self.X_nc = X_nc
        self.X_c_indices = range(len(self.X_c))
        self.X_nc_indices = range(len(self.X_nc))

        if X_s is not None:
            self.X_s = X_s
            self.X_s_indices = range(len(self.X_s))

    # TODO: remove _ prefix from method name
    @abstractmethod
    def _get_distance_from_center(self, x):
        pass

    def solve(self):
        return self._internal_solve()

    @abstractmethod
    def _internal_solve(self):
        pass

    def _converge(self, n, c):
        if (n - 0) < self.tol:
            return 0
        elif (c - n) < self.tol:
            return c
        else:
            return n

    @abstractmethod
    def _estimation_function(self, x):
        pass

    # TODO: repr


class TwoPhasesSolver(Solver, metaclass=ABCMeta):

    def __init__(self, C0, C1, k, tol, X_c, X_nc):
        self.C0 = C0
        self.C1 = C1
        Solver.__init__(self, k, tol, X_c, X_nc)

    def _internal_solve(self):
        omega_star = self._solve_first_phase()
        self.omega_star = omega_star
        fixed_term = self._compute_fixed_term(omega_star)
        self.fixed_term = fixed_term
        sq_r1 = self._get_squared_r1()
        self.sq_r1 = sq_r1
        self.sq_r0 = self._solve_second_phase()

        return self._estimation_function

    def _get_distance_from_center(self, x):
        omega_star = self.omega_star
        k = self.k
        X_c = self.X_c
        X_c_indices = self.X_c_indices
        fixed_term = self.fixed_term
        return k(x, x) - 2 * sum([omega_star[h] * k(X_c[h], x) for h in X_c_indices]) + fixed_term

    def _get_squared_r1(self):
        X_c = self.X_c
        omega_star = np.array(self.omega_star)
        i, = np.where((0 < omega_star) & (omega_star < self.C1))
        if not i.size:
            return 0
        return np.mean([self._get_distance_from_center(el) for el in X_c[i]])

    def _compute_fixed_term(self, omega_star):
        k = self.k
        X_c = self.X_c
        X_c_indices = self.X_c_indices
        return sum([omega_star[h] * omega_star[j] * k(X_c[h], X_c[j]) for h in X_c_indices for j in X_c_indices])

    def _estimation_function(self, x):
        # print(f'estimation: x={x}')
        distance = self._get_distance_from_center(x)
        if distance <= self.sq_r1:
            return 1
        elif distance >= self.sq_r0:
            return -1
        else:
            return 0

    @abstractmethod
    def _solve_first_phase(self):
        pass

    @abstractmethod
    def _solve_second_phase(self):
        pass

    def __str__(self):
        return str(self.k) + fr', $C_0 = {self.C0:.2f}$, $C_1 = {self.C1:.2f}$'

    def get_plotly_string(self):
        return self.k.get_plotly_string() + fr', C_0 = {self.C0:.2f}, C_1 = {self.C1:.2f}'


class TwoPhasesShadowSolver(Solver):
    def __init__(self, C0, C1, k, tol, X_c, X_nc, X_s):
        self.C0 = C0
        self.C1 = C1
        self.X_s = X_s
        Solver.__init__(self, k, tol, X_c, X_nc, X_s=X_s)

    def _internal_solve(self):
        omega_star = self._solve_first_phase()
        self.omega_star = omega_star
        fixed_term = self._compute_fixed_term(omega_star)
        self.fixed_term = fixed_term
        sq_r0 = self._get_squared_r0()
        self.sq_r0 = sq_r0
        self.sq_r1 = self._solve_second_phase()

        return self._estimation_function

    def _get_distance_from_center(self, x):
        omega_star = self.omega_star
        k = self.k
        X_s = self.X_s
        X_s_indices = self.X_s_indices
        fixed_term = self.fixed_term
        return k(x, x) - 2 * sum([omega_star[h] * k(X_s[h], x) for h in X_s_indices]) + fixed_term

    def _get_squared_r0(self):
        X_s = self.X_s
        omega_star = np.array(self.omega_star)
        i, = np.where((0 < omega_star) & (omega_star < self.C0))
        if not i.size:
            return 0
        return np.mean([self._get_distance_from_center(el) for el in X_s[i]])

    def _compute_fixed_term(self, omega_star):
        k = self.k
        X_s = self.X_s
        X_s_indices = self.X_s_indices
        return sum([omega_star[h] * omega_star[j] * k(X_s[h], X_s[j]) for h in X_s_indices for j in X_s_indices])

    def _estimation_function(self, x):
        # print(f'estimation: x={x}')
        distance = self._get_distance_from_center(x)
        if distance <= self.sq_r1:
            return 1
        elif distance >= self.sq_r0:
            return -1
        else:
            return 0

    @abstractmethod
    def _solve_first_phase(self):
        pass

    @abstractmethod
    def _solve_second_phase(self):
        pass

    def __str__(self):
        return str(self.k) + fr', $C_0 = {self.C0:.2f}$, $C_1 = {self.C1:.2f}$'

    def get_plotly_string(self):
        return self.k.get_plotly_string() + fr', C_0 = {self.C0:.2f}, C_1 = {self.C1:.2f}'


class GurobiTwoPhasesSolver(TwoPhasesSolver):

    def _solve_first_phase(self):
        C1 = self.C1
        k = self.k
        X_c = self.X_c
        X_c_indices = self.X_c_indices

        m = gp.Model('first-phase')
        m.setParam('OutputFlag', 0)
        omega = m.addVars(X_c_indices, name='omega', ub=C1)
        m.addConstr((gp.quicksum(omega) == 1), name='sum_constraint')

        first_summation = gp.quicksum(omega[h] * k(X_c[h], X_c[h]) for h in X_c_indices)
        second_summation = gp.quicksum(
            omega[h] * omega[j] * k(X_c[h], X_c[j]) for h in X_c_indices for j in X_c_indices)
        m.setObjective(first_summation - second_summation, GRB.MAXIMIZE)

        m.optimize()

        try:
            return [self._converge(value.x, self.C1) for (key, value) in omega.items()]
        except Exception as e:
            # logging.warning('Error in _solve_first_phase while returning omega_star')
            # logging.error(traceback.format_exc())
            raise e

    def _solve_second_phase(self):
        C0 = self.C0
        sq_r1 = self.sq_r1
        X_nc = self.X_nc
        X_nc_indices = self.X_nc_indices

        m = gp.Model('second-phase')
        m.setParam('OutputFlag', 0)
        sq_r0 = m.addVar(name='sq_r0')
        tau = m.addVars(X_nc_indices, name='tau')
        m.addConstrs(
            (self._get_distance_from_center(X_nc[i]) - sq_r0 + tau[i] >= 0 for i
             in X_nc_indices), name='distance_constraint')
        m.addConstr(sq_r0 >= sq_r1)
        m.setObjective(-sq_r0 + C0 * gp.quicksum(tau), GRB.MINIMIZE)

        m.optimize()

        try:
            return sq_r0.x
        except AttributeError as e:
            # logging.warning('Error in second phase: ' + str(e))
            raise SecondPhaseErrorException


class GurobiTwoPhasesDualSolver(GurobiTwoPhasesSolver):

    def _solve_second_phase(self):
        X_c = self.X_c
        X_nc = self.X_nc
        X_c_indices = self.X_c_indices
        X_nc_indices = self.X_nc_indices
        k = self.k
        omega_star = self.omega_star

        m = gp.Model('second-phase')
        m.setParam('OutputFlag', 0)
        beta = m.addVars(X_nc_indices, name='beta', ub=self.C0)
        m.addConstr((gp.quicksum(beta) == 1), name='sum_constraint')

        first_sum = gp.quicksum(-1 * beta[i] * k(X_nc[i], X_nc[i]) for i in X_nc_indices)
        second_sum = 2 * gp.quicksum(
            beta[i] * omega_star[h] * k(X_nc[i], X_c[h]) for h in X_c_indices for i in X_nc_indices)
        m.setObjective(first_sum + second_sum, GRB.MAXIMIZE)

        m.optimize()

        beta_star = np.array([self._converge(value.x, self.C0) for (key, value) in beta.items()])
        self.beta_star = beta_star

        try:
            return self._get_squared_r0()
        except AttributeError as e:
            # logging.warning('Error in second phase: ' + str(e))
            raise SecondPhaseErrorException

    def _get_squared_r0(self):
        X_nc = self.X_nc
        beta_star = self.beta_star
        i, = np.where((0 < beta_star) & (beta_star < self.C0))
        return np.mean([self._get_distance_from_center(el) for el in X_nc[i]])


class GurobiTwoPhasesSingleCSolver(GurobiTwoPhasesSolver):

    def __init__(self, C, k, tol, X_c, X_nc):
        self.C = C
        GurobiTwoPhasesSolver.__init__(self, C, C, k, tol, X_c, X_nc)

    def __str__(self):
        return str(self.k) + fr', $C = {self.C:.2f}$'


class GurobiTwoPhasesShadowSolver(TwoPhasesShadowSolver):

    def _solve_first_phase(self):
        C0 = self.C0
        k = self.k
        X_s = self.X_s
        X_s_indices = self.X_s_indices

        m = gp.Model('first-phase')
        m.setParam('OutputFlag', 0)
        omega = m.addVars(X_s_indices, name='omega', ub=C0)
        m.addConstr((gp.quicksum(omega) == 1), name='sum_constraint')

        first_summation = gp.quicksum(omega[h] * k(X_s[h], X_s[h]) for h in X_s_indices)
        second_summation = gp.quicksum(
            omega[h] * omega[j] * k(X_s[h], X_s[j]) for h in X_s_indices for j in X_s_indices)
        m.setObjective(first_summation - second_summation, GRB.MAXIMIZE)

        m.optimize()

        try:
            return [self._converge(value.x, self.C0) for (key, value) in omega.items()]
        except Exception as e:
            # logging.warning('Error in _solve_first_phase while returning omega_star')
            # logging.error(traceback.format_exc())
            raise e

    def _solve_second_phase(self):
        C1 = self.C1
        sq_r0 = self.sq_r0
        X_s = self.X_s
        X_s_indices = self.X_s_indices

        m = gp.Model('second-phase')
        m.setParam('OutputFlag', 0)
        sq_r1 = m.addVar(name='sq_r1')
        tau = m.addVars(X_s_indices, name='tau')
        m.addConstrs(
            (self._get_distance_from_center(X_s[i]) - sq_r1 + tau[i] >= 0 for i
             in X_s_indices), name='distance_constraint')
        m.addConstr(sq_r1 <= sq_r0)
        m.setObjective(-sq_r1 + C1 * gp.quicksum(tau), GRB.MINIMIZE)

        m.optimize()

        try:
            return sq_r1.x
        except AttributeError as e:
            raise SecondPhaseErrorException


class GurobiTwoPhasesTwoCoresSolver(TwoPhasesSolver):

    def _solve_first_phase(self):
        return self.first_phase(self.C1, self.X_c, self.X_c_indices, self.k)

    def first_phase(self, C1, X_inside, X_inside_indices, k):
        m = gp.Model('first-phase')
        m.setParam('OutputFlag', 0)
        omega = m.addVars(X_inside_indices, name='omega', ub=C1)
        m.addConstr((gp.quicksum(omega) == 1), name='sum_constraint')
        first_summation = gp.quicksum(omega[h] * k(X_inside[h], X_inside[h]) for h in X_inside_indices)
        second_summation = gp.quicksum(
            omega[h] * omega[j] * k(X_inside[h], X_inside[j]) for h in X_inside_indices for j in X_inside_indices)
        m.setObjective(first_summation - second_summation, GRB.MAXIMIZE)
        m.optimize()

        try:
            return [self._converge(value.x, self.C1) for (key, value) in omega.items()]
        except Exception as e:
            # logging.warning('Error in _solve_first_phase while returning omega_star')
            # logging.error(traceback.format_exc())
            raise e

    def _solve_second_phase(self):
        return self.second_phase(self.C0, self.X_nc, self.X_nc_indices, self.k)

    def second_phase(self, C0, X_outside, X_outside_indices, k):
        m = gp.Model('second-phase')
        m.setParam('OutputFlag', 0)
        beta = m.addVars(X_outside_indices, name='beta', ub=C0)
        m.addConstr((gp.quicksum(beta) == 1), name='sum_constraint')
        first_summation = gp.quicksum(beta[h] * k(X_outside[h], X_outside[h]) for h in X_outside_indices)
        second_summation = gp.quicksum(
            beta[h] * beta[j] * k(X_outside[h], X_outside[j]) for h in X_outside_indices for j in X_outside_indices)
        m.setObjective(first_summation - second_summation, GRB.MAXIMIZE)
        m.optimize()
        beta_star = [self._converge(value.x, self.C0) for (key, value) in beta.items()]
        self.beta_star = beta_star
        self.fixed_term_phase2 = sum(
            [beta_star[h] * beta_star[j] * k(X_outside[h], X_outside[j]) for h in X_outside_indices for j in X_outside_indices])

        try:
            return self._get_squared_r0()
        except AttributeError as e:
            raise SecondPhaseErrorException

    def _get_squared_r0(self):
        return self._internal_get_squared_r0(np.array(self.beta_star), self.X_nc)

    def _internal_get_squared_r0(self, beta_star, X_outside):
        i, = np.where((0 < beta_star) & (beta_star < self.C0))
        if not i.size:
            return 0
        distances = [self._get_distance_from_center_phase2(el) for el in X_outside[i]]
        if len(distances) == 0:
            raise NoSupportVectorException
        return np.mean(distances)

    def _get_distance_from_center_phase2(self, x):
        return self._internal_get_distance_from_center_phase2(self.X_nc, self.X_nc_indices,
                                                              self.beta_star, self.fixed_term_phase2, self.k, x)

    @staticmethod
    def _internal_get_distance_from_center_phase2(X_outside, X_outside_indices,
                                                  beta_star, fixed_term_phase2, k, x):
        return k(x, x) - 2 * sum([beta_star[h] * k(X_outside[h], x) for h in X_outside_indices]) + fixed_term_phase2

    def _estimation_function(self, x):
        distance_phase1 = self._get_distance_from_center(x)
        distance_phase2 = self._get_distance_from_center_phase2(x)
        if distance_phase1 <= self.sq_r1:
            return 1
        elif distance_phase2 <= self.sq_r0:
            return -1
        else:
            return 0


class GurobiTwoPhasesTwoCoresShadowSolver(GurobiTwoPhasesTwoCoresSolver):

    def __init__(self, C0, C1, k, tol, X_c, X_nc, X_s):
        self.C0 = C0
        self.C1 = C1
        self.X_s = X_s
        self.X_s_indices = range(len(X_s))

        GurobiTwoPhasesTwoCoresSolver.__init__(self, C0, C1, k, tol, X_c, X_nc)

    def _solve_second_phase(self):
        return self.second_phase(self.C0, self.X_s, self.X_s_indices, self.k)

    def _estimation_function(self, x):
        distance_phase1 = self._get_distance_from_center(x)
        distance_phase2 = self._get_distance_from_center_phase2(x)
        if distance_phase1 <= self.sq_r1:
            return 1
        elif distance_phase2 <= self.sq_r0:
            return 0
        else:
            return -1

    def _get_squared_r0(self):
        return self._internal_get_squared_r0(np.array(self.beta_star), self.X_s)

    def _get_distance_from_center_phase2(self, x):
        return self._internal_get_distance_from_center_phase2(self.X_s, self.X_s_indices,
                                                              self.beta_star, self.fixed_term_phase2, self.k, x)


class TwoPhasesDoubleConstraintSolver(Solver, metaclass=ABCMeta):

    def __init__(self, C0, C1, k, tol, X_c, X_nc, X_s):
        self.C0 = C0
        self.C1 = C1
        self.X_s = X_s

        X_ncs = np.concatenate((X_nc, X_s))
        self.X_ncs = X_ncs
        X_ncs_indices = range(len(X_ncs))
        self.X_ncs_indices = X_ncs_indices

        X_cs = np.concatenate((X_c, X_s))
        self.X_cs = X_cs
        X_cs_indices = range(len(X_cs))
        self.X_cs_indices = X_cs_indices

        Solver.__init__(self, k, tol, X_c, X_nc)

    def _internal_solve(self):
        alpha_star, beta_star = self._solve_first_phase()
        self.alpha_star = alpha_star
        self.beta_star = beta_star
        fixed_term = self._compute_fixed_term(alpha_star, beta_star)
        self.fixed_term = fixed_term
        sq_r1 = self._get_squared_r1()
        self.sq_r1 = sq_r1
        self.sq_r0 = self._solve_second_phase()

        return self._estimation_function

    def _get_distance_from_center(self, x):
        return self.two_phases_double_constraint_get_distance_from_center(self.X_c, self.X_c_indices, self.X_nc,
                                                                          self.X_nc_indices, self.alpha_star,
                                                                          self.beta_star, self.fixed_term, self.k, x)

    @staticmethod
    def two_phases_double_constraint_get_distance_from_center(X_inside, X_inside_indices, X_outside, X_outside_indices, alpha_star,
                                                              beta_star, fixed_term, k, x):
        result = k(x, x) - 2 * sum([alpha_star[i] * k(X_inside[i], x) for i in X_inside_indices])
        result += 2 * sum([beta_star[j] * k(X_outside[j], x) for j in X_outside_indices]) + fixed_term
        return result

    def _get_squared_r1(self):
        return self.two_phases_double_constraint_get_squared_r1(self.X_c, self.X_nc,
                                                                np.array(self.alpha_star), np.array(self.beta_star))

    def two_phases_double_constraint_get_squared_r1(self, X_inside, X_outside, alpha_star, beta_star):
        i, = np.where((0 < alpha_star) & (alpha_star < self.C1))
        j, = np.where((0 < beta_star) & (beta_star < self.C1))
        if len(i) > 0:
            return np.max([self._get_distance_from_center(el) for el in X_inside[i]])
        elif len(j) > 0:
            return np.max([self._get_distance_from_center(el) for el in X_outside[j]])
        else:
            return 0

    def _compute_fixed_term(self, alpha_star, beta_star):
        return self.two_phases_double_constraint_compute_fixed_term(self.X_c, self.X_c_indices, self.X_nc, self.X_nc_indices, self.alpha_star,
                                                                    self.beta_star, self.k)

    @staticmethod
    def two_phases_double_constraint_compute_fixed_term(X_inside, X_inside_indices, X_outside, X_outside_indices, alpha_star,
                                                        beta_star, k):
        fix_1 = sum([alpha_star[i] * alpha_star[h] * k(X_inside[i], X_inside[h]) for i in X_inside_indices for h in X_inside_indices])
        fix_2 = 2 * sum([alpha_star[i] * beta_star[j] * k(X_inside[i], X_outside[j]) for i in X_inside_indices for j in X_outside_indices])
        fix_3 = sum([beta_star[j] * beta_star[z] * k(X_outside[j], X_outside[z]) for j in X_outside_indices for z in X_outside_indices])
        return fix_1 - fix_2 + fix_3

    @abstractmethod
    def _solve_first_phase(self):
        pass

    @abstractmethod
    def _solve_second_phase(self):
        pass

    def _estimation_function(self, x):
        # print(f'estimation: x={x}')
        distance = self._get_distance_from_center(x)
        if distance <= self.sq_r1:
            return 1
        elif distance >= self.sq_r0:
            return -1
        else:
            return 0

    def __str__(self):
        return str(self.k) + fr', $C_0 = {self.C0:.2f}$, $C_1 = {self.C1:.2f}$'

    def get_plotly_string(self):
        return self.k.get_plotly_string() + fr', C_0 = {self.C0:.2f}, C_1 = {self.C1:.2f}'


class GurobiTwoPhasesDoubleConstraintSolver(TwoPhasesDoubleConstraintSolver):

    def _solve_first_phase(self):
        return self.first_phase(self.C1, self.X_c, self.X_c_indices, self.X_nc, self.X_nc_indices, self.k)

    def first_phase(self, C1, X_inside, X_inside_indices, X_outside,
                    X_outside_indices, k):
        m = gp.Model('first-phase')
        m.setParam('OutputFlag', 0)
        alpha = m.addVars(X_inside_indices, name='alpha', ub=C1)
        beta = m.addVars(X_outside_indices, name='beta', ub=C1)
        m.addConstr((gp.quicksum(beta) - gp.quicksum(alpha) == -1), name='single_constraint')
        sum1 = gp.quicksum(alpha[i] * k(X_inside[i], X_inside[i]) for i in X_inside_indices)
        sum2 = gp.quicksum(beta[j] * k(X_outside[j], X_outside[j]) for j in X_outside_indices)
        sum3 = gp.quicksum(
            alpha[i] * alpha[h] * k(X_inside[i], X_inside[h]) for i in X_inside_indices for h in X_inside_indices)
        sum4 = 2 * gp.quicksum(
            alpha[i] * beta[j] * k(X_inside[i], X_outside[j]) for i in X_inside_indices for j in X_outside_indices)
        sum5 = gp.quicksum(
            beta[j] * beta[z] * k(X_outside[j], X_outside[z]) for j in X_outside_indices for z in X_outside_indices)
        m.setObjective(sum1 - sum2 - sum3 + sum4 - sum5, GRB.MAXIMIZE)
        m.optimize()
        alpha_star = np.array([self._converge(value.x, C1) for (key, value) in alpha.items()])
        beta_star = np.array([self._converge(value.x, C1) for (key, value) in beta.items()])
        return alpha_star, beta_star

    def _solve_second_phase(self):
        return self.second_phase(self.C0, self.X_c, self.X_c_indices, self.X_nc, self.X_nc_indices)

    def second_phase(self, C0, X_inside, X_inside_indices, X_outside, X_outside_indices):
        m = gp.Model('second-phase')
        m.setParam('OutputFlag', 0)
        # m2.setParam(GRB.Param.NonConvex, 2)
        sq_r0 = m.addVar(name='sq_r0')
        xi = m.addVars(X_inside_indices, name='xi')
        tau = m.addVars(X_outside_indices, name='tau')
        m.setObjective(- sq_r0 + C0 * gp.quicksum(xi) + C0 * gp.quicksum(tau), GRB.MINIMIZE)
        m.addConstrs(
            (sq_r0 + xi[i] - self._get_distance_from_center(X_inside[i]) >= 0 for i
             in X_inside_indices), name='distance_constraints_i')
        m.addConstrs(
            (self._get_distance_from_center(X_outside[j]) - sq_r0 + tau[j] >= 0 for j
             in X_outside_indices), name='distance_constraints_j')
        m.optimize()

        try:
            return sq_r0.x
        except AttributeError as e:
            raise SecondPhaseErrorException


class GurobiTwoPhasesDoubleConstraintShadowSolver(GurobiTwoPhasesDoubleConstraintSolver):

    def _solve_first_phase(self):
        return self.first_phase(self.C1, self.X_c, self.X_c_indices, self.X_ncs, self.X_ncs_indices, self.k)

    def _solve_second_phase(self):
        return self.second_phase(self.C0, self.X_cs, self.X_cs_indices, self.X_nc, self.X_nc_indices)

    def _get_distance_from_center(self, x):
        return self.two_phases_double_constraint_get_distance_from_center(self.X_c, self.X_c_indices, self.X_ncs,
                                                                          self.X_ncs_indices, self.alpha_star,
                                                                          self.beta_star, self.fixed_term, self.k, x)

    def _get_squared_r1(self):
        return self.two_phases_double_constraint_get_squared_r1(self.X_c, self.X_ncs,
                                                                np.array(self.alpha_star), np.array(self.beta_star))

    def _compute_fixed_term(self, alpha_star, beta_star):
        return self.two_phases_double_constraint_compute_fixed_term(self.X_c, self.X_c_indices,
                                                                    self.X_ncs, self.X_ncs_indices, self.alpha_star,
                                                                    self.beta_star, self.k)


class CPLEXTwoPhasesSolver(TwoPhasesSolver):

    def _solve_first_phase(self):
        C1 = self.C1
        k = self.k
        X_c = self.X_c
        X_c_indices = self.X_c_indices

        mdl = cpx.Model(name="first-phase")
        mdl.parameters.optimalitytarget = 3  # global optimality
        omega = mdl.continuous_var_list(X_c_indices, name='omega', ub=C1)
        mdl.add_constraint((sum(omega) == 1), ctname='sum_constraint')
        first_summation = sum(omega[h] * k(X_c[h], X_c[h]) for h in X_c_indices)
        second_summation = sum(
            omega[h] * omega[j] * k(X_c[h], X_c[j]) for h in X_c_indices for j in X_c_indices)
        mdl.set_objective('max', first_summation - second_summation)

        try:
            mdl.solve()
            return np.array([self._converge(o.solution_value, C1) for o in omega])
        except Exception as e:
            # logging.warning('Error in _solve_first_phase while returning omega_star')
            # logging.error(traceback.format_exc())
            raise e

    def _solve_second_phase(self):
        C0 = self.C0
        sq_r1 = self.sq_r1
        X_nc = self.X_nc
        X_nc_indices = self.X_nc_indices

        mdl2 = cpx.Model(name="second-phase")
        mdl2.parameters.optimalitytarget = 3  # global optimality
        sq_r0 = mdl2.continuous_var(name='sq_r0')
        tau = mdl2.continuous_var_list(X_nc_indices, name='tau')
        mdl2.add_constraints((self._get_distance_from_center(X_nc[i]) - sq_r0 + tau[i] >= 0 for i
                              in X_nc_indices), names='distance_constraint')
        mdl2.add_constraint(sq_r0 >= sq_r1)
        mdl2.set_objective('min', -sq_r0 + C0 * sum(tau))
        mdl2.solve()

        try:
            return sq_r0.solution_value
        except AttributeError as e:
            # logging.warning('Error in second phase: ' + str(e))
            raise SecondPhaseErrorException


class TensorFlowTwoPhasesSolver(TwoPhasesSolver):

    def __init__(self, C0, C1, k, tol, X_c, X_nc, n_iter=500,
                 optimizer=tf.keras.optimizers.Adam(lr=0.001), penalization=100):
        self.n_iter = n_iter
        self.optimizer = optimizer
        self.penalization = penalization
        TwoPhasesSolver.__init__(self, C0, C1, k, tol, X_c, X_nc)

    def _solve_first_phase(self):
        device_type = 'CPU'
        '''
        if len(tf.config.list_physical_devices('GPU')) > 0:
            print('phase 1, trying GPU')
            device_type = 'GPU'
        else:
            print('phase 1, using CPU')
        '''

        with tf.device(tf.DeviceSpec(device_type=device_type, device_index=0)):
            C1 = self.C1
            k = self.k
            X_c = self.X_c
            X_c_indices = self.X_c_indices

            omega = [tf.Variable(0, name=f'omega_{i}', dtype=tf.float32, trainable=True,
                                 constraint=lambda t: tf.clip_by_value(t, 0, C1))
                     for i, val in enumerate(np.random.randn(len(X_c)))]

            omega_shape = np.array(omega).shape

            kxc = [k(X_c[i], X_c[h]) for i in X_c_indices for h in X_c_indices]
            kxc = tf.cast(tf.reshape(kxc, (omega_shape[0], omega_shape[0])), dtype=tf.float32)

            kxc2 = [k(X_c[i], X_c[i]) for i in X_c_indices]
            kxc2 = tf.convert_to_tensor(kxc2, dtype=tf.float32)

            @tf.function
            def tf_f(omega):
                loss = tf.tensordot(tf.linalg.matvec(kxc, omega, transpose_a=True), omega, axes=1)
                loss += - tf.tensordot(omega, kxc2, axes=1)
                loss += self.penalization * tf.math.maximum(tf.cast(0, tf.float32), 1. - sum(omega))
                loss += self.penalization * tf.math.maximum(tf.cast(0, tf.float32), sum(omega) - 1.)
                return loss

            obj = lambda: tf_f(omega)

            for j in tqdm.trange(self.n_iter, desc='first phase', ascii=True):
                self.optimizer.minimize(obj, var_list=omega)
                # print(tf_f(omega).numpy())

            return np.array([self._converge(o.numpy(), C1) for o in omega])

    def _solve_second_phase(self):
        device_type = 'CPU'
        '''
        if len(tf.config.list_physical_devices('GPU')) > 0:
            print('phase 2, trying GPU')
            device_type = 'GPU'
        else:
            print('phase 2, using CPU')
        '''

        with tf.device(tf.DeviceSpec(device_type=device_type, device_index=0)):
            C0 = self.C0
            k = self.k
            sq_r1 = self.sq_r1
            X_c = self.X_c
            X_c_indices = self.X_c_indices
            X_nc = self.X_nc
            X_nc_indices = self.X_nc_indices
            omega_star = self.omega_star

            beta = [tf.Variable(0, name=f'beta_{i}', dtype=tf.float32, trainable=True,
                                constraint=lambda t: tf.clip_by_value(t, 0, C0))
                    for i, val in enumerate(np.random.randn(len(X_nc)))]

            beta_shape = np.array(beta).shape
            oknc = [omega_star[h] * k(X_nc[i], X_c[h]) for i in X_nc_indices for h in X_c_indices]
            oknc = tf.cast(tf.reshape(oknc, (beta_shape[0], omega_star.shape[0])), dtype=tf.float32)

            oknc2 = [k(X_nc[i], X_nc[i]) for i in X_nc_indices]
            oknc2 = tf.convert_to_tensor(oknc2, dtype=tf.float32)

            @tf.function
            def tf_f2(beta):
                loss = -2 * tf.reduce_sum(tf.linalg.matvec(oknc, beta, transpose_a=True))
                loss += tf.tensordot(beta, oknc2, axes=1)
                loss += self.penalization * tf.math.maximum(tf.cast(0, tf.float32), 1. - sum(beta))
                loss += self.penalization * tf.math.maximum(tf.cast(0, tf.float32), sum(beta) - 1.)
                return loss

            obj2 = lambda: tf_f2(beta)

            for i in tqdm.trange(self.n_iter, desc='second phase', ascii=True):
                self.optimizer.minimize(obj2, var_list=beta)
                # print(tf_f2(beta).numpy())

            beta_star = np.array([self._converge(b.numpy(), C0) for b in beta])

            # TODO: refactor this
            def get_squared_r0():
                i, = np.where((0 < beta_star) & (beta_star < C0))
                distances = [self._get_distance_from_center(el) for el in X_nc[i]]
                if len(distances) == 0:
                    raise NoSupportVectorException
                return np.mean(distances)

            return get_squared_r0()


class SinglePhaseSolver(Solver, metaclass=ABCMeta):

    def __init__(self, C, psi, k, tol, X_c, X_nc):
        self.C = C
        self.psi = psi
        Solver.__init__(self, k, tol, X_c, X_nc)

    def _internal_solve(self):
        alpha_star, beta_star = self._solve_single_phase()

        # TODO: better way to handle this?
        if alpha_star is None and beta_star is None:
            return lambda x: 2

        self.alpha_star = alpha_star
        self.beta_star = beta_star
        fixed_term = self._compute_fixed_term(alpha_star, beta_star)
        self.fixed_term = fixed_term
        sq_r1 = self._get_squared_r1()
        self.sq_r1 = sq_r1
        sq_r0 = self._get_squared_r0()
        self.sq_r0 = sq_r0

        return self._estimation_function

    def _get_distance_from_center(self, x):
        k = self.k
        alpha_star = self.alpha_star
        beta_star = self.beta_star
        fixed_term = self.fixed_term
        X_c = self.X_c
        X_c_indices = self.X_c_indices
        X_nc = self.X_nc
        X_nc_indices = self.X_nc_indices

        return k(x, x) - sum([alpha_star[i] * k(X_c[i], x) for i in X_c_indices]) + sum(
            [beta_star[j] * k(X_nc[j], x) for j in X_nc_indices]) + fixed_term

    def _compute_fixed_term(self, alpha_star, beta_star):
        k = self.k
        X_c = self.X_c
        X_c_indices = self.X_c_indices
        X_nc = self.X_nc
        X_nc_indices = self.X_nc_indices

        fixed1 = 0.25 * sum(
            [alpha_star[i] * alpha_star[z] * k(X_c[i], X_c[z]) for i in X_c_indices for z in X_c_indices])
        fixed2 = -0.5 * sum(
            [alpha_star[i] * beta_star[j] * k(X_c[i], X_nc[j]) for i in X_c_indices for j in X_nc_indices])
        fixed3 = 0.25 * sum(
            [beta_star[j] * beta_star[h] * k(X_nc[j], X_nc[h]) for j in X_nc_indices for h in X_nc_indices])
        return fixed1 + fixed2 + fixed3

    def _get_squared_r1(self):
        alpha_star = np.array(self.alpha_star)
        C = self.C
        X_c = self.X_c

        i, = np.where((0 < alpha_star) & (alpha_star < C))
        distances = [self._get_distance_from_center(el) for el in X_c[i]]
        if len(distances) == 0:
            # logging.warning('empty distances in sq_r1')
            raise NoSupportVectorException
        return np.mean(distances)

    def _get_squared_r0(self):

        beta_star = np.array(self.beta_star)
        C = self.C
        X_nc = self.X_nc

        i, = np.where((0 < beta_star) & (beta_star < C))
        distances = [self._get_distance_from_center(el) for el in X_nc[i]]
        if len(distances) == 0:
            # logging.warning('empty distances in sq_r0')
            # raise NoSupportVectorException
            # TODO: check whether to raise exception again
            return np.inf
        return np.mean(distances)

        # return self.sq_r1 + self.psi

    @abstractmethod
    def _solve_single_phase(self):
        pass

    def _estimation_function(self, x):
        # print(f'estimation: x={x}')
        distance = self._get_distance_from_center(x)
        if distance <= self.sq_r1:
            return 1
        elif distance >= self.sq_r0:
            return -1
        else:
            return 0

    def __str__(self):
        return str(self.k) + fr', $C = {self.C:.2f}$, $\psi = {self.psi:.2f}$'

    def get_plotly_string(self):
        return self.k.get_plotly_string() + fr', C = {self.C:.2f}, \psi = {self.psi:.2f}'


class GurobiSinglePhaseSolver(SinglePhaseSolver):

    def _solve_single_phase(self):
        C = self.C
        k = self.k
        X_c = self.X_c
        X_c_indices = self.X_c_indices
        X_nc = self.X_nc
        X_nc_indices = self.X_nc_indices
        psi = self.psi

        m = gp.Model('single-phase')
        m.setParam('OutputFlag', 0)
        alpha = m.addVars(X_c_indices, name='alpha', ub=C)
        beta = m.addVars(X_nc_indices, name='beta', ub=C)
        m.addConstr(gp.quicksum(alpha) - gp.quicksum(beta) == 2, name='sum_constraint')

        sum1 = gp.quicksum(alpha[i] * k(X_c[i], X_c[i]) for i in X_c_indices)
        sum2 = -1 * gp.quicksum(beta[j] * k(X_nc[j], X_nc[j]) for j in X_nc_indices)
        sum3 = gp.quicksum(alpha[i] * beta[j] * k(X_c[i], X_nc[j]) for i in X_c_indices for j in X_nc_indices)
        sum4 = -0.5 * gp.quicksum(beta[j] * beta[h] * k(X_nc[j], X_nc[h]) for j in X_nc_indices for h in X_nc_indices)
        sum5 = -0.5 * gp.quicksum(alpha[i] * alpha[z] * k(X_c[i], X_c[z]) for i in X_c_indices for z in X_c_indices)
        sum6 = psi * gp.quicksum(beta) + psi

        m.setObjective(sum1 + sum2 + sum3 + sum4 + sum5 + sum6, GRB.MAXIMIZE)

        try:
            m.optimize()
        except gp.GurobiError as ge:
            # logging.warning(ge.message)
            # not raising exception in order to speed up computation
            # raise ge
            # logging.warning('Setting Gurobi NonConvex parameter')
            # m.setParam(GRB.Param.NonConvex, 2)
            # m.optimize()
            return None, None

        alpha_star = [self._converge(value.x, self.C) for (key, value) in alpha.items()]
        beta_star = [self._converge(value.x, self.C) for (key, value) in beta.items()]
        return alpha_star, beta_star


class CPLEXSinglePhaseSolver(SinglePhaseSolver):

    def _solve_single_phase(self):
        C = self.C
        k = self.k
        X_c = self.X_c
        X_c_indices = self.X_c_indices
        X_nc = self.X_nc
        X_nc_indices = self.X_nc_indices
        psi = self.psi

        mdl = cpx.Model('single-phase')
        mdl.parameters.optimalitytarget = 3  # global optimality
        alpha = mdl.continuous_var_list(X_c_indices, name='alpha', ub=C)
        beta = mdl.continuous_var_list(X_nc_indices, name='beta', ub=C)
        mdl.add_constraint(sum(alpha) - sum(beta) == 2, ctname='sum_constraint')

        sum1 = sum(alpha[i] * k(X_c[i], X_c[i]) for i in X_c_indices)
        sum2 = -1 * sum(beta[j] * k(X_nc[j], X_nc[j]) for j in X_nc_indices)
        sum3 = sum(alpha[i] * beta[j] * k(X_c[i], X_nc[j]) for i in X_c_indices for j in X_nc_indices)
        sum4 = -0.5 * sum(beta[j] * beta[h] * k(X_nc[j], X_nc[h]) for j in X_nc_indices for h in X_nc_indices)
        sum5 = -0.5 * sum(alpha[i] * alpha[z] * k(X_c[i], X_c[z]) for i in X_c_indices for z in X_c_indices)
        sum6 = psi * sum(beta) + psi

        mdl.set_objective('max', sum1 + sum2 + sum3 + sum4 + sum5 + sum6)

        try:
            mdl.solve()
            alpha_star = [self._converge(a.solution_value, self.C) for a in alpha]
            beta_star = [self._converge(b.solution_value, self.C) for b in beta]
            return alpha_star, beta_star
        except DOcplexException as e:
            # logging.warning(e.message)
            # not raising exception in order to speed up computation
            # raise ge
            # logging.warning('Setting Gurobi NonConvex parameter')
            # m.setParam(GRB.Param.NonConvex, 2)
            # m.optimize()
            return None, None
