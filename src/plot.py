import logging
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import skfuzzy as fuzz
from ipywidgets import widgets
from sklearn.model_selection import StratifiedShuffleSplit, StratifiedKFold
from sklearn.decomposition import PCA
from matplotlib import cm
from matplotlib.offsetbox import AnchoredText
from matplotlib.ticker import StrMethodFormatter

from src.metrics import conservative_score, non_conservative_score
import src.model_selection
from src.utils import get_random_dots, get_train_test_split, append_anomalies_and_apply_target

random_state = 42


def get_pred_class(pred):
    if pred == -1:
        return 'outside'
    elif pred == 0:
        return 'shadow'
    elif pred == 1:
        return 'core'
    else:
        raise ValueError('get_pred_class: Unknown pred')


def get_compounded_pred_class(pred):
    if pred == -1:
        return 'outside'
    elif pred == 0:
        return 'shadow'
    elif pred == 1:
        return 'core virginica'
    elif pred == 2:
        return 'core versicolor'
    else:
        raise ValueError('get_pred_class: Unknown pred')


def plotly_dict_plot(dataset_loader, estimator, metric, values_dict,
                     labels, label_colors, colorscale,
                     filename, simplified=True,
                     op='w', log=True, directory='\\csv\\'):
    filename = os.getcwd() + directory + filename
    coord_frames, distance_frames, score_frames, test_scatter_frames, test_scatter_plots, train_scatter_frames, train_scatter_plots, values_targets, values_to_mesh = initialize_dict_plot_arrays(
        values_dict)

    if op == 'r':
        coord_result, df_test, df_train, distance_result, scores_result, test_scatter_result, train_scatter_result = load_dict_plot_csv(
            filename)
    else:
        splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=random_state)

        if metric == 'conservative':
            metric = conservative_score
        elif metric == 'non-conservative':
            metric = non_conservative_score
        else:
            raise ValueError('plotly_dict_plot: Unknown metric')

        m = np.array(np.meshgrid(*values_to_mesh)).T.reshape(-1, len(values_to_mesh))
        values_combinations = [{t: p for t, p in zip(values_targets, value_combination)}
                               for value_combination in m]

        for i, combination in enumerate(values_combinations):
            print('combination', combination)
            X, y, data_labels, target = dataset_loader(**combination)

            # splitting after data load to keep ratio of classes after addition of anomalies
            # and after addition of gaussian points.
            for train_index, test_index in splitter.split(X, data_labels):
                X_train = X[train_index]
                y_train = y[train_index]
                label_train = data_labels[train_index]
                X_test = X[test_index]
                y_test = y[test_index]
                label_test = data_labels[test_index]

            estimator.fit(X_train, y_train)

            df_test, df_train = get_test_train_df(X, X_test, X_train, combination, coord_frames, distance_frames,
                                                  estimator, label_test, label_train, metric, score_frames, simplified,
                                                  test_scatter_frames, train_scatter_frames, values_targets, y_test)

    create_scatter_plots(df_test, df_train, label_colors, labels, test_scatter_plots, train_scatter_plots)

    if op == 'w':
        coord_result, distance_result, scores_result, test_scatter_result, train_scatter_result = write_dict_plot_csv(
            coord_frames, df_test, df_train, distance_frames, filename, score_frames,
            test_scatter_frames, train_scatter_frames)

    g, values_widgets = create_widgets(colorscale, coord_result, scores_result, test_scatter_plots, train_scatter_plots,
                                       values_dict, values_targets, test_scatter_result, train_scatter_result, labels,
                                       label_colors)

    if log:
        log_dict_plot(colorscale, dataset_loader, estimator, filename, label_colors, labels, metric, op, simplified,
                      values_dict, values_widgets)

    return dict(
        widget=widgets.VBox(values_widgets + [g]),
        coord_result=coord_result,
        train_scatter_result=train_scatter_result,
        test_scatter_result=test_scatter_result,
        scores_result=scores_result,
        distance_result=distance_result
    )


def log_dict_plot(colorscale, dataset_loader, estimator, filename, label_colors, labels, metric, op, simplified,
                  values_dict, values_widgets):
    logging.info('called plotly_dict_plot')
    logging.info('dataset_loader: ' + str(dataset_loader))
    logging.info('estimator: ' + str(estimator))
    logging.info('metric: ' + str(metric))
    logging.info('values_dict: ' + str(values_dict))
    logging.info('labels: ' + str(labels))
    logging.info('label_colors: ' + str(label_colors))
    logging.info('colorscale: ' + str(colorscale))
    logging.info('filename: ' + str(filename))
    logging.info('simplified: ' + str(simplified))
    logging.info('op: ' + str(op))
    logging.info('values_widget: ' + str(values_widgets))
    # not logging data saved to csv


def create_widgets(colorscale, coord_result, scores_result, test_scatter_plots, train_scatter_plots, values_dict,
                   values_targets, test_scatter_result, train_scatter_result, labels, label_colors):
    values_widgets = []
    for values_target in values_targets:
        values_widget = widgets.FloatSlider(
            value=values_dict[values_target]['values'][-1],
            min=0.0,
            max=values_dict[values_target]['values'][-1],
            step=values_dict[values_target]['step'],
            description=str(values_target),
            continuous_update=False
        )
        values_widgets.append(values_widget)
    contour = go.Contour(
        # z=Z,
        z=coord_result['Z'],
        x=coord_result['x'],  # horizontal axis
        y=coord_result['y'],  # vertical axis
        colorscale=colorscale,
        contours=dict(
            start=-1,
            end=1,
            size=1
        ),
        hoverinfo='none',
        line=dict(
            dash='2px'
        ),
        showscale=False,
        showlegend=True,
        name='core/shadow'
    )
    acc = 'Accuracy = {:.3f}'.format(scores_result['score'][-1:].values[0])
    annotation = dict(
        x=0.95,
        y=0.05,
        bgcolor='wheat',
        bordercolor='DarkSlateGrey',
        borderpad=5,
        borderwidth=1,
        showarrow=False,
        text=acc,
        xref="paper",
        yref="paper"
    )
    layout = dict(
        template='simple_white',
        title=dict(
            x=0.5,
            text='Synthetic Dataset With Anomalies'
        ),
        legend_title_text='Legend',
        annotations=(annotation,)
    )
    g = go.FigureWidget(data=[contour] + train_scatter_plots +
                             test_scatter_plots, layout=layout)

    def validate():
        val = True
        for w, v_target in zip(values_widgets, values_targets):
            val = val and w.value in coord_result[v_target].unique()
        return val

    def response(change):
        if validate():
            coord_temp_df = coord_result
            train_scatter_temp_df = train_scatter_result
            test_scatter_temp_df = test_scatter_result
            score_temp_df = scores_result
            for w, v_target in zip(values_widgets, values_targets):
                coord_temp_df = coord_temp_df[coord_temp_df[v_target] == w.value]
                train_scatter_temp_df = train_scatter_temp_df[train_scatter_temp_df[v_target] == w.value]
                test_scatter_temp_df = test_scatter_temp_df[test_scatter_temp_df[v_target] == w.value]
                score_temp_df = score_temp_df[score_temp_df[v_target] == w.value]

            x = coord_temp_df['x']
            y = coord_temp_df['y']
            z = coord_temp_df['Z']
            with g.batch_update():
                g.data[0].x = x
                g.data[0].y = y
                g.data[0].z = z

                acc = 'Accuracy = {:.3f}'.format(score_temp_df['score'].values[0])
                g.layout.annotations[0].text = acc

                for i, (lab, col) in enumerate(zip(labels, label_colors)):
                    g.data[i + 1].x = train_scatter_temp_df[train_scatter_temp_df['label'] == lab]['x']
                    g.data[i + 1].y = train_scatter_temp_df[train_scatter_temp_df['label'] == lab]['y']

                    g.data[i + 1].text = [f'Prediction: {get_pred_class(pred)}<br>Set: Train'
                                          for pred in
                                          train_scatter_temp_df[train_scatter_temp_df['label'] == lab]['pred']]

                    g.data[i + len(labels) + 1].x = test_scatter_temp_df[test_scatter_temp_df['label'] == lab]['x']
                    g.data[i + len(labels) + 1].y = test_scatter_temp_df[test_scatter_temp_df['label'] == lab]['y']

                    g.data[i + len(labels) + 1].text = [f'Prediction: {get_pred_class(pred)}<br>Set: Test'
                                                        for pred in
                                                        test_scatter_temp_df[test_scatter_temp_df['label'] == lab][
                                                            'pred']]

    for values_widget in values_widgets:
        values_widget.observe(response, names="value")
    return g, values_widgets


def write_dict_plot_csv(coord_frames, df_test, df_train, distance_frames, filename,
                        score_frames, test_scatter_frames, train_scatter_frames):
    coord_result = pd.concat(coord_frames)
    train_scatter_result = pd.concat(train_scatter_frames)
    test_scatter_result = pd.concat(test_scatter_frames)
    scores_result = pd.concat(score_frames)
    distance_result = pd.concat(distance_frames)
    coord_result.to_csv(filename + '_coord_result.csv', index_label=False)
    train_scatter_result.to_csv(filename + '_train_scatter_result.csv', index_label=False)
    test_scatter_result.to_csv(filename + '_test_scatter_result.csv', index_label=False)
    scores_result.to_csv(filename + '_scores_result.csv', index_label=False)
    distance_result.to_csv(filename + '_distance_result.csv', index_label=False)
    df_train.to_csv(filename + '_df_train.csv', index_label=False)
    df_test.to_csv(filename + '_df_test.csv', index_label=False)
    return coord_result, distance_result, scores_result, test_scatter_result, train_scatter_result


def create_scatter_plots(df_test, df_train, label_colors, labels, test_scatter_plots, train_scatter_plots):
    for lab, col in zip(labels, label_colors):
        train_scatter = go.Scatter(
            x=df_train[df_train['label'] == lab]['x'],
            y=df_train[df_train['label'] == lab]['y'],
            hovertemplate=
            '<b>x</b>: %{x:.3f}<br>' +
            '<b>y</b>: %{y:.3f}<br>' +
            '<b>%{text}</b>',
            text=[f'Prediction: {get_pred_class(pred)}<br>Set: Train'
                  for pred in df_train[df_train['label'] == lab]['pred']],
            marker=dict(
                color=col,
                size=5,
                line=dict(width=1, color='DarkSlateGrey')
            ),
            mode='markers',
            name=lab + ' train'
        )
        train_scatter_plots.append(train_scatter)

        test_scatter = go.Scatter(
            x=df_test[df_test['label'] == lab]['x'],
            y=df_test[df_test['label'] == lab]['y'],
            hovertemplate=
            '<b>x</b>: %{x:.3f}<br>' +
            '<b>y</b>: %{y:.3f}<br>' +
            '<b>%{text}</b>',
            text=[f'Prediction: {get_pred_class(pred)}<br>Set: Test'
                  for pred in df_test[df_test['label'] == lab]['pred']],
            marker=dict(
                color=col,
                size=5,
                line=dict(width=1, color='DarkSlateGrey'),
                symbol='square'
            ),
            mode='markers',
            name=lab + ' test'
        )
        test_scatter_plots.append(test_scatter)


def get_test_train_df(X, X_test, X_train, combination, coord_frames, distance_frames, estimator, label_test,
                      label_train, metric, score_frames, simplified, test_scatter_frames, train_scatter_frames,
                      values_targets, y_test):
    y_pred = estimator.predict(X_test)
    score = metric(y_test, y_pred)
    score_dict = combination
    score_dict['score'] = score
    score_df = pd.DataFrame(score_dict, index=[0])
    score_frames.append(score_df)
    distance_dict = combination
    distance_dict['distance'] = estimator.solver_.sq_r0 - estimator.solver_.sq_r1
    distance_df = pd.DataFrame(distance_dict, index=[0])
    distance_frames.append(distance_df)
    X0, X1 = X[:, 0], X[:, 1]
    if simplified:
        xm = np.linspace(min(X0) - 1, max(X0) + 1, 150)
        ym = np.linspace(min(X1) - 1, max(X1) + 1, 150)
    else:
        x_min, x_max = min(X0) - 1, max(X0) + 1
        y_min, y_max = min(X1) - 1, max(X1) + 1
        xm = np.concatenate((np.arange(x_min, x_max, .02), X0))
        ym = np.concatenate((np.arange(y_min, y_max, .02), X1))
    xx, yy = np.meshgrid(xm, ym)
    Zs = np.array(estimator.predict_trace(np.c_[xx.ravel(), yy.ravel()], True))
    Z = Zs.reshape(xx.shape)
    coord_dict = {'x': xx.ravel(), 'y': yy.ravel(), 'Z': Zs}
    coord_df = pd.DataFrame(coord_dict)
    train_pred = estimator.predict(X_train)
    d_train = {'x': X_train[:, 0], 'y': X_train[:, 1], 'label': label_train,
               'pred': train_pred}
    df_train = pd.DataFrame(d_train)
    test_pred = estimator.predict(X_test)
    d_test = {'x': X_test[:, 0], 'y': X_test[:, 1], 'label': label_test,
              'pred': test_pred}
    df_test = pd.DataFrame(d_test)
    for values_target in values_targets:
        coord_df[values_target] = combination[values_target]
        df_train[values_target] = combination[values_target]
        df_test[values_target] = combination[values_target]
    coord_frames.append(coord_df)
    train_scatter_frames.append(df_train)
    test_scatter_frames.append(df_test)
    return df_test, df_train


def initialize_dict_plot_arrays(values_dict):
    train_scatter_plots = []
    test_scatter_plots = []
    score_frames = []
    coord_frames = []
    train_scatter_frames = []
    test_scatter_frames = []
    distance_frames = []
    values_targets = []
    values_to_mesh = []
    for values_target, values_target_dict in values_dict.items():
        values_list = values_target_dict['values']
        values_targets.append(values_target)
        values_to_mesh.append(values_list)
    return coord_frames, distance_frames, score_frames, test_scatter_frames, test_scatter_plots, train_scatter_frames, train_scatter_plots, values_targets, values_to_mesh


def load_dict_plot_csv(filename):
    coord_result = pd.read_csv(filename + '_coord_result.csv')
    train_scatter_result = pd.read_csv(filename + '_train_scatter_result.csv')
    test_scatter_result = pd.read_csv(filename + '_test_scatter_result.csv')
    scores_result = pd.read_csv(filename + '_scores_result.csv')
    distance_result = pd.read_csv(filename + '_distance_result.csv')
    df_train = pd.read_csv(filename + '_df_train.csv')
    df_test = pd.read_csv(filename + '_df_test.csv')
    return coord_result, df_test, df_train, distance_result, scores_result, test_scatter_result, train_scatter_result


def plotly_anomaly_numbers_plot(dataset_loader, estimator, metric, values_dict,
                                labels, label_colors, colorscale,
                                filename, simplified=True,
                                op='w', log=True, directory='\\csv\\'):
    filename = os.getcwd() + directory + filename
    coord_frames, distance_frames, score_frames, test_scatter_frames, test_scatter_plots, train_scatter_frames, train_scatter_plots, values_targets, values_to_mesh = initialize_dict_plot_arrays(
        values_dict)

    if op == 'r':
        coord_result, df_test, df_train, distance_result, scores_result, test_scatter_result, train_scatter_result = load_dict_plot_csv(
            filename)
    else:
        splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=random_state)

        if metric == 'conservative':
            metric = conservative_score
        elif metric == 'non-conservative':
            metric = non_conservative_score
        else:
            raise ValueError('plotly_anomaly_numbers_plot: Unknown metric')

        m = np.array(np.meshgrid(*values_to_mesh)).T.reshape(-1, len(values_to_mesh))
        values_combinations = [{t: p for t, p in zip(values_targets, value_combination)}
                               for value_combination in m]

        for i, combination in enumerate(values_combinations):
            print('combination', combination)
            X, y, data_labels, target = dataset_loader()

            for train_index, test_index in splitter.split(X, data_labels):
                X_train = X[train_index]
                y_train = y[train_index]
                label_train = data_labels[train_index]
                X_test = X[test_index]
                y_test = y[test_index]
                label_test = data_labels[test_index]

            X_train, y_train, label_train = append_anomalies_and_apply_target(X_train, y_train, label_train, target,
                                                                              **combination)

            estimator.fit(X_train, y_train)
            df_test, df_train = get_test_train_df(X, X_test, X_train, combination, coord_frames, distance_frames,
                                                  estimator, label_test, label_train, metric, score_frames, simplified,
                                                  test_scatter_frames, train_scatter_frames, values_targets, y_test)

    create_scatter_plots(df_test, df_train, label_colors, labels, test_scatter_plots, train_scatter_plots)

    if op == 'w':
        coord_result, distance_result, scores_result, test_scatter_result, train_scatter_result = write_dict_plot_csv(
            coord_frames, df_test, df_train, distance_frames, filename, score_frames,
            test_scatter_frames, train_scatter_frames)

    g, values_widgets = create_widgets(colorscale, coord_result, scores_result, test_scatter_plots, train_scatter_plots,
                                       values_dict, values_targets, test_scatter_result, train_scatter_result, labels,
                                       label_colors)

    if log:
        log_dict_plot(colorscale, dataset_loader, estimator, filename, label_colors, labels, metric, op, simplified,
                      values_dict, values_widgets)

    return dict(
        widget=widgets.VBox(values_widgets + [g]),
        coord_result=coord_result,
        train_scatter_result=train_scatter_result,
        test_scatter_result=test_scatter_result,
        scores_result=scores_result,
        distance_result=distance_result
    )


def plotly_distance_and_score_plot(res_dict):
    scores_df = res_dict['scores_result']
    df = res_dict['distance_result']

    first_element_name = df.columns[0]
    first_element_values = np.unique(df.iloc[:, 0].values)
    second_element_name = df.columns[1]
    second_element_values = np.unique(df.iloc[:, 1].values)
    objective_name = df.columns[-1]
    objective_values = df.iloc[:, -1].values.reshape(len(first_element_values), len(second_element_values))
    second_objective_name = df.columns[-2]
    second_objective_values = df.iloc[:, -2].values.reshape(len(first_element_values), len(second_element_values))

    # z = np.array(scores_df['score'].apply(lambda x: float(x))).reshape(len(probas), len(probas))
    # x = np.sort(scores_df['red_anomaly_proba'].apply(lambda x: float(x)).unique())
    # y = np.sort(scores_df['blue_anomaly_proba'].apply(lambda x: float(x)).unique())
    z = np.array(df.iloc[:, -2].apply(lambda x: float(x))).reshape(len(first_element_values),
                                                                   len(second_element_values))
    x = np.unique(df.iloc[:, 1].apply(lambda x: float(x)))
    y = np.unique(df.iloc[:, 0].apply(lambda x: float(x)))

    # matplotlib score heatmap
    fig, ax = plt.subplots(figsize=(10, 7))
    im, cbar = heatmap(z, x, y, ax=ax, cbarlabel='accuratezza')
    annotate_heatmap(im, valfmt="{x:.3f}")
    plt.xlabel(second_element_name)
    plt.ylabel(first_element_name)
    plt.show()

    # plotly score surface
    fig = go.Figure(data=[go.Surface(z=z, x=x, y=y)])
    fig.update_layout(
        template='simple_white',
        width=500, height=500,
        margin=dict(l=65, r=50, b=65, t=90),
        #title=dict(
        #    x=0.5,
        #    text='<b>Accuracy'
        #),
        scene=dict(
            xaxis=dict(
                title=second_element_name,
            ),
            yaxis=dict(
                title=first_element_name,
            ),
            zaxis=dict(
                #title=second_objective_name
                title='accuratezza'
            ),
        ),
    )
    fig.show()

    # plotly blue anomaly distance function
    fig = go.Figure(
        layout=dict(
            #title=dict(
            #    x=0.5,
            #    text="blue anomaly distance function"
            #),
            xaxis_title=first_element_name,
            yaxis_title="distanza core/shadow",
        )
    )
    for i, value in enumerate(first_element_values):
        fig.add_trace(go.Scatter(x=first_element_values, y=objective_values.T[i],
                                 mode='lines+markers',
                                 name=second_element_name + ' ' + str(second_element_values[i])))
    fig.show()

    # plotly blue anomaly score function
    fig = go.Figure(
        layout=dict(
            #title=dict(
            #    x=0.5,
            #    text="blue anomaly score function"
            #),
            xaxis_title=first_element_name,
            # yaxis_title=second_objective_name,
            yaxis_title='accuratezza'
        )
    )
    for i, value in enumerate(first_element_values):
        fig.add_trace(go.Scatter(x=first_element_values, y=second_objective_values.T[i],
                                 mode='lines+markers',
                                 name=second_element_name + ' ' + str(second_element_values[i])))
    fig.show()

    # plotly red anomaly distance function
    fig = go.Figure(
        layout=dict(
            #title=dict(
            #    x=0.5,
            #    text="red anomaly distance function"
            #),
            xaxis_title=second_element_name,
            yaxis_title="distanza core/shadow",
        )
    )
    for i, value in enumerate(first_element_values):
        fig.add_trace(go.Scatter(x=first_element_values, y=objective_values[i],
                                 mode='lines+markers',
                                 name=first_element_name + ' ' + str(second_element_values[i])))
    fig.show()

    # plotly red anomaly score function
    fig = go.Figure(
        layout=dict(
            #title=dict(
            #    x=0.5,
            #    text="red anomaly score function"
            #),
            xaxis_title=second_element_name,
            # yaxis_title=second_objective_name,
            yaxis_title='accuratezza'
        )
    )
    for i, value in enumerate(first_element_values):
        fig.add_trace(go.Scatter(x=first_element_values, y=second_objective_values[i],
                                 mode='lines+markers',
                                 name=first_element_name + ' ' + str(second_element_values[i])))
    fig.show()

    same_proba_df = df[df[first_element_name] == df[second_element_name]]

    # plotly same proba distance function
    fig = go.Figure(
        layout=dict(
            template='simple_white',
            width=500, height=500,
            margin=dict(l=65, r=50, b=65, t=90),
            #title=dict(
            #    x=0.5,
            #    text="<b>Red/blue same anomaly values distance function"
            #),
            xaxis_title="anomalie rosse/blu",
            yaxis_title="distanza core/shadow",
        )
    )
    fig.add_trace(go.Scatter(x=same_proba_df.iloc[:, 0], y=same_proba_df.iloc[:, 3],
                             mode='lines+markers',
                             name='distanza core/shadow'))
    fig.show()

    # plotly same proba score function
    fig = go.Figure(
        layout=dict(
            template='simple_white',
            width=500, height=500,
            margin=dict(l=65, r=50, b=65, t=90),
            #title=dict(
            #    x=0.5,
            #    text="<b>Red/blue same anomaly values score function"
            #),
            xaxis_title="anomalie rosse/blu",
            # yaxis_title=second_objective_name,
            yaxis_title='accuratezza'
        )
    )
    fig.add_trace(go.Scatter(x=same_proba_df.iloc[:, 0], y=same_proba_df.iloc[:, 2],
                             mode='lines+markers',
                             name='accuratezza'))
    fig.show()


def plotly_fcm_shadowed_set(dataset_loader, sse_cons, sse_non_cons, metric, labels, label_colors, colorscale,
                            ncenters=2, center_target=0, log=True):
    if metric == 'conservative':
        metric = conservative_score
    elif metric == 'non-conservative':
        metric = non_conservative_score
    else:
        raise ValueError('plotly_fcm_shadowed_set: Unknown metric')

    X, y, data_labels, target, _ = dataset_loader()
    splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=random_state)
    for train_index, test_index in splitter.split(X, data_labels):
        X_train = X[train_index]
        y_train = y[train_index]
        label_train = data_labels[train_index]
        X_test = X[test_index]
        y_test = y[test_index]
        label_test = data_labels[test_index]
        pca = PCA(n_components=2)
        pca.fit(X_train, y_train)
        X_train = pca.transform(X_train)
        X_test = pca.transform(X_test)
        X = pca.transform(X)

    sse_cons.fit(X_train, y_train)
    sse_non_cons.fit(X_train, y_train)

    # TRAIN
    xpts = np.array(X_train)[:, 0]
    ypts = np.array(X_train)[:, 1]
    alldata = np.vstack((xpts, ypts))
    cntr, u_train, _, _, _, _, fpc_train = fuzz.cluster.cmeans(
        alldata, ncenters, 2, error=0.005, maxiter=1000, init=None, seed=random_state)
    pred_train = from_u_to_pred(u_train, center_target)
    sse_cons_pred_train = sse_cons.predict(X_train)
    sse_non_cons_pred_train = sse_non_cons.predict(X_train)

    # TEST
    pred_test, fpc_test = c_means_predict_from_X(X_test, center_target, cntr)
    sse_cons_pred_test = sse_cons.predict(X_test)
    sse_non_cons_pred_test = sse_non_cons.predict(X_test)

    # CONTOUR
    X0, X1 = X[:, 0], X[:, 1]
    x_min, x_max = X0.min() - 1, X0.max() + 1
    y_min, y_max = X1.min() - 1, X1.max() + 1
    step = .02
    XX0 = np.arange(x_min, x_max, step)
    XX1 = np.arange(y_min, y_max, step)
    xx, yy = np.meshgrid(XX0, XX1)
    combined_contour_data = np.c_[xx.ravel(), yy.ravel()]
    pred_contour, fpc_contour = c_means_predict_from_X(combined_contour_data, center_target, cntr)
    sse_cons_pred_contour = sse_cons.predict_trace(combined_contour_data)
    sse_non_cons_pred_contour = sse_non_cons.predict_trace(combined_contour_data)
    Z = pred_contour.reshape(xx.shape)
    sse_cons_Z = sse_cons_pred_contour.reshape(xx.shape)
    sse_non_cons_Z = sse_non_cons_pred_contour.reshape(xx.shape)

    d_train = {'X_train': X_train[:, 0], 'y_train': X_train[:, 1], 'label_train': label_train,
               'pred_train': pred_train, 'sse_cons_pred_train': sse_cons_pred_train,
               'sse_non_cons_pred_train': sse_non_cons_pred_train}
    df_train = pd.DataFrame(d_train)
    d_test = {'X_test': X_test[:, 0], 'y_test': X_test[:, 1], 'label_test': label_test,
              'pred_test': pred_test, 'sse_cons_pred_test': sse_cons_pred_test,
              'sse_non_cons_pred_test': sse_non_cons_pred_test}
    df_test = pd.DataFrame(d_test)

    if log:
        logging.info('plotly_fcm_shadowed_set')
        logging.info('dataset_loader: ' + str(dataset_loader))
        logging.info('metric: ' + str(metric))
        logging.info('labels: ' + str(labels))
        logging.info('label_colors: ' + str(label_colors))
        logging.info('colorscale: ' + str(colorscale))
        logging.info('ncenters: ' + str(ncenters))
        logging.info('center_target: ' + str(center_target))
        logging.info('df_train: ' + str(df_train))
        logging.info('df_test: ' + str(df_test))

    # ACTUAL PLOT
    fig = go.Figure()
    scatter_plots = []
    for lab, col in zip(labels, label_colors):
        scatter = go.Scatter(
            x=df_train[df_train['label_train'] == lab]['X_train'],
            y=df_train[df_train['label_train'] == lab]['y_train'],
            hovertemplate=
            '<b>x</b>: %{x:.3f}<br>' +
            '<b>y</b>: %{y:.3f}<br>' +
            '<b>%{text}</b>',
            text=[f'FCM Prediction: {get_pred_class(pred)}<br>'
                  f'Cons. Prediction: {get_pred_class(sse_cons_pred)}<br>'
                  f'Non-cons. Prediction: {get_pred_class(sse_non_cons_pred)}'
                  f'<br> Set: Train'
                  for pred, sse_cons_pred, sse_non_cons_pred in
                  zip(df_train[df_train['label_train'] == lab]["pred_train"],
                      df_train[df_train['label_train'] == lab]["sse_cons_pred_train"],
                      df_train[df_train['label_train'] == lab]["sse_non_cons_pred_train"])],
            marker=dict(
                color=col,
                size=5,
                line=dict(width=1, color='DarkSlateGrey')
            ),
            mode='markers',
            name=lab + ' train')
        scatter_plots.append(scatter)

        scatter = go.Scatter(
            x=df_test[df_test['label_test'] == lab]['X_test'],
            y=df_test[df_test['label_test'] == lab]['y_test'],
            hovertemplate=
            '<b>x</b>: %{x:.3f}<br>' +
            '<b>y</b>: %{y:.3f}<br>' +
            '<b>%{text}</b>',
            text=[f'FCM Prediction: {get_pred_class(pred)}<br>'
                  f'Cons. Prediction: {get_pred_class(sse_cons_pred)}<br>'
                  f'Non-cons. Prediction: {get_pred_class(sse_non_cons_pred)}'
                  f'<br> Set: Test'
                  for pred, sse_cons_pred, sse_non_cons_pred in
                  zip(df_test[df_test['label_test'] == lab]["pred_test"],
                      df_test[df_test['label_test'] == lab]["sse_cons_pred_test"],
                      df_test[df_test['label_test'] == lab]["sse_non_cons_pred_test"])],
            marker=dict(
                color=col,
                size=5,
                line=dict(width=1, color='DarkSlateGrey'),
                symbol='square'
            ),
            mode='markers',
            name=lab + ' test')
        scatter_plots.append(scatter)

    fcm_contour = go.Contour(
        z=Z,
        x=XX0,  # horizontal axis
        y=XX1,  # vertical axis
        colorscale=colorscale,
        contours=dict(
            start=-1,
            end=1,
            size=1
        ),
        hoverinfo='none',
        line=dict(
            dash='2px'
        ),
        showscale=False,
        showlegend=True,
        name='FCM Core/Shadow'
    )
    sse_cons_contour = go.Contour(
        z=sse_cons_Z,
        x=XX0,  # horizontal axis
        y=XX1,  # vertical axis
        colorscale=colorscale,
        contours=dict(
            start=-1,
            end=1,
            size=1
        ),
        hoverinfo='none',
        line=dict(
            dash='2px'
        ),
        showscale=False,
        showlegend=True,
        name='Cons. Core/Shadow'
    )
    sse_non_cons_contour = go.Contour(
        z=sse_non_cons_Z,
        x=XX0,  # horizontal axis
        y=XX1,  # vertical axis
        colorscale=colorscale,
        contours=dict(
            start=-1,
            end=1,
            size=1
        ),
        hoverinfo='none',
        line=dict(
            dash='2px'
        ),
        showscale=False,
        showlegend=True,
        name='Non-cons. Core/Shadow'
    )
    fig.add_traces([fcm_contour, sse_cons_contour, sse_non_cons_contour])
    fig.add_traces(scatter_plots)
    acc = 'FCM Accuracy = {:.3f}<br>'.format(metric(y_test, pred_test))
    acc += 'Cons. Model Accuracy = {:.3f}<br>'.format(metric(y_test, sse_cons_pred_test))
    acc += 'Non-cons. Model Accuracy = {:.3f}'.format(metric(y_test, sse_non_cons_pred_test))
    fig.add_annotation(
        x=0.95,
        y=0.05,
        bgcolor='wheat',
        bordercolor='DarkSlateGrey',
        borderpad=5,
        borderwidth=1,
        showarrow=False,
        text=acc,
        xref="paper",
        yref="paper"
    )
    fig.update_layout(
        template='simple_white',
        title=dict(
            x=0.5,
            text='FCM Shadowed Set'
        )
    )
    fig.show()


def c_means_predict_from_X(X, center_target, cntr):
    xpts = np.array(X)[:, 0]
    ypts = np.array(X)[:, 1]
    alldata = np.vstack((xpts, ypts))
    u, _, _, _, _, fpc = fuzz.cluster.cmeans_predict(
        alldata, cntr, 2, error=0.005, maxiter=1000)
    pred = from_u_to_pred(u, center_target)
    return pred, fpc


def from_u_to_pred(u, center_target):
    ls = [np.concatenate(el) for el in np.hsplit(u, u.shape[-1])]
    pred = []
    for el in ls:
        index_max = np.argmax(el)
        val_max = el[index_max]
        if val_max > .75:
            pred.append(1 if index_max == center_target else -1)
        else:
            pred.append(0)
    return np.array(pred)


def plotly_shadowed_set(nrows, ncols, estimators, train_dfs, test_dfs, coord_dfs, scores, title,
                        labels, label_colors, colorscale_ls, colorscale_show_ls, colorscale_dict,
                        plotly_parameters=True, compounded=False, subplot_titles=None, annotation_start_x=3.1,
                        annotation_start_y=-1.75, annotation_opacity=1, fixed_height=None, no_title=False, no_legend=False,
                        no_subplot_titles=False):
    if subplot_titles is None:
        if plotly_parameters:
            subplot_titles = [estimator.get_plotly_parameters() for estimator in estimators]
        else:
            subplot_titles = [estimator.get_plotly_solver_name() for estimator in estimators]

    fig = make_subplots(
        rows=nrows,
        cols=ncols,
        subplot_titles=[] if no_subplot_titles else subplot_titles,
        vertical_spacing=0.1,
        horizontal_spacing=0.05,
        shared_xaxes=True,
        shared_yaxes=True
    )

    estimators_matrix = []
    train_dfs_matrix = []
    test_dfs_matrix = []
    coord_dfs_matrix = []
    scores_matrix = []
    colorscale_matrix = []
    colorscale_show_matrix = []

    for row in range(nrows):
        r = []
        train_r = []
        test_r = []
        coord_r = []
        score_r = []
        color_r = []
        color_show_r = []
        for col in range(ncols):
            index = row * ncols + col
            if index < len(estimators):
                r.append(estimators[index])
                train_r.append(train_dfs[index])
                test_r.append(test_dfs[index])
                coord_r.append(coord_dfs[index])
                score_r.append(scores[index])
                color_r.append(colorscale_ls[index])
                color_show_r.append(colorscale_show_ls[index])

        estimators_matrix.append(r)
        train_dfs_matrix.append(train_r)
        test_dfs_matrix.append(test_r)
        coord_dfs_matrix.append(coord_r)
        scores_matrix.append(score_r)
        colorscale_matrix.append(color_r)
        colorscale_show_matrix.append(color_show_r)

    for row, estimators_row in enumerate(estimators_matrix):
        for col, estimator in enumerate(estimators_row):
            df_train = train_dfs_matrix[row][col]
            df_test = test_dfs_matrix[row][col]
            coord_result = coord_dfs_matrix[row][col]
            score = scores_matrix[row][col]

            for lab, color in zip(labels, label_colors):
                train_scatter = go.Scatter(
                    x=df_train[df_train['label'] == lab]['x'],
                    y=df_train[df_train['label'] == lab]['y'],
                    hovertemplate='<b>x</b>: %{x:.3f}<br>' + '<b>y</b>: %{y:.3f}<br>' + '<b>%{text}</b>',
                    text=[f'Prediction: {get_compounded_pred_class(pred) if compounded else get_pred_class(pred)}'
                          f'<br>Set: Train'
                          for pred in df_train[df_train['label'] == lab]['pred']],
                    marker=dict(
                        color=color,
                        size=5,
                        line=dict(width=1, color='black')
                    ),
                    mode='markers',
                    name=lab + ' train',
                    showlegend=False if no_legend else True if (row + 1) == 1 and (col + 1) == 1 else False
                )
                fig.add_trace(
                    train_scatter,
                    row=row + 1,
                    col=col + 1
                )

                test_scatter = go.Scatter(
                    x=df_test[df_test['label'] == lab]['x'],
                    y=df_test[df_test['label'] == lab]['y'],
                    hovertemplate='<b>x</b>: %{x:.3f}<br>' + '<b>y</b>: %{y:.3f}<br>' + '<b>%{text}</b>',
                    text=[f'Prediction: {get_compounded_pred_class(pred) if compounded else get_pred_class(pred)}'
                          f'<br>Set: Test'
                          for pred in df_test[df_test['label'] == lab]['pred']],
                    marker=dict(
                        color=color,
                        size=5,
                        line=dict(width=1, color='black'),
                        symbol='square'
                    ),
                    mode='markers',
                    name=lab + ' test',
                    showlegend=False if no_legend else True if (row + 1) == 1 and (col + 1) == 1 else False
                )
                fig.add_trace(
                    test_scatter,
                    row=row + 1,
                    col=col + 1
                )

            contour = go.Contour(
                # z=Z,
                z=coord_result['Z'],
                x=coord_result['x'],  # horizontal axis
                y=coord_result['y'],  # vertical axis
                colorscale=colorscale_dict[colorscale_matrix[row][col]],
                contours=dict(
                    start=-1,
                    end=1,
                    size=1
                ),
                hoverinfo='none',
                line=dict(
                    dash='2px'
                ),
                showscale=False,
                showlegend=False if no_legend else colorscale_show_matrix[row][col],
                name='core/shadow'
            )
            fig.add_trace(
                contour,
                row=row + 1,
                col=col + 1
            )

            acc = '<b>Accuratezza = {:.3f}'.format(score)
            annotation = dict(
                x=annotation_start_x,
                y=annotation_start_y,
                opacity=annotation_opacity,
                bgcolor='wheat',  # it is possible to make this opaque by using rgba(0,0,0,0) values
                bordercolor='black',
                borderpad=5,
                borderwidth=1,
                showarrow=False,
                text=acc,
                xref="paper",
                yref="paper",
                font=dict(
                    color='black',
                    size=14
                )
            )
            fig.add_annotation(
                annotation,
                row=row + 1,
                col=col + 1
            )

    if no_title:
        fig.update_layout(
            template='simple_white',
            height=fixed_height if fixed_height is not None else 250 * nrows + 150,
            width=600 * ncols
        )
    else:
        fig.update_layout(
            template='simple_white',
            title_text="<b>" + title + "</b>",
            title_x=0.5,
            height=fixed_height if fixed_height is not None else 250 * nrows + 150,
            width=600 * ncols
        )

    for annotation in fig['layout']['annotations']:
        annotation['y'] = annotation['y'] + 0.01

    fig.show()


def plotly_score_zoom(dataset_loader, estimator, param_grid, metric, scaled=False, variance_ratio=None, cv_num=10,
                      verbose=1, n_jobs=2):
    X, X_train, y_train, X_test, y_test, label_train, label_test = get_train_test_split(dataset_loader)
    cv = StratifiedKFold(cv_num, random_state=random_state, shuffle=True)

    gs, _, _, _ = src.model_selection._internal_grid_search(X_train, y_train, X_test, y_test, estimator, cv, param_grid, metric, verbose,
                                        n_jobs=n_jobs, display=True, log=False, main_metric_name=None, scaled=scaled,
                                        variance_ratio=variance_ratio,
                                        dim=None)

    keys = sorted(param_grid.keys())

    plotly_score_surface(param_grid[keys[0]], param_grid[keys[1]], gs, 'Accuratezza', keys[0], keys[1])
    plot_score_heatmap(param_grid[keys[0]], param_grid[keys[1]], gs, 'Accuratezza', keys[1], keys[0])


def plotly_score_surface(param1_range, param2_range, gs, metric_name, param1_name, param2_name):
    # swapped param1_range and param2_range
    xx, yy = np.meshgrid(param2_range, param1_range)
    scores = gs.cv_results_['mean_test_' + metric_name].reshape(len(param1_range),
                                                                len(param2_range))

    fig = go.Figure(data=[go.Surface(z=scores, x=xx, y=yy, colorscale='viridis')])
    fig.update_layout(
                      # title=r'<b>Validation Accuracy',
                      # title_x=0.5,
                      width=500, height=500,
                      margin=dict(l=65, r=50, b=65, t=90),
                      template='plotly_white',
                      scene=dict(
                          xaxis=dict(
                              title=param2_name
                          ),
                          yaxis=dict(
                              title=param1_name
                          ),
                          zaxis=dict(
                              title='Accuratezza'
                          ),
                      ),
                      )
    fig.show()


def make_meshgrid(x, y, step=.02):
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    #xx, yy = np.meshgrid(np.arange(x_min, x_max, h).astype(np.float32),
    #                     np.arange(y_min, y_max, h).astype(np.float32))
    xx, yy = np.meshgrid(np.arange(x_min, x_max, step),
                         np.arange(y_min, y_max, step))
    return xx, yy


def plot_contours(ax, xx, yy, Z, contourf_levels, contourf_colors, contour_levels, line_colors='k', alpha=0.8):

    Z = Z.reshape(xx.shape)
    out1 = ax.contourf(xx, yy, Z, levels=contourf_levels, colors=contourf_colors, alpha=alpha)
    out2 = ax.contour(xx, yy, Z, levels=contour_levels, linewidths=1, linestyles='dashed', colors=line_colors)
    proxy = [plt.Rectangle((0, 0), 1, 1, fc=pc.get_facecolor()[0])
             for pc in out1.collections]
    out2.collections[0].set_label('learned frontier')
    return proxy


def draw_scatterplot(ax, X, title, y_labels, labels, label_colors):
    for lab, col in zip(labels, label_colors):
        ax.scatter(X[y_labels == lab, 0],
                X[y_labels == lab, 1],
                label=lab,
                c=col, s=20, edgecolors='k')
    ax.set_title(title)


def plot_shadowed_set(gs_res, labels, label_colors,
                      contourf_colors=np.array(['white', 'pink', 'palevioletred', 'red']),
                      contourf_levels=np.array([-2, -1, 0, 1]),
                      contour_levels=np.array([-1, 0]),
                      step=.02,
                      area_labels=["shadow", "core"],
                      simplified=False,
                      trace=True,
                      custom_suptitle=None,
                      plotly=False,
                      colorscale_ls=None,
                      colorscale_dict=None,
                      annotation_start_x=3.1,
                      annotation_start_y=-1.75,
                      annotation_opacity=1,
                      no_title=False,
                      no_legend=False,
                      no_subplot_titles=False):

    X = gs_res['concatenated_X'][0]
    estimator = gs_res['best_gs'].best_estimator_['estimator']
    score = gs_res['scores'][0]
    y_labels = gs_res['concatenated_labels'][0]

    if custom_suptitle is not None:
        suptitle = custom_suptitle
    else:
        suptitle = gs_res['suptitle']

    if plotly:
        estimators = [estimator]
        scores = [score]
        title = suptitle
        colorscale_show_ls = [True]
        X_train = gs_res['sets']['X_trains'][0]
        X_test = gs_res['sets']['X_tests'][0]
        label_train = gs_res['sets']['label_trains'][0]
        label_test = gs_res['sets']['label_tests'][0]

        X0, X1 = X[:, 0], X[:, 1]
        if simplified:
            xm = np.linspace(min(X0) - 1, max(X0) + 1, 150)
            ym = np.linspace(min(X1) - 1, max(X1) + 1, 150)
        else:
            x_min, x_max = min(X0) - 1, max(X0) + 1
            y_min, y_max = min(X1) - 1, max(X1) + 1
            xm = np.concatenate((np.arange(x_min, x_max, .02), X0))
            ym = np.concatenate((np.arange(y_min, y_max, .02), X1))
        xx, yy = np.meshgrid(xm, ym)
        Zs = np.array(estimator.predict_trace(np.c_[xx.ravel(), yy.ravel()], True))
        Z = Zs.reshape(xx.shape)
        coord_dict = {'x': xx.ravel(), 'y': yy.ravel(), 'Z': Zs}
        coord_df = pd.DataFrame(coord_dict)
        train_pred = estimator.predict(X_train)
        d_train = {'x': X_train[:, 0], 'y': X_train[:, 1], 'label': label_train,
                   'pred': train_pred}
        df_train = pd.DataFrame(d_train)
        test_pred = estimator.predict(X_test)
        d_test = {'x': X_test[:, 0], 'y': X_test[:, 1], 'label': label_test,
                  'pred': test_pred}
        df_test = pd.DataFrame(d_test)

        train_dfs = [df_train]
        test_dfs = [df_test]
        coord_dfs = [coord_df]

        plotly_shadowed_set(1, 1, estimators, train_dfs, test_dfs, coord_dfs, scores, title,
                            labels, label_colors, colorscale_ls, colorscale_show_ls, colorscale_dict, compounded=True,
                            annotation_start_x=annotation_start_x, annotation_start_y=annotation_start_y,
                            annotation_opacity=annotation_opacity, no_title=no_title, no_legend=no_legend,
                            no_subplot_titles=no_subplot_titles)
    else:
        internal_plot_shadowed_set(X, y_labels, labels, label_colors, estimator, suptitle,
                                   contourf_levels, contourf_colors, contour_levels, score=score,
                                   step=step, area_labels=area_labels, simplified=simplified, trace=trace)

    # else:
    # TODO: handle gs_res[concatenated_X] better (it may be different over the indices). Also broken models
    '''
            for i, ax, model, title, Z in zip(range(len(titles)), subs, models, titles, Zs):
                m_proxy = plot_contours(ax, xx, yy, Z, contourf_levels, contourf_colors, contour_levels)
                draw_scatterplot(ax, X, title, y_labels, labels, label_colors)

                textstr = r'score = %.3f' % (score)
                text_box = AnchoredText(textstr, frameon=True, loc=4, pad=0.5, prop=dict(fontweight="bold", fontsize=14))
                plt.setp(text_box.patch, facecolor='wheat', alpha=0.8, boxstyle='round')
                ax.add_artist(text_box)

                if i == 0:
                    handles, legend_labels = subs[0].get_legend_handles_labels() if nrows == 1 \
                        else subs[0][0].get_legend_handles_labels()
                    fig.legend(handles + m_proxy[1:],  # The line objects
                               legend_labels + area_labels,  # The labels for each line
                               loc="upper left",  # Position of legend
                               borderaxespad=0.1,  # Small spacing around legend box
                               title="Legend",  # Title for the legend
                               bbox_to_anchor=(0.01, 0.78),

                               )

            # plt.subplots_adjust(top=0.8, wspace=0.1, hspace=0.1)
    '''
    # raise NotImplementedError
    # plt.show()


def internal_plot_shadowed_set(X, y_labels, labels, label_colors, estimator, suptitle,
                               contourf_levels=np.array([-2, -1, 0, 1]),
                               contourf_colors=np.array(['white', 'pink', 'palevioletred', 'red']),
                               contour_levels=np.array([-1, 0]),
                               score=None,
                               step=.02,
                               area_labels=["shadow", "core"],
                               simplified=False, trace=True):

    title = estimator.get_plot_title()

    if simplified:
        X0, X1 = X[:, 0], X[:, 1]
        XX0 = np.linspace(min(X0) - 1, max(X0) + 1, 150)
        XX1 = np.linspace(min(X1) - 1, max(X1) + 1, 150)
        xx, yy = np.meshgrid(XX0, XX1)
    else:
        X0, X1 = X[:, 0], X[:, 1]
        xx, yy = make_meshgrid(X0, X1, step)

    Zs = np.array(estimator.predict_trace(np.c_[xx.ravel(), yy.ravel()], trace))

    wsize = 8

    fig, subs = plt.subplots(nrows=1, ncols=1, sharex=True, sharey=True, figsize=(wsize, 5))
    fig.suptitle(suptitle, fontsize=20)

    m_proxy = plot_contours(subs, xx, yy, Zs, contourf_levels, contourf_colors, contour_levels)
    draw_scatterplot(subs, X, title, y_labels, labels, label_colors)

    if score is not None:
        textstr = f'Accuracy = {score:.3f}'
        text_box = AnchoredText(textstr, frameon=True, loc=4, pad=0.5, prop=dict(fontweight="bold", fontsize=14))
        plt.setp(text_box.patch, facecolor='wheat', alpha=0.8, boxstyle='round')
        subs.add_artist(text_box)

    handles, legend_labels = subs.get_legend_handles_labels()
    fig.legend(handles + m_proxy[1:],  # The line objects
               legend_labels + area_labels,  # The labels for each line
               loc="upper left",  # Position of legend
               borderaxespad=0.1,  # Small spacing around legend box
               title="Legend",  # Title for the legend
               bbox_to_anchor=(0.01, 0.78),

               )
    plt.show()


def plot_compounded_shadowed_set(nrows, ncols, X, y_labels, labels, label_colors, models, titles, suptitle,
                               contourf_levels, contourf1_colors, contourf2_colors, contour_levels, line1_colors,
                               line2_colors, alpha):

    X0, X1 = X[:, 0], X[:, 1]
    xx, yy = make_meshgrid(X0, X1)

    Zs1 = [np.array(model.predict1(np.c_[xx.ravel(), yy.ravel()])) for model in models]
    Zs2 = [np.array(model.predict2(np.c_[xx.ravel(), yy.ravel()])) for model in models]

    wsize = 8 * ncols
    # wsize = wsize if wsize >= 10 else 10

    fig, subs = plt.subplots(nrows=nrows, ncols=ncols, sharex=True, sharey=True, figsize=(wsize, 5 * nrows))
    fig.suptitle(suptitle, fontsize=20)

    subs = subs.flatten() if nrows > 1 else subs

    if nrows == 1 and ncols == 1:
        m_proxy1 = plot_contours(subs, xx, yy, Zs1[0], contourf_levels, contourf1_colors, contour_levels, line1_colors, alpha)
        m_proxy2 = plot_contours(subs, xx, yy, Zs2[0], contourf_levels, contourf2_colors, contour_levels, line2_colors, alpha)

        draw_scatterplot(subs, X, titles[0], y_labels, labels, label_colors)

        handles, legend_labels = subs.get_legend_handles_labels()
        fig.legend(handles + m_proxy1[1:] + m_proxy2[1:],  # The line objects
                   legend_labels + ["shadow virginica", "core virginica"]
                   + ["shadow versicolor", "core versicolor"],  # The labels for each line
                   loc="upper left",  # Position of legend
                   borderaxespad=0.1,  # Small spacing around legend box
                   title="Legend",  # Title for the legend
                   bbox_to_anchor=(0.01, 0.78),

                   )

    else:
        raise NotImplementedError

        # plt.subplots_adjust(top=0.8, wspace=0.1, hspace=0.1)

    plt.show()


def plot_score_surface(param1_range, param2_range, gs, metric_name, param1_name, param2_name):
    # swapped param1_range and param2_range
    xx, yy = np.meshgrid(param2_range, param1_range)
    scores = gs.cv_results_['mean_test_' + metric_name].reshape(len(param1_range),
                                                                len(param2_range))
    fig = plt.figure(figsize=(15, 10))
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(xx, yy, scores, cmap=cm.coolwarm, linewidth=0)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    ax.set_xlabel(param2_name, fontdict={'fontsize': 16, 'fontweight': 'medium'})
    ax.set_ylabel(param1_name, fontdict={'fontsize': 16, 'fontweight': 'medium'})
    ax.set_zlabel('score', fontdict={'fontsize': 16, 'fontweight': 'medium'})
    plt.show()


def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom", fontdict={'fontsize': 16, 'fontweight': 'medium'})

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(np.round(col_labels, decimals=2))
    ax.set_yticklabels(np.round(row_labels, decimals=2))

    # Let the horizontal axes labeling appear on top.
    # ax.tick_params(top=True, bottom=False,
    #               labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=("white", "black"),
                     threshold=None, **textkw):

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


def plot_score_heatmap(param1_range, param2_range, gs, metric_name, param1_name, param2_name):
    fig, ax = plt.subplots(figsize=(15, 10))

    scores = gs.cv_results_['mean_test_' + metric_name].reshape(len(param1_range),
                                                                len(param2_range))

    im, cbar = heatmap(scores, param1_range, param2_range, ax=ax, cbarlabel="Accuratezza")
    texts = annotate_heatmap(im, valfmt="{x:.3f}")
    plt.xlabel(param1_name, fontdict={'fontsize': 16, 'fontweight': 'medium'})
    plt.ylabel(param2_name, fontdict={'fontsize': 16, 'fontweight': 'medium'})
    # ax.set_title("Validation accuracy", fontdict={'fontsize': 16, 'fontweight': 'medium'})

    fig.tight_layout()
    plt.show()