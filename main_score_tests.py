import numpy as np
import pandas as pd

from src.datasets import load_iris
from src.model_selection import grid_search, score_zoom
from src.shadow_learn import ShadowedSetEstimator
from src.utils import create_logfile, log_last_execution
from src.plot import plot_shadowed_set

log = False
create_logfile('grid_search_two_phases_iris',
              'Started Grid Search for Gurobi Two Phases Models on Iris dataset', log=log)

labels = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
label_colors = ['cornflowerblue', 'mediumseagreen', 'crimson']

contourf_colors = ['white', 'pink', 'palevioletred', 'red']

colorscale_dict = {
    'Iris-virginica': [[0, 'white'], [0.5, 'pink'], [1.0, 'palevioletred']],
    'Iris-versicolor': [[0, 'white'], [0.5, 'aquamarine'], [1.0, 'lightseagreen']],
    'Iris-setosa': [[0, 'white'], [0.5, 'lightskyblue'], [1.0, 'steelblue']]
}

results = {}

estimator = ShadowedSetEstimator(solver='gurobi-two-phases')
X, _, data_labels, _, _ = load_iris(d='all', target='Iris-versicolor', without='Iris-setosa')
dataset_loader = lambda: load_iris(d='all', target='Iris-versicolor', without='Iris-setosa')
contourf_colors = ['white', 'aquamarine', 'lightseagreen', 'red']

C0_range = np.concatenate((np.logspace(-2, 1, 2), []))
C1_range = np.concatenate((np.logspace(-2, 1, 2), []))
sigma_range = np.concatenate((np.logspace(-1, 2, 2), []))
param_grid = {'C0': C0_range, 'C1': C1_range, 'sigma': sigma_range}

gs_res = grid_search(dataset_loader, estimator, 'svc-shadowed', param_grid, dim=2,
                     log=log)

print(gs_res)


