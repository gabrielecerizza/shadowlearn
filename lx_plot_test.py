import logging
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

from src.datasets import load_census_dataset
from src.shadow_learn import ShadowedSetEstimator
from src.model_selection import grid_search, score_zoom
from src.utils import create_logfile, log_last_execution
from src.plot import plot_shadowed_set

log = False
create_logfile('grid_search_two_phases_census',
              'Started Grid Search for Gurobi Two Phases Models on census dataset', log)

labels = ['<=50K', '>50K']
label_colors = ['crimson', 'cornflowerblue']

contourf_colors = ['white', 'pink', 'palevioletred', 'red']

colorscale_dict = {
    '<=50K': [[0, 'white'], [0.5, 'pink'], [1.0, 'palevioletred']],
    '>50K': [[0, 'white'], [0.5, 'lightskyblue'], [1.0, 'steelblue']]
}

results = {}
results2d = {}

estimator = ShadowedSetEstimator(solver='gurobi-two-phases')

results2d['<=50K'] = {}
dataset_loader = lambda: load_census_dataset(d='all', target='<=50K', target_column='income')

C0_range = np.concatenate((np.logspace(-2, 1, 0), [0.01]))
C1_range = np.concatenate((np.logspace(-2, 1, 0), [0.0562341]))
sigma_range = np.concatenate((np.logspace(-1, 2, 0), [100]))
param_grid = {'C0': C0_range, 'C1': C1_range, 'sigma': sigma_range}

gs_res = grid_search(dataset_loader, estimator, 'conservative', param_grid, scaled=True, dim=2, log=log)
results2d['<=50K']['conservative'] = gs_res['scores'][0]

colorscale_ls = ['<=50K']
plot_shadowed_set(gs_res, labels, label_colors, contourf_colors,
                  colorscale_ls=colorscale_ls, colorscale_dict= colorscale_dict, plotly=True,
                  annotation_start_x=4, annotation_start_y=-4, annotation_opacity=1, simplified=True)