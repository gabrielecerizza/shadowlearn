"""
OpenEnsembles is a resource for performing and analyzing ensemble clustering

Copyright (C) 2017 Naegle Lab

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from sklearn import datasets
import math
from scipy.spatial import distance
import matplotlib.pyplot as plt
import numpy as np
import itertools
import re
import warnings
from sklearn import metrics


class validation:
    """
    validation is a class for calculating validation metrics on a data matrix (data), given the clustering labels in labels.
    Instantiation sets validation to NaN and a description to ''. Once a metric is performed, these are replaced (unless)
    validation did not yield a valid mathematical number, which can happen in certain cases, such as when a cluster
    consists of only one member. Such results will warn the user.

    Parameters
    ----------
    data: matrix of floats
        data matrix
    labels: list of ints
        The solution labels

    Attributes
    ----------
    validation: float
        Validation metric. NaN if error
    description: string
        A description of the validation metric

    See Also
    --------
    openensembles.validation()


    """
    def __init__(self, data, labels):
        self.dataMatrix = data
        self.classLabel = labels
        self.validation = np.nan
        self.description = ''

    def validation_metrics_available(self):
        """
        self.validation_metrics_available() returns a dictionary, whose keys are the available validation metrics
        """
        methods = [method for method in dir(self) if callable(getattr(self, method))]
        methods.remove('validation_metrics_available')
        methodDict = {}
        for method in methods:
            if not re.match('__', method) and not re.match('_validation__', method):
                methodDict[method] = ''
        return methodDict

    def root_mean_square(self):
        """
        The Root-Mean-Square Standard Deviation (RMSSTD), the root mean square
        standard deviation of all variables within each cluster. A measure of
        connectedness.
        """
        self.description = "The Root-Mean-Square Standard Deviation (RMSSTD), a measure of connectedness"
        numCluster = max(self.classLabel) + 1
        attributes = len(self.dataMatrix[0])
        denominator = attributes * (len(self.dataMatrix) - numCluster)
        normSum = 0
        # iterate through all the clusters
        for i in range(numCluster):
            indices = [t for t, x in enumerate(self.classLabel) if x == i]
            clusterMember = self.dataMatrix[indices, :]
            # compute the center of the cluster
            clusterCenter = np.mean(clusterMember, 0)
            # compute the square error for every member in the cluster
            for member in clusterMember:
                normSum = normSum + distance.euclidean(member, clusterCenter)
        self.validation = math.sqrt(normSum / denominator)
        return self.validation

    def r_squared(self):
        """
        R-squared, a statistical measure of how close the data is to a fitted regression line.
        A measure of compactness.
        """
        self.description = "R-squared, a measure of compactness"
        # compute the center of the dataset
        dataCenter = np.mean(self.dataMatrix, 0)
        numCluster = max(self.classLabel) + 1
        normClusterSum = 0
        normDatasetSum = 0
        # iterate through all the clusters
        for i in range(numCluster):
            indices = [t for t, x in enumerate(self.classLabel) if x == i]
            clusterMember = self.dataMatrix[indices, :]
            # compute the center of the cluster
            clusterCenter = np.mean(clusterMember, 0)
            # compute the norm for every member in the cluster with cluster center and dataset center
            for member in clusterMember:
                normClusterSum = normClusterSum + distance.euclidean(member, clusterCenter)
                normDatasetSum = normDatasetSum + distance.euclidean(member, dataCenter)
        # compute the fitness
        self.validation = (normDatasetSum - normClusterSum) / normDatasetSum
        return self.validation

    def modified_hubert_t(self):
        """
        The Modified Hubert T Statistic, a measure of compactness.
        """
        self.description = "The Modified Hubert T Statistic, a measure of compactness"
        sumDiff = 0
        # compute the centers of all the clusters
        list_center = []
        numCluster = max(self.classLabel) + 1
        for i in range(numCluster):
            indices = [t for t, x in enumerate(self.classLabel) if x == i]
            clusterMember = self.dataMatrix[indices, :]
            list_center.append(np.mean(clusterMember, 0))
        size = len(self.classLabel)
        # iterate through each of the two pairs exhaustively
        for i in range(size - 1):
            for j in range(i + 1, size):
                # get the cluster labels of the two objects
                label1 = self.classLabel[i]
                label2 = self.classLabel[j]
                # compute the distance of the two objects
                pairDistance = distance.euclidean(self.dataMatrix[i], self.dataMatrix[j])
                # compute the distance of the cluster center of the two objects
                centerDistance = distance.euclidean(list_center[label1], list_center[label2])
                # add the product to the sum
                sumDiff = sumDiff + pairDistance * centerDistance
        # compute the fitness
        self.validation = 2 * sumDiff / (size * (size - 1))
        return self.validation

    def Davies_Bouldin(self):
        """
        The Davies-Bouldin index, the average of all cluster similarities.
        """
        self.description = "The Davies-Bouldin index, the average of all cluster similarities"
        numCluster = max(self.classLabel) + 1
        list_max = []
        # iterate through the clusters
        for i in range(numCluster):
            list_tempMax = []
            # get all members from cluster i
            indices1 = [t for t, x in enumerate(self.classLabel) if x == i]
            clusterMember1 = self.dataMatrix[indices1, :]
            # compute the cluster center
            clusterCenter1 = np.mean(clusterMember1, 0)
            # compute the cluster norm sum
            sumNorm1 = 0
            for member in clusterMember1:
                sumNorm1 = sumNorm1 + distance.euclidean(member, clusterCenter1)
            for j in range(numCluster):
                if j != i:
                    # get all members from cluster j
                    indices2 = [t for t, x in enumerate(self.classLabel) if x == j]
                    clusterMember2 = self.dataMatrix[indices2, :]
                    # compute the cluster center
                    clusterCenter2 = np.mean(clusterMember2, 0)
                    # compute the cluster norm sum
                    sumNorm2 = 0
                    for member in clusterMember2:
                        sumNorm2 = sumNorm2 + distance.euclidean(member, clusterCenter2)
                    tempDis = (sumNorm1 / len(indices1) + sumNorm2 / len(indices2)) / distance.euclidean(clusterCenter1,
                                                                                                         clusterCenter2)
                    list_tempMax.append(tempDis)
            list_max.append(max(list_tempMax))
        # compute the fitness
        self.validation = sum(list_max) / numCluster
        return self.validation

    def Xie_Beni(self):
        """
        The Xie-Beni index, a measure of compactness.
        """
        self.description = "The Xie-Beni index, a measure of compactness"
        numCluster = max(self.classLabel) + 1
        numObject = len(self.classLabel)
        sumNorm = 0
        list_centers = []
        for i in range(numCluster):
            # get all members from cluster i
            indices = [t for t, x in enumerate(self.classLabel) if x == i]
            clusterMember = self.dataMatrix[indices, :]
            # compute the cluster center
            clusterCenter = np.mean(clusterMember, 0)
            list_centers.append(clusterCenter)
            # interate through each member of the cluster
            for member in clusterMember:
                sumNorm = sumNorm + math.pow(distance.euclidean(member, clusterCenter), 2)
        minDis = min(distance.pdist(list_centers))
        # compute the fitness
        self.validation = sumNorm / (numObject * pow(minDis, 2))
        return self.validation

    ## density function for SDBW
    @staticmethod
    def __density(a, b, stdev):
        dis = distance.euclidean(a, b)
        if dis > stdev:
            return 0
        else:
            return 1

    def s_dbw(self):
        """
        The S_Dbw index, a measure of compactness.
        """
        self.description = "The S_Dbw index, a measure of compactness"
        sumDens = 0
        sumNormCluster = 0
        sumScat = 0
        list_centers = []
        numCluster = max(self.classLabel) + 1
        # compute the norm of sigma(dataset)
        normSigDataset = np.linalg.norm(np.var(self.dataMatrix, 0))
        # iterate through all the clusters of self.classLabel
        for i in range(numCluster):
            # get all members from cluster i
            indices = [t for t, x in enumerate(self.classLabel) if x == i]
            clusterMember = self.dataMatrix[indices, :]
            # compute the cluster center
            clusterCenter = np.mean(clusterMember, 0)
            list_centers.append(clusterCenter)
            normSigCluster = np.linalg.norm(np.var(clusterMember, 0))
            sumScat = sumScat + normSigCluster / normSigDataset
            sumNormCluster = sumNormCluster + normSigCluster
        # compute stdev
        stdev = math.sqrt(sumNormCluster) / numCluster
        # iterate again for density_bw
        for i in range(numCluster):
            sumDensity1 = 0
            sumTemp = 0
            # get all members from cluster i
            indices1 = [t for t, x in enumerate(self.classLabel) if x == i]
            clusterMember1 = self.dataMatrix[indices1, :]
            # compute sum of f(x,ci)
            for member in clusterMember1:
                sumDensity1 = sumDensity1 + validation.__density(member, list_centers[i], stdev)
            for j in range(numCluster):
                if j != i:
                    sumDensity2 = 0
                    sumDensityCombine = 0
                    # get all members from cluster j
                    indices2 = [t for t, x in enumerate(self.classLabel) if x == j]
                    clusterMember2 = self.dataMatrix[indices2, :]
                    # compute sum of f(x,cj)
                    for member in clusterMember2:
                        sumDensity2 = sumDensity2 + validation.__density(member, list_centers[j], stdev)
                    # compute the middle point of the two cluster centers
                    midPoint = []
                    for k in range(len(list_centers[0])):
                        midPoint.append((list_centers[i][k] + list_centers[j][k]) / 2)
                    # compute sum of f(x,uij)
                    combined = clusterMember1 + clusterMember2
                    for member in combined:
                        sumDensityCombine = sumDensityCombine + validation.__density(member, midPoint, stdev)
                    sumTemp = sumTemp + sumDensityCombine / max([sumDensity1, sumDensity2])
            sumDens = sumDens + sumTemp
        # compute scat and dens_bw
        scat = sumScat / numCluster
        dens_bw = sumDens / (numCluster * (numCluster - 1))
        # compute the fitness
        self.validation = scat + dens_bw
        return self.validation

    def Dunns_index(self):
        """
        Dunn's index, a measure of cluster compactness
        """
        self.description = "Dunn's Index, a measure of compactness"
        list_diam = []
        list_minDis = []
        numCluster = max(self.classLabel) + 1
        # iterate through the clusters
        for i in range(numCluster - 1):
            # get all members from cluster i
            indices1 = [t for t, x in enumerate(self.classLabel) if x == i]
            clusterMember1 = self.dataMatrix[indices1, :]
            # compute the diameter of the cluster
            list_diam.append(max(distance.pdist(clusterMember1)))
            for j in range(i + 1, numCluster):
                # get all members from cluster j
                indices2 = [t for t, x in enumerate(self.classLabel) if x == j]
                clusterMember2 = self.dataMatrix[indices2, :]
                # compute the diameter of the cluster
                diameter = distance.pdist(clusterMember2)
                # If it is zero, the value is undefined
                if len(diameter) == 0:
                    warnings.warn('Cannot calculate Dunns_index, due to an undefined value', UserWarning)
                    self.validation = 0
                    return self.validation
                list_diam.append(max(diameter))
                # get the pairwise distance and find the minimum
                pairDis = distance.cdist(clusterMember1, clusterMember2)
                minDis = min(list(itertools.chain(*pairDis)))
                list_minDis.append(minDis)
        # compute the fitness
        return min(list_minDis) / max(list_diam)