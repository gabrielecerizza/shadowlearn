"""
Created on Wed May 25 04:20:00 CEST 2016

@authors: Juan C Entizne, Juan L. Trincado
@email: juancarlos.entizne01[at]estudiant.upf.edu,
        juanluis.trincado[at]upf.edu
"""

import os
import logging
from itertools import islice
from sklearn.cluster import DBSCAN
from collections import defaultdict
from sklearn.metrics import silhouette_score
import numpy as np

# https://www.programcreek.com/python/example/89273/sklearn.metrics.silhouette_score
def calculate_rmsstd(cluster_vectors):

    n_cond = len(cluster_vectors[0])
    # Degrees of Freedom
    dof = n_cond-1

    return np.sqrt(sum(np.std(cluster_vectors, axis=0))/dof)