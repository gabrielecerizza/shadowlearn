import logging
import numpy as np
import pandas as pd

from src.shadow_learn import ShadowedSetEstimator, WILF2018Adapter
from src.model_selection import model_comparison
from src.utils import create_logfile, log_last_execution
from src.datasets import load_iris

log = False
create_logfile('vs_iris', 'Started Model Comparisons on Iris dataset', log)

estimators = []
param_grids = []

dataset = [
    lambda: (lambda: load_iris(d='all', target=target) for target in ['Iris-setosa']),
    ['Iris-setosa']
]

gurobi_two_phases_double_constraint = ShadowedSetEstimator(solver='gurobi-two-phases-double-constraint')
gurobi_two_phases_double_constraint_param_grid = {'C0': np.concatenate((np.logspace(-1, 1, 2), [])),
                                                  'C1': np.concatenate((np.logspace(-1, 1, 2), [])),
                                                  'sigma': np.concatenate((np.logspace(-1, 2, 2), []))}
estimators.append(gurobi_two_phases_double_constraint)
param_grids.append(gurobi_two_phases_double_constraint_param_grid)

cons_df = model_comparison(dataset, estimators, 'conservative', param_grids, main_metric_name='conservative', n_splits=5,
                           cv_num=5, verbose=0, dim=2, nested=True, op='a', log=log)
print(cons_df)