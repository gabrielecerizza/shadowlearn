import logging
import numpy as np
import pandas as pd

from src.datasets import load_covid_dataset
from src.shadow_learn import ShadowedSetEstimator, WILF2018Adapter
from src.model_selection import model_comparison
from src.utils import create_logfile, log_last_execution, load_stored_csv

log = False
op = 'w'
separate_targets = True
create_logfile('vs_shadow_covid', 'Started Model Comparisons on shadow covid dataset', log)

estimators = []
param_grids = []
dataset = [
    lambda: (lambda: load_covid_dataset(target=target, shadow_label='LABEL-3', only_shadow=True) for target in [0, 1]),
    [0, 1]
]
additional_test_metrics = {
    'multi-class-auc': 'multi-class-auc',
    #'sensitivity': 'sensitivity',
    #'specificity': 'specificity',
    #'full-f1-score': 'full-f1-score',
    #'precision': 'precision'
}

gurobi_two_phases_shadow = ShadowedSetEstimator(solver='gurobi-two-phases-shadow')
gurobi_two_phases_shadow_param_grid = {'C0': np.concatenate((np.logspace(-1, 1, 2), [])),
                                'C1': np.concatenate((np.logspace(-1, 1, 2), [1.325])),
                                'sigma': np.concatenate((np.logspace(-1, 2, 2), [7.22222, 10]))}
estimators.append(gurobi_two_phases_shadow)
param_grids.append(gurobi_two_phases_shadow_param_grid)

ac3_df = model_comparison(dataset, estimators, 'accuracy', param_grids,
                                n_splits=5, cv_num=5, main_metric_name='accuracy', scaled=True,
                                additional_test_metrics=additional_test_metrics, filename='ac3',
                                separate_targets=separate_targets, nested=True, verbose=0, op=op, log=log)
print(ac3_df)