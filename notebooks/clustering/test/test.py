import unittest
from src import metrics as smet
from sklearn import datasets


# The values used for double checking the results are obtained thanks to the R
# package clusterCrit. As these values are rounded to the precision of the
# asserts properties have been adjusted in accordance
class internal_index_test(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(internal_index_test, self).__init__(*args, **kwargs)
        iris = datasets.load_iris()
        self.data = iris.data
        self.labels = iris.target

    def test_xie_beni(self):
        value = 11.91824
        self.assertAlmostEqual(
            smet.xie_beni_index(self.data, self.labels),
            value,
            places=1)


if __name__ == '__main__':
    unittest.main()