\documentclass[a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english,italian]{babel}
\usepackage{amsthm}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage[makeroom]{cancel}
\usepackage{physics}

\numberwithin{equation}{section}

\newtheorem{corollary}{Corollario}[section]
\theoremstyle{definition}
\newtheorem{definition}{Definizione}[section]

\theoremstyle{plain}
\newtheorem{proposition}{Proposizione}[section]

%\newcommand{\norm}[1]{\lVert#1\rVert_2}
%\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclareMathOperator{\Lagr}{\mathscr{L}}

\begin{document}

\author{Gabriele Cerizza}
\title{Relazione}
\maketitle

\tableofcontents

\section{Linear Second Phase}
\begin{definition}
    Il problema primale della seconda fase è così definito:
    \begin{gather*}
        \begin{alignat*}{3}
            &\!\min_{R_0^2, \tau_i} &\qquad& R_0^2 + C\sum_i \tau_i                     &\qquad& \\
            &\text{subject to}      &      & \norm{\phi(x_i) - a}^2 \geq R_0^2 - \tau_i &\qquad& \forall i : \mu_i = 0\\
            &                       &      & \tau_i \geq 0                              &\qquad& \forall i : \mu_i = 0\\
            &                       &      & R_0^2 \geq R_1^2 &\qquad&
        \end{alignat*}
    \end{gather*}
\end{definition}

\begin{definition}
    Il duale di Wolfe è un problema di programmazione lineare così definito:
    \begin{gather*}
        \begin{alignat*}{3}
            &\!\max_{\gamma, \alpha_i} &\qquad& \gamma R_1^2 - \sum_i \alpha_i \norm{\phi(x_i) - a}^2     &\qquad& \\
            &\text{subject to}      &      & \sum_i \alpha_i = \gamma - 1                                 &\qquad& \\
            &                       &      & 0 \leq \alpha_i \leq C                                       &\qquad& \forall i : \mu_i = 0 \\
            &                       &      & \gamma \geq 0 &\qquad&
        \end{alignat*}
    \end{gather*}
\end{definition}
\noindent
In particolare $\gamma$ è sempre strettamente maggiore di zero, poiché vale:
\begin{gather}
    \alpha_i \geq 0 \quad \forall i : \mu_i = 0 \implies \sum_i \alpha_i \geq 0 \notag \\
    \gamma = \sum_i \alpha_i + 1 \implies \gamma > 0 \label{eqn:gammaGreaterThanZero}
\end{gather}
\\
Le condizioni KKT sono:

\begin{alignat}{2}
    &\alpha_i (\norm{\phi(x_i) - a}^2 - R_0^2 + \tau_i) = 0     &\qquad& \forall i : \mu_i = 0 \\
    &\beta_i \tau_i = 0                                         &\qquad& \forall i : \mu_i = 0 \\
    &\gamma (R_0^2 - R_1^2) = 0                                 &\qquad& \label{eqn:gammaComplementarityCondition}
\end{alignat}
\\
\begin{proposition}
    Il valore ottimo di $R_0^2$ coincide sempre con $R_1^2$.
\end{proposition}

\begin{proof}
    Per la~(\ref{eqn:gammaComplementarityCondition}):
    \begin{equation*}
        \gamma > 0 \quad \implies \quad R_0^2 - R_1^2 = 0
    \end{equation*}
    Quindi:
    \begin{equation*}
        \gamma > 0 \quad \implies \quad R_0^2 = R_1^2
    \end{equation*}
    Poiché $\gamma$ risulta sempre strettamente maggiore di zero per la~(\ref{eqn:gammaGreaterThanZero}), la proposizione è dimostrata.  
\end{proof}


\section{Single Phase without $\psi$}
\begin{definition}
    Il problema primale è così definito:
    \begin{gather*}
        \begin{alignat*}{3}
            &\!\min_{a, R_0^2, R_1^2, \xi_i, \tau_j}    &\qquad& R_1^2 + R_0^2 + C\sum_i \xi_i + C\sum_j \tau_j     &\qquad& \\
            &\text{subject to}                          &      & \norm{\phi(x_i) - a}^2 \leq R_1^2 + \xi_i          &\qquad& \forall i : \mu_i = 1 \\
            &                                           &      & \norm{\phi(x_j) - a}^2 \geq R_0^2 - \tau_j         &\qquad& \forall j : \mu_j = 0 \\
            &                                           &      & \xi_i \geq 0                                       &\qquad& \forall i : \mu_i = 1 \\
            &                                           &      & \tau_j \geq 0                                      &\qquad& \forall j : \mu_j = 0 \\
            &                                           &      & R_0^2 \geq R_1^2                                   &\qquad&
        \end{alignat*}
    \end{gather*}
\end{definition}
\noindent
La funzione Lagrangiana è:

\begin{align*}
    \begin{split}
        \Lagr ={}& R_1^2 + R_0^2 + C\sum_i \xi_i + C\sum_j \tau_j - \sum_i \alpha_i (R_1^2 + \xi_i - \norm{\phi(x_i) - a}^2) \\
             & - \sum_j \beta_j (\norm{\phi(x_j) - a}^2 - R_0^2 + \tau_j) - \sum_i \gamma_i \xi_i - \sum_j \delta_j \tau_j \\
             & - \epsilon(R_0^2 - R_1^2)
    \end{split}\\
    \begin{split}
         ={}& R_1^2 (1 - \sum_i \alpha_i + \epsilon) + R_0^2 (1 + \sum_j \beta_j - \epsilon) + \sum_i \xi_i (C - \alpha_i - \gamma_i) \\
             & + \sum_j \tau_j (C - \beta_j - \delta_j) + \sum_i \alpha_i \norm{\phi(x_i) - a}^2 - \sum_j \beta_j \norm{\phi(x_j) - a}^2
    \end{split}
\end{align*}
\\
Valgono le condizioni:
\begin{gather}
    \pdv{\Lagr}{R_0^2}  := 1 + \sum_j \beta_j - \epsilon = 0 \quad    \implies    \quad     \epsilon = \sum_j \beta_j + 1 \label{eqn:epsilonSum}\\
    \beta_j \geq 0 \qquad \forall j : \mu_j = 0 \label{eqn:betaGreaterThanZero}
\end{gather}
\\
Le condizioni KKT sono:
\begin{alignat}{2}
    &\alpha_i (R_1^2 + \xi_i - \norm{\phi(x_i) - a}^2) = 0 &\qquad& \forall i : \mu_i = 1 \\
    &\beta_j (\norm{\phi(x_j) - a}^2 - R_0^2 + \tau_j) = 0 &\qquad& \forall j : \mu_j = 0 \\
    &\gamma_i \xi_i = 0 &\qquad& \forall i : \mu_i = 1 \\
    &\delta_j \tau_j = 0 &\qquad& \forall j : \mu_j = 0  \\
    &\epsilon (R_0^2 - R_1^2) = 0                                 &\qquad& \label{eqn:epsilonComplementarityCondition}
\end{alignat}
\\
\begin{proposition}
    Il valore ottimo di $R_0^2$ coincide sempre con $R_1^2$.
\end{proposition}

\begin{proof}
    Per la~(\ref{eqn:epsilonSum}) e la~(\ref{eqn:betaGreaterThanZero}) $\epsilon$ risulta sempre strettamente maggiore di zero:
    \begin{equation*}
        \epsilon = \sum_j \beta_j + 1 \quad \implies \quad \epsilon > 0
    \end{equation*}
    \\
    E per la~(\ref{eqn:epsilonComplementarityCondition}):
    \begin{equation*}
        \epsilon > 0 \quad \implies \quad R_0^2 = R_1^2
    \end{equation*}
\end{proof}

\section{Single Phase with $\psi$}
\begin{definition}
    Il problema primale è così definito:
    \begin{gather*}
        \begin{alignat*}{3}
            &\!\min_{a, R_0^2, R_1^2, \xi_i, \tau_j}    &\qquad& R_1^2 + R_0^2 + C\sum_i \xi_i + C\sum_j \tau_j     &\qquad& \\
            &\text{subject to}                          &      & \norm{\phi(x_i) - a}^2 \leq R_1^2 + \xi_i          &\qquad& \forall i : \mu_i = 1 \\
            &                                           &      & \norm{\phi(x_j) - a}^2 \geq R_0^2 - \tau_j         &\qquad& \forall j : \mu_j = 0 \\
            &                                           &      & \xi_i \geq 0                                       &\qquad& \forall i : \mu_i = 1 \\
            &                                           &      & \tau_j \geq 0                                      &\qquad& \forall j : \mu_j = 0 \\
            &                                           &      & R_0^2 \geq R_1^2 + \psi                                  &\qquad&
        \end{alignat*}
    \end{gather*}
\end{definition}
\noindent
La funzione Lagrangiana è:

\begin{align*}
    \begin{split}
        \Lagr ={}& R_1^2 + R_0^2 + C\sum_i \xi_i + C\sum_j \tau_j - \sum_i \alpha_i (R_1^2 + \xi_i - \norm{\phi(x_i) - a}^2) \\
             & - \sum_j \beta_j (\norm{\phi(x_j) - a}^2 - R_0^2 + \tau_j) - \sum_i \gamma_i \xi_i - \sum_j \delta_j \tau_j \\
             & - \epsilon(R_0^2 - R_1^2 - \psi)
    \end{split}\\
    \begin{split}
         ={}& R_1^2 (1 - \sum_i \alpha_i + \epsilon) + R_0^2 (1 + \sum_j \beta_j - \epsilon) + \sum_i \xi_i (C - \alpha_i - \gamma_i) \\
             & + \sum_j \tau_j (C - \beta_j - \delta_j) + \sum_i \alpha_i \norm{\phi(x_i) - a}^2 - \sum_j \beta_j \norm{\phi(x_j) - a}^2 + \epsilon \psi
    \end{split}
\end{align*}
\\
Le First order condition sono:

\begin{subequations}
    \begin{alignat}{5}
        &\pdv{\Lagr}{R_1^2}     :={}&& 1 - \sum_i \alpha_i + \epsilon = 0  &\quad&    \implies    &\quad&     \sum_i \alpha_i = \epsilon + 1 \label{eqn-single:sum_alpha_i} \\
        &\pdv{\Lagr}{R_0^2}     :={}&& 1 + \sum_j \beta_j - \epsilon = 0   &\quad&    \implies    &\quad&     \sum_j \beta_j = \epsilon - 1 \label{eqn-single:sum_beta_j} \\
        &\pdv{\Lagr}{\xi_i}    :={}&& C - \alpha_i - \gamma_i = 0         &\quad&    \implies    &\quad&     C = \alpha_i + \gamma_i  &\quad&     \forall i : \mu_i = 1 \label{eqn-single:c_alpha_gamma} \\
        &\pdv{\Lagr}{\tau_j}    :={}&& C - \beta_j - \delta_j = 0          &\quad&    \implies    &\quad&     C = \beta_j + \delta_j   &\quad&     \forall j : \mu_j = 0 \label{eqn-single:c_beta_delta}
    \end{alignat}
    \begin{align}
        \pdv{\Lagr}{a} :={} & -2 \sum_i \alpha_i \phi(x_i) + 2 a \underbrace{\sum_i \alpha_i}_{=~\epsilon + 1} + 2 \sum_j \beta_j \phi(x_j) - 2 a \underbrace{\sum_j \beta_j}_{=~\epsilon - 1} = 0 \quad \notag \\
                            & -2 \sum_i \alpha_i \phi(x_i) + \cancel{2 a \epsilon} + 2 a + 2 \sum_j \beta_j \phi(x_j) - \cancel{2 a \epsilon} + 2 a = 0 \notag \\
                            & ~4a = 2 \sum_i \alpha_i \phi(x_i) - 2 \sum_j \beta_j \phi(x_j) \notag \\
                            & ~2a = \sum_i \alpha_i \phi(x_i) - \sum_j \beta_j \phi(x_j) \label{eqn-single:two_a}
    \end{align}
\end{subequations}
\\
Valgono inoltre:
\begin{align}
    \alpha_i, \gamma_i \geq 0 \qquad & \forall i : \mu_i = 1 \label{eqn-single:alpha_gamma} \\
    \beta_j, \delta_j \geq 0 \qquad & \forall j : \mu_j = 0 \label{eqn-single:beta_delta}
\end{align}
\newpage
\noindent
La funzione Lagrangiana diventa quindi:

\begin{align*}
    \begin{split}
        \Lagr   ={} & R_1^2 \underbrace{(1 - \sum_i \alpha_i + \epsilon)}_{=~0} + R_0^2 \underbrace{(1 + \sum_j \beta_j - \epsilon)}_{=~0} + \sum_i \xi_i \underbrace{(C - \alpha_i - \gamma_i)}_{=~0} \\
                    & + \sum_j \tau_j \underbrace{(C - \beta_j - \delta_j)}_{=~0} + \sum_i \alpha_i \norm{\phi(x_i) - a}^2 - \sum_j \beta_j \norm{\phi(x_j) - a}^2 + \epsilon \psi
    \end{split}\\
    ={} & \sum_i \alpha_i \norm{\phi(x_i) - a}^2 - \sum_j \beta_j \norm{\phi(x_j) - a}^2 + \epsilon \psi \\
    \begin{split}
                ={} & \sum_i \alpha_i \phi(x_i) \cdot \phi(x_i) - 2a \sum_i \alpha_i \phi(x_i) + a \cdot a \underbrace{\sum_i \alpha_i}_{=~\epsilon + 1} - \sum_j \beta_j \phi(x_j) \cdot \phi(x_j) \\
                    & + 2a \sum_j \beta_j \phi (x_j) - a \cdot a \underbrace{\sum_j \beta_j}_{=~\epsilon - 1} + \epsilon \psi
    \end{split}\\
    ={} & \sum_i \alpha_i k(x_i, x_i) - \sum_j \beta_j k (x_j, x_j) + 2a \cdot a + \epsilon \psi -2a \underbrace{\biggl(\sum_i \alpha_i \phi (x_i) - \sum_j \beta_j \phi (x_j) \biggr)}_{=~2a ~\text{per la (\ref{eqn-single:two_a})}} \\
    ={} & \sum_i \alpha_i k(x_i, x_i) - \sum_j \beta_j k (x_j, x_j) + \underbrace{\epsilon \psi}_{=~(\sum_j \beta_j + 1) \psi} -2a \cdot a \\
    \begin{split}
        ={} & \sum_i \alpha_i k(x_i, x_i) - \sum_j \beta_j k (x_j, x_j) + \psi \sum_j \beta_j + \psi + \frac{1}{2} \sum_i \sum_j \alpha_i \beta_j k(x_i, x_j) \\
            & - \frac{1}{2} \sum_j \sum_h \beta_j \beta_h k(x_j, x_h) - \frac{1}{2} \sum_i \sum_z \alpha_i \alpha_z k (x_i, x_z) + \frac{1}{2} \sum_i \sum_j \alpha_i \beta_j k(x_i, x_j)
    \end{split}\\
    \begin{split}
        ={} & \sum_i \alpha_i k(x_i, x_i) - \sum_j \beta_j k (x_j, x_j) + \sum_i \sum_j \alpha_i \beta_j k(x_i, x_j) - \frac{1}{2} \sum_i \sum_z \alpha_i \alpha_z k (x_i, x_z) \\
            & - \frac{1}{2} \sum_j \sum_h \beta_j \beta_h k(x_j, x_h) + \psi \sum_j \beta_j  + \psi
    \end{split}
\end{align*}
\\
Per la (\ref{eqn-single:sum_alpha_i}) e la (\ref{eqn-single:sum_beta_j}) vale:

\begin{equation*}
    \sum_i \alpha_i = \sum_j \beta_j + 2
\end{equation*}
\\
Per la (\ref{eqn-single:c_alpha_gamma}) e la (\ref{eqn-single:alpha_gamma}) vale:

\begin{equation*}
    0 \leq \alpha_i \leq C \qquad \forall i : \mu_i = 1
\end{equation*}
\\
Analogamente per la (\ref{eqn-single:c_beta_delta}) e la (\ref{eqn-single:beta_delta}) vale:

\begin{equation}
    0 \leq \beta_j \leq C \qquad \forall j : \mu_j = 0 \label{eqn-single:betaGreaterThanZero}
\end{equation}
\\
\begin{definition}
    Il duale di Wolfe può essere così definito:
    \begin{gather*}
        \begin{alignat*}{3}
            &\!\max_{\alpha_i, \beta_j} &\qquad& \sum_i \alpha_i k(x_i, x_i) - \sum_j \beta_j k (x_j, x_j) + \sum_i \sum_j \alpha_i \beta_j k(x_i, x_j) \\ 
            &                          &\qquad& - \frac{1}{2} \sum_i \sum_z \alpha_i \alpha_z k (x_i, x_z) - \frac{1}{2} \sum_j \sum_h \beta_j \beta_h k(x_j, x_h)   \\
            &                          &\qquad& + \psi \sum_j \beta_j + \psi \\
            &\text{subject to}      &      & \sum_i \alpha_i = \sum_j \beta_j + 2                                  \\
            &                       &      & 0 \leq \alpha_i \leq C                                     \qquad \qquad \qquad \forall i : \mu_i = 1 \\
            &                       &      & 0 \leq \beta_j \leq C                                       \qquad \qquad \qquad \forall j : \mu_j = 0 
        \end{alignat*}
    \end{gather*}
\end{definition}
\noindent
Le condizioni KKT sono:

\begin{alignat}{2}
    &\alpha_i (R_1^2 + \xi_i - \norm{\phi(x_i) - a}^2) = 0 &\qquad& \forall i : \mu_i = 1 \label{eqn-single:alphaComplementarityCondition} \\
    &\beta_j (\norm{\phi(x_j) - a}^2 - R_0^2 + \tau_j) = 0 &\qquad& \forall j : \mu_j = 0 \label{eqn-single:betaComplementarityCondition} \\
    &\gamma_i \xi_i = 0 &\qquad& \forall i : \mu_i = 1 \label{eqn-single:gammaComplementarityCondition} \\
    &\delta_j \tau_j = 0 &\qquad& \forall j : \mu_j = 0  \label{eqn-single:deltaComplementarityCondition} \\
    &\epsilon (R_0^2 - R_1^2 - \psi) = 0                                 &\qquad& \label{eqn-single:epsilonComplementarityCondition}
\end{alignat}
\\
\begin{corollary}
    Risulta che $R_0^2$ sia sempre pari a $R_1^2 + \psi$.
\end{corollary}

\begin{proof}
    Per la (\ref{eqn-single:sum_beta_j}) e la (\ref{eqn-single:betaGreaterThanZero}) $\epsilon$ è sempre strettamente maggiore di zero:
    \begin{equation*}
        \epsilon = \sum_j \underbrace{\beta_j}_{\geq ~0} + 1  \quad    \implies    \quad     \epsilon > 0 \\
    \end{equation*}
    \\
    Per la (\ref{eqn-single:epsilonComplementarityCondition}):
    \begin{equation*}
        \epsilon > 0   \quad    \implies    \quad     R_0^2 - R_1^2 - \psi = 0 \\
    \end{equation*}
    \\
    Quindi:
    \begin{equation*}
        \epsilon > 0   \quad    \implies   \quad     R_0^2 = R_1^2 + \psi \\
    \end{equation*}
\end{proof}

\begin{corollary}
    $R_1^2$ si può ricavare dall'espressione $\norm{\phi (x_i) - a}^2$ in corrispondenza di tutti gli $\alpha_i : 0 < \alpha_i < C $.  
\end{corollary}

\begin{proof}
    Per la (\ref{eqn-single:alphaComplementarityCondition}):
    \begin{equation*}
        \alpha_i > 0   \quad    \implies    \quad     \norm{\phi (x_i) - a}^2 = R_1^2 + \xi_i \qquad \forall i : \mu_i = 1 \\
    \end{equation*}
    Per la (\ref{eqn-single:c_alpha_gamma}) e la (\ref{eqn-single:gammaComplementarityCondition}):
    \begin{equation*}
        \alpha_i < C   \quad    \implies    \quad  \gamma_i > 0 \quad    \implies    \quad   \xi_i = 0 \qquad \forall i : \mu_i = 1 \\
    \end{equation*}
    Quindi:
    \begin{equation*}
        0 < \alpha_i < C   \quad    \implies    \quad  \norm{\phi (x_i) - a}^2 = R_1^2 \qquad \forall i : \mu_i = 1 \\
    \end{equation*}
\end{proof}

\begin{corollary}
    $R_0^2$ si può ricavare dall'espressione $\norm{\phi (x_j) - a}^2$ in corrispondenza di tutti i $\beta_j : 0 < \beta_j < C $.  
\end{corollary}

\begin{proof}
    Per la (\ref{eqn-single:betaComplementarityCondition}):
    \begin{equation*}
        \beta_j > 0   \quad    \implies    \quad     \norm{\phi (x_j) - a}^2 = R_0^2 - \tau_j \qquad \forall j : \mu_j = 0 \\
    \end{equation*}
    Per la (\ref{eqn-single:c_beta_delta}) e la (\ref{eqn-single:deltaComplementarityCondition}):
    \begin{equation*}
        \beta_j < C   \quad    \implies    \quad  \delta_j > 0 \quad    \implies    \quad   \tau_j = 0 \qquad \forall j : \mu_j = 0 \\
    \end{equation*}
    Quindi:
    \begin{equation*}
        0 < \beta_j < C   \quad    \implies    \quad  \norm{\phi (x_j) - a}^2 = R_0^2 \qquad \forall j : \mu_j = 0 \\
    \end{equation*}
\end{proof}

\begin{definition}
    Sfruttando la (\ref{eqn-single:two_a}), $\norm{\phi (x) - a}^2$ può essere così definita:

    \begin{align*}
        \norm{\phi (x) - a}^2 ={}& \phi(x) \cdot \phi(x) -2a \cdot \phi(x) + a \cdot a \\
        \begin{split}
            ={}& k(x, x) - \biggl(\sum_i \alpha_i^* \phi (x_i) - \sum_j \beta_j^* \phi(x_j) \biggr) \cdot \phi(x) \\
                & + \biggl(\frac{1}{2} \sum_i \alpha_i^* \phi(x_i) - \frac{1}{2} \sum_j \beta_j^* \phi(x_j) \biggr)^2
        \end{split}\\
        \begin{split}
            ={}& k(x, x) - \sum_i \alpha_i^* k(x_i, x) + \sum_j \beta_j^* k(x_j, x) \\
                & + \underbrace{\frac{1}{4} \sum_i \sum_z \alpha_i^* \alpha_z^* k(x_i, x_z) - \frac{1}{2} \sum_i \sum_j \alpha_i^* \beta_j^* k(x_i, x_j) + \frac{1}{4} \sum_j \sum_h \beta_j^* \beta_h^* k(x_j, x_h)}_{\text{termine fisso}}
    \end{split}
    \end{align*}
\end{definition}

\end{document}