\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{amssymb,amsmath,amsthm}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{physics}
\usepackage[makeroom]{cancel}
\usepackage{subfig}
\usepackage[labelfont=bf, format=hang]{caption}
\usepackage{rotating}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{bm}
\usepackage[makeroom]{cancel}

\usetheme[progressbar=frametitle]{metropolis}
\setbeamertemplate{frame numbering}[fraction]
\useoutertheme{metropolis}
\useinnertheme{metropolis}
\usefonttheme{metropolis}
\usecolortheme{spruce}
\setbeamercolor{background canvas}{bg=white}
\setbeamercovered{invisible}

\title{Induzione di shadowed set tramite tecniche di one-class clustering}
\date{}
\author{Gabriele Cerizza}
\institute{Università degli Studi di Milano}

\begin{document}
\maketitle

\begin{frame}{Obiettivi e definizioni}
    \begin{itemize}
        \item[$\bullet$] \textbf{Che problemi vogliamo risolvere?}
        \begin{itemize}
            \item[$\circ$] Problemi di classificazione, con individuazione di \textbf{“casi difficili”} a partire dai dati
            \begin{itemize}
                \item[-] Importanza in ambito medico
            \end{itemize} 
        \end{itemize}
    \end{itemize}
    \begin{minipage}{.5\linewidth}
        \begin{itemize}
            \item[$\bullet$] \textbf{Cos’è uno shadowed set?}
            \begin{itemize}
                \item[$\circ$] Insieme con tre regioni
                \begin{itemize}
                    \item[-] Core
                    \item[-] Zona di esclusione 
                    \item[-] Shadow
                \end{itemize}
            \end{itemize}
        \end{itemize}
        \end{minipage}
        \hfill
        \begin{minipage}{.4\linewidth}
            \centering
            \includegraphics[width=\linewidth]{img/slides/shadowed_set_example2.png}
    \end{minipage} 

\end{frame}

\begin{frame}{Algoritmo di induzione di shadowed set}
    \begin{itemize}
        \item[$\bullet$] \textbf{Modello principale a due fasi}
        \\[10pt]
        \begin{center}
            \onslide<1->{
                1. \includegraphics[scale=0.24]{img/slides/two_phases1.PNG}
            }
            \onslide<2->{  
                2. \includegraphics[scale=0.30]{img/slides/two_phases2_n.png}
            }
        \end{center}
        
        \begin{center}
            \onslide<3->{
                3. \includegraphics[scale=0.30]{img/slides/two_phases3_n.png}
            }
            \onslide<4->{
                4. \includegraphics[scale=0.24]{img/slides/two_phases4.PNG}
            }
            \onslide<5->{
                5. \includegraphics[scale=0.30]{img/slides/two_phases5.PNG}
            }
        \end{center}
        
        \onslide<6>{
            \item[$\bullet$] \textbf{Altre 6 varianti}
        }
    \end{itemize}
\end{frame}


\setbeamercovered{transparent=25}
\begin{frame}{Modello principale a due fasi}
    \begin{itemize}
        \only<1>{
            \item[$\bullet$] \textbf{Prima fase (primale)}
            % usare i box?
            \footnotesize
            \begin{alignat*}{3}
                &\!\min_{a, R_1^2, \xi_i} &\qquad& R_1^2 + C_1\sum_i \xi_i \, \\
                &\text{subject to}  &       & \norm{\Phi(x_i) - a}^2 \leq R_1^2 + \xi_i &\qquad& \forall i : \mu_i = 1 \,  \notag\\
                &                   &       & \xi_i \geq 0  &\qquad& \forall i: \mu_i = 1 \,  \notag
            \end{alignat*}
        }
        \only<2->{
            \item[$\bullet$] \textbf{Prima fase (duale)}
            % usare i box?
            \footnotesize
            \begin{alignat*}{5}
                &\!\max_{\alpha_i} &\qquad& \sum_i \alpha_i K(x_i, x_i) - \sum_{i, i^\prime} \alpha_i \alpha_{i^\prime} K(x_i, x_{i^\prime}) \,  \\ 
                &\text{subject to}      &      & \sum_i \alpha_i = 1                                 \,  \\
                &                       &      & 0 \leq \alpha_i \leq C_1  \qquad  \qquad \qquad \forall i : \mu_i = 1 \, 
            \end{alignat*}
        }
        \onslide<3->{
            \vspace{-15pt}
            \normalsize
            \item[$\bullet$] \textbf{Seconda fase}
            \footnotesize
            \begin{alignat*}{5}
                &\!\min_{R_0^2, \tau_j} &\qquad& - R_0^2 + C_0 \sum_j \tau_j \,  \\ 
                &\text{subject to}      &      & \norm{\Phi(x_j) - a^*}^2 \geq R_0^2 - \tau_j &\quad& \forall j : \mu_j = 0 \, \\
                &                       &      & \tau_j \geq 0                              &\quad& \forall j : \mu_j = 0 \, 
            \end{alignat*}
        }
        \onslide<4->{
            \vspace{-15pt}
            \normalsize
            \item[$\bullet$] \textbf{Predizioni}
            \small
            \begin{gather*}
                R^2(x) \leq R_1^{*2} \implies \text{Core}\\
                R^2(x) \geq R_0^{*2} \implies \text{Zona di esclusione}\\
                R_1^{*2} < R^2(x) < R_0^{*2} \implies \text{Shadow}
            \end{gather*}
        }
    \end{itemize}
\end{frame}

\setbeamercovered{invisible}
\begin{frame}{Come valutare uno shadowed set?}
    \begin{itemize}
        \item[$\bullet$] \textbf{Indici di validazione di clustering}
        \item[$\bullet$] \textbf{Metriche conservativa e non conservativa}
        \item[$\bullet$] \textbf{Metriche che valorizzano lo shadow}
    \end{itemize}
    \vspace{8pt}
    \centering
    \includegraphics[scale=0.40]{img/slides/sh_metrics2.png}
\end{frame}

\begin{frame}{Dataset “Iris”}
    \begin{itemize}
        \item[$\bullet$] \textbf{Il dataset}
        \begin{itemize}
            \item[$\circ$] 150 istanze di fiori, 4 attributi
            \item[$\circ$] Tre classi: “Iris-virginica”, “Iris-versicolor”, “Iris-setosa”
        \end{itemize}
        \item[$\bullet$] \textbf{Shadowed set di Iris-virginica e Iris-versicolor, a seguito di model selection}
    \end{itemize}
    
    \begin{center}
        \includegraphics[scale=0.40]{img/slides/iris_full_cropped.png}
    \end{center}
    
\end{frame}

\begin{frame}{Confronti sul dataset “Iris”}
    \begin{itemize}
        \item[$\bullet$] \textbf{Cross-validation annidata con 5 fold esterne e 5 fold interne}
        \item[$\bullet$] \textbf{Sono indicate media e deviazione standard dell'accuratezza per gli shadowed set di ciascuna classe}
    \end{itemize}

    \begin{table}
        \centering
        \resizebox{\textwidth}{!}{
            \begin{tabular}{lcccr}
                \toprule
                {} &  Iris-virginica &  Iris-versicolor &  Iris-setosa &     Tempo \\
                \midrule
                GurobiDueFasi          &                                        0.927 $(\pm 0.044)$                                         &                                         0.893 $(\pm 0.049)$                                          &                                     1.000 $(\pm 0.000)$                                      &  0:07:59 \\
                CPLEXDueFasi          &                                        0.927 $(\pm 0.044)$                                         &                                         0.893 $(\pm 0.049)$                                          &                                     1.000 $(\pm 0.000)$                                      &  0:10:39 \\
                GurobiSingolaFase       &                                        0.833 $(\pm 0.253)$                                         &                                         0.940 $(\pm 0.049)$                                          &                                     0.987 $(\pm 0.027)$                                      &  0:25:39 \\
                CPLEXSingolaFase        &                                        0.947 $(\pm 0.045)$                                         &                                         0.940 $(\pm 0.049)$                                          &                                     0.987 $(\pm 0.027)$                                      &  0:47:06 \\
                GurobiDueFasiSingolaC  &                                        0.927 $(\pm 0.039)$                                         &                                         0.907 $(\pm 0.065)$                                          &                                     0.993 $(\pm 0.013)$                                      &  0:01:43 \\
                GurobiDueCore &                                        0.907 $(\pm 0.093)$                                         &                                         0.840 $(\pm 0.053)$                                          &                                     0.973 $(\pm 0.039)$                                      &  0:18:26 \\
                GurobiDoppioVincolo &                                        0.947 $(\pm 0.040)$                                          &                                         0.933 $(\pm 0.047)$                                          &                                       1.000 $(\pm 0.000)$                                        &  0:51:52 \\
                WILF2018ShadowedLearn   &                                        0.853 $(\pm 0.045)$                                         &                                         0.893 $(\pm 0.065)$                                         &                                     1.000 $(\pm 0.000)$                                      &  0:42:14 \\
                \bottomrule
            \end{tabular}
        }
    \end{table}
    
\end{frame}

\begin{frame}{Insiemi non convessi (dataset sintetico)}
    \begin{itemize}
        \item[$\bullet$] \textbf{Shadowed Set-Based Rough-Fuzzy C-Means (SRFCM) (2011)
        }
        \vspace{7pt}
        \begin{center}
            \includegraphics[scale=0.35]{img/slides/srfcm_synth2.png}
        \end{center}
        \onslide<2->{
            \item[$\bullet$] \textbf{Nuovo algoritmo}
            \vspace{7pt}
            \begin{center}
                \includegraphics[scale=0.35]{img/slides/alg_synth_cropped.png}
            \end{center}
        } 
    \end{itemize}
\end{frame}

\begin{frame}{Dataset “Covid”}
    \begin{itemize}
        \item[$\bullet$] \textbf{301 istanze, 22 attributi}
        \begin{itemize}
            \item[$\circ$] Sintomi, comorbidità, valori del sangue, valori di saturazione, esami radiologici
        \end{itemize}
        
        \item[$\bullet$] \textbf{Etichette}
        \begin{itemize}
            \item[$\circ$] LABEL-2: classe 0 (\textbf{freq.\ 0.711}), classe 1 (freq.\ 0.289)
            \item[$\circ$] LABEL-3: classe 0 (freq.\ 0.186), classe 1 (\textbf{freq.\ 0.525}), classe 2 (freq.\ 0.289)
        \end{itemize}
        
        
        \item[$\bullet$] \textbf{Riduzione della dimensionalità}
        \begin{itemize}
            \item[$\circ$] PCA
            \item[$\circ$] Boruta
            \item[$\circ$] RFE
        \end{itemize}
        
    \end{itemize}
\end{frame}

\begin{frame}{Confronti sul dataset “Covid”}
    \begin{itemize}
        \item[$\bullet$] \textbf{Training e test su LABEL-2}
    \end{itemize}
    \vspace{-15pt}
    \begin{table}
        \centering
        \resizebox{\textwidth}{!}{
        \begin{tabular}{llllllllll}
            \toprule
            Riduz.\ dim.  & Accuratezza & AUC & Sensibilità &  Specificità & F1-score \\
            \midrule
            Nessuna  &                        0.754 $(\pm 0.037)$                       &          0.606 $(\pm 0.067)$ & 0.958 $(\pm 0.027)$                  &                  0.255 $(\pm 0.149)$ & 0.848 $(\pm 0.021)$ \\
            PCA  &                        0.758 $(\pm  0.037)$                       &          0.608 $(\pm   0.061)$ & 0.963 $(\pm 0.038)$                  &                  0.254 $(\pm 0.139)$ & 0.850 $(\pm 0.023)$  \\
            \textbf{Boruta}  &                       \textbf{0.777} $\bm{(\pm 0.016)}$                       &          \textbf{0.639} $\bm{(\pm 0.014)}$ &   \textbf{0.967} $\bm{(\pm 0.041)}$                  &                  \textbf{0.310} $\bm{(\pm 0.060)}$ & \textbf{0.860} $\bm{(\pm 0.013)}$ \\RFE  &  0.767 $(\pm 0.045)$                       &          0.625 $(\pm 0.067)$ & 0.963 $(\pm 0.041)$                  &                  0.288 $(\pm 0.135)$ & 0.855 $(\pm 0.028)$ \\\hline
            RF (2020) & 0.74 $(\pm 0.008)$ & 0.81 $(\pm 0.008)$ & 0.72 $(\pm 0.013)$ & 0.76 $(\pm 0.008)$ & 0.62 $(\pm 0.009)$ \\                              
            \bottomrule
        \end{tabular}
        }
    \end{table}

    
    \begin{itemize}
        \item[$\bullet$] \textbf{Training su LABEL-2 e test su LABEL-3}
    \end{itemize}
    \vspace{-15pt}
    \begin{table}
        \centering
        \resizebox{\textwidth}{!}{
            \begin{tabular}{lllllllllll}
                \toprule
                Riduz.\ dim.  & Accuratezza & AUC & Recall esclusione & Recall shadow & Recall core & F1-score \\
                \midrule
                Nessuna &                      0.528 $(\pm 0.049)$                      &                             0.507 $(\pm 0.010)$                             &                              0.039 $(\pm 0.048)$ & 0.993 $(\pm 0.013)$                           & 0.000 $(\pm 0.000)$                         &                      0.374 $(\pm 0.056)$                                         \\
                PCA  &                      0.525 $(\pm 0.046)$                      &                             0.500 $(\pm 0.000)$                             &                              0.000 $(\pm 0.000)$ & 1.000 $(\pm 0.000)$ &                      0.000 $(\pm 0.000)$                         &                      0.362 $(\pm 0.052)$  \\
                \textbf{Boruta}  &                      \textbf{0.552} $\bm{(\pm 0.116)}$                      &                             \textbf{0.669} $\bm{(\pm 0.091)}$                             &                              \textbf{0.754} $\bm{(\pm 0.141)}$ & \textbf{0.531} $\bm{(\pm 0.136)}$                           &  \textbf{0.451} $\bm{(\pm 0.181)}$                         &                      \textbf{0.546} $\bm{(\pm 0.109)}$                      \\
                RFE  &                      0.525 $(\pm 0.046)$                      &                             0.501 $(\pm 0.001)$                             &                              0.000 $(\pm 0.000)$ & 1.000 $(\pm 0.000)$ &                      0.000 $(\pm 0.000)$                          &                      0.363 $(\pm 0.054)$ \\                             
                \bottomrule
            \end{tabular}
        }
    \end{table}

\end{frame}

\begin{frame}{Conclusioni e sviluppi futuri}
    \begin{itemize}
        \item[$\bullet$] \textbf{Risultati e innovazioni}
        \begin{itemize}
            \item[$\circ$] Accuratezza superiore o comparabile a quella di altri algoritmi noti in letteratura
            \item[$\circ$] Capacità di lavorare con insiemi non convessi
        \end{itemize}
        \item[$\bullet$] \textbf{Sviluppi futuri}
        \begin{itemize}
            \item[$\circ$] Ideazione di nuove metriche
            \item[$\circ$] Combinazione di classificatori
            \item[$\circ$] Miglior scalabilità (TensorFlow)
        \end{itemize} 
    \end{itemize}
\end{frame}



\begingroup
\setbeamertemplate{footline}{}
\begin{frame}[standout]
    \Huge
    Grazie per l'attenzione
\end{frame}

\begin{frame}[noframenumbering]{Indici di validazione di clustering (dataset “Iris”)}
    \begin{itemize}
        \item[$\bullet$] \textbf{Modello principale a due fasi, dopo aver rimosso le istanze di Iris-setosa (senza PCA)}
    \end{itemize}
    \begin{table}
        \centering
        \resizebox{\textwidth}{!}{
    \begin{tabular}{lrrrrrrr}
        \toprule
        {} & \multicolumn{2}{c}{Iris-virginica} & \multicolumn{2}{c}{Iris-versicolor} \\  
        {} &  Cons. &  Non cons. &  Cons.  &  Non cons. &    SVC &  Default &                      Ottimo \\
        \midrule
        Silhouette                          &                                    0.23 &                                        0.18 &                                     0.24 &                                         0.27 &       0.39 &                  0.37 &  Max \\
        Calinski-Harabasz                  &                                   52.49 &                                       51.01 &                                    46.02 &                                        40.03 &     100.87 &                 86.77 &                                Max \\
        Davies-Bouldin                     &                                    1.77 &                                        2.20 &                                     2.64 &                                         1.29 &       0.89 &                  0.94 &  Min \\
        RMSSTD             &                                    0.42 &                                        0.42 &                                     0.43 &                                         0.50 &       0.42 &                  0.43 &                              Elbow \\
        R-Squared                                &                                    0.95 &                                        0.95 &                                     0.95 &                                         0.93 &       0.95 &                  0.95 &                              Elbow \\
        Modified Hubert $\Gamma$ &                                    3.34 &                                        3.29 &                                     3.19 &                                         2.19 &       3.19 &                  3.02 &                              Elbow \\
        Dunn                                &                                    0.07 &                                        0.05 &                                     0.06 &                                         0.04 &       0.05 &                  0.06 &                                Max \\
        Xie-Beni                           &                                   13.43 &                                       34.07 &                                    11.96 &                                        24.81 &      34.44 &                 14.83 &                                Min \\
        SD Validity                        &                                    2.11 &                                        2.50 &                                     3.33 &                                         1.71 &       1.06 &                  1.17 &                                Min \\
        S\_Dbw Validity                  &                                    1.69 &                                        1.63 &                                     1.68 &                                         2.67 &       2.06 &                  2.24 &                                Min \\
        \bottomrule
    \end{tabular}
        }
    \end{table}
\end{frame}

\begin{frame}[noframenumbering]{Combinazione di classificatori (dataset “Iris”)}
    \begin{itemize}
        \item[$\bullet$] \textbf{Due classificatori: uno addestrato su “Iris-virginica”, uno su “Iris-versicolor”}
        \item[$\bullet$] \textbf{Metodo di combinazione delle predizioni}
        \vspace{8pt}
        \begin{table}
            \centering
            \resizebox{0.6\textwidth}{!}{
                \begin{tabular}{llrrr}
                    \toprule
                    {} & {} & \multicolumn{3}{c}{Virginica} \\
                    {} & {} & Core & Shadow & Esclusione \\
                    \midrule
                    \multirow{3}*{Versicolor} & Core & Shadow & Versicolor & Versicolor \\
                                             & Shadow & Virginica & Shadow & Versicolor\\
                                           & Esclusione & Virginica & Virginica & Esclusione \\
                    \bottomrule
                \end{tabular}
            }
        \end{table}
        
        \vspace{8pt}
        \item[$\bullet$] \textbf{Combinazione di classificatori}
        \vspace{8pt}
        \begin{center}
            \includegraphics[scale=0.45]{img/slides/iris_compounded_cropped.png}
        \end{center}
        
    \end{itemize}
\end{frame}

\begin{frame}[noframenumbering]{Metriche che valorizzano lo shadow (dataset “Iris”)}
    \begin{itemize}
        \item[$\bullet$] \textbf{Accuratezza e precisione dello shadow sul dataset “Iris”}
    \end{itemize}
    \begin{table}
        \centering
        \resizebox{\textwidth}{!}{
        \begin{tabular}{lcccc}
            \toprule
            {} & \multicolumn{2}{c}{Accuratezza} & \multicolumn{2}{c}{Precisione} \\
            {} &  Iris-virginica &  Iris-versicolor &  Iris-virginica &  Iris-versicolor\\
            \midrule
            SVC            &                                           0.500 &                                            0.000 &                                            0.600 &                                             0.000 \\
            Dist.\ media           &                                           0.417 &                                            0.083 &                                            1.000 &                                             0.333 \\
            Dist.\ minima            &                                           0.333 &                                            0.333 &                                            0.200 &                                             0.333 \\
            Dist.\ media o minima    &                                           0.385 &                                            0.077 &                                            1.000 &                                             0.333 \\
            Frequenza           &                                           0.385 &                                            0.077 &                                            1.000 &                                             0.333 \\
            Entropia        &                                           0.208 &                                            0.125 &                                            1.000 &                                             1.000 \\
            Silhouette    &                                           0.500 &                                            0.250 &                                            0.400 &                                             0.333 \\
            Media              &                                           0.390 &                                            0.135 &                                            0.743 &                                             0.381 \\
            Unione          &                                           0.200 &                                            0.120 &                                            1.000 &                                             1.000 \\
            Intersezione 3 &                                           0.385 &                                            0.077 &                                            1.000 &                                             0.333 \\
            \bottomrule
        \end{tabular}
        }
    \end{table}

\end{frame}

\begin{frame}[noframenumbering]{Combinazione di classificatori (dataset “Covid”)}
    \begin{itemize}
        \item[$\bullet$] \textbf{Accuratezza dei singoli classificatori su LABEL-4}
        \vspace{8pt}
        \begin{table}
            \centering
            \resizebox{0.5\textwidth}{!}{
                \begin{tabular}{lrrrr}
                    \toprule
                    {}  & Classe 0 & Classe 1 & Classe 2 & Classe 3\\
                    \midrule
                    Accuratezza & 0.846 & 0.604 & 0.879  & 0.835\\
                    \bottomrule
                \end{tabular}
            }
        \end{table}
        \vspace{8pt}
        
        \item[$\bullet$] \textbf{Matrice di confusione della combinazione di classificatori}
        \vspace{8pt}
        \begin{table}
            \centering
            \resizebox{0.7\textwidth}{!}{
                \begin{tabular}{lrrrrr}
                    \toprule
                    {} &  Pred.\ 0 &  Pred.\ 1 &  Pred.\ 2 &  Pred.\ 3 &  Pred.\ Uncertain \\
                    \midrule
                    True 0         &       3 &       2 &       0 &       0 &              12 \\
                    True 1         &       1 &      36 &       0 &       0 &              11 \\
                    True 2         &       0 &       9 &       0 &       0 &               2 \\
                    True 3         &       0 &      15 &       0 &       0 &               0 \\
                    True Uncertain &       0 &       0 &       0 &       0 &               0 \\
                    \bottomrule
                \end{tabular}
            }
        \end{table}
        
        
        \vspace{8pt}
        \item[$\bullet$] \textbf{Performance della combinazione di classificatori}
        \vspace{8pt}
        \begin{table}
            \centering
            \resizebox{0.9\textwidth}{!}{
                \begin{tabular}{lrrrrrrrr}
                    \toprule
                    {} &          0 &          1 &     2 &     3 &  Incerti &  Accuratezza &  Macro avg &  Weighted avg \\
                    \midrule
                    Precisione &   0.750 &   0.581 &   0.0 &   0.0 &        0.0 &     0.429 &      0.266 &         0.446 \\
                    Recall    &   0.176 &   0.750 &   0.0 &   0.0 &        0.0 &     0.429 &      0.185 &         0.429 \\
                    F1-score  &   0.286 &   0.655 &   0.0 &   0.0 &        0.0 &     0.429 &      0.188 &         0.399 \\
                    Supporto   &  17.000 &  48.000 &  11.0 &  15.0 &        0.0 &     0.429 &     91.000 &        91.000 \\
                    \bottomrule
                \end{tabular}
            }
        \end{table}
        
    \end{itemize}
\end{frame}

\endgroup

\end{document}