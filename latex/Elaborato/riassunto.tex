\documentclass[a4paper,12pt]{report}
\usepackage[a4paper]{geometry}
%\usepackage[a-1a]{pdfx}
\usepackage{amssymb,amsmath,amsthm}
\usepackage{graphicx}
\usepackage{url}
\usepackage{hyperref}
\usepackage{xmpincl}
\usepackage{epsfig}
\usepackage[italian]{babel}
\usepackage{setspace}
\usepackage{tesi}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{physics}
\usepackage[makeroom]{cancel}
\usepackage{subfig}
\usepackage[labelfont=bf, format=hang]{caption}
\usepackage{rotating}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{bm}
\usepackage{quoting}
\quotingsetup{font=small}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\pdfminorversion=4

\newenvironment{tightcenter}{%
  \setlength\topsep{0pt}
  \setlength\parskip{0pt}
  \begin{center}
}{%
  \end{center}
}


\begin{document}
\thispagestyle{plain}
            
\Large
\begin{tightcenter}
    \textbf{\textsc{Induzione di shadowed set tramite tecniche di one-class clustering}}\\
    \vspace*{15pt}%
    \large
    \textsc{Gabriele Cerizza -- Matricola 909272}
\end{tightcenter}

    
\vspace{0.5cm}
\LARGE

\vspace{1.5cm}
\normalsize
Il pensiero umano è caratterizzato dall'uso di concetti dai confini incerti e la stessa realtà spesso si sottrae a qualificazioni manichee. Se ne trae una conferma dal linguaggio naturale, il quale è ricco di espressioni indefinite che cionondimeno l'uomo comprende intuitivamente. Nessuno si inganna, ad esempio, sul significato della parola “mucchio”, eppure sui confini di questa stessa parola hanno ragionato molte delle più raffinate menti umane, nel tentativo di risolvere il “paradosso del sorite” (dal greco \textit{sorós}, che significa appunto “mucchio”). Se da un mucchio di sabbia togliamo un granello, avremo certamente ancora un mucchio di sabbia. Applicando questo principio per ogni granello di sabbia, arriveremo a considerare anche il singolo granello come un mucchio: in ciò consiste il paradosso del sorite. Il problema è quello di stabilire una soglia per il numero di granelli di sabbia che costituiscono un mucchio. 
\\\indent L'incertezza dei concetti e le ambiguità della realtà possono rappresentare non solo un limite, ma anche una risorsa. Nell'esame di dati clinici, finalizzato ad esempio a individuare i soggetti affetti da una patologia, può essere utile isolare quei casi che, per la loro difficile diagnosi, è opportuno sottoporre a più attento scrutinio. L'analisi di questi casi “difficili” può aiutare, in un secondo momento, a far progredire la disciplina scientifica che si occupa della patologia considerata. Ancora, ne “L'arte di ottenere ragione” Arthur Schopenhauer è prodigo di suggerimenti su come sfruttare ambiguità di significato e vaghezza dei termini per prevalere sul proprio avversario. Un altro esempio viene dalla mitologia norrena, dove il dio Loki, dopo aver perso una scommessa con in palio la propria testa, riesce a scampare all'esecuzione per l'impossibilità di stabilire esattamente dove inizi la testa e dove finisca il collo.
\\\indent Per trattare concetti vaghi e operare su casi ambigui la scienza necessita di strumenti che permettano di manipolarli matematicamente. Uno di questi strumenti è lo \textit{shadowed set}, teorizzato da Witold Pedrycz in un articolo del 1998 intitolato “\textit{Shadowed Sets: Representing and Processing Fuzzy Sets}”. Si tratta di una tipologia di insieme caratterizzata da tre regioni: la regione degli elementi che sicuramente appartengono all'insieme (\textit{core}), la regione degli elementi che sicuramente non appartengono all'insieme (zona di esclusione) e la regione degli elementi incerti, per i quali non è possibile stabilire l'appartenenza o meno all'insieme (\textit{shadow}). 
% Applicando questo approccio al paradosso del sorite, ad esempio, potremmo stabilire due soglie per il numero di granelli di sabbia: una soglia al di sotto della quale sicuramente non si è in presenza di un mucchio (zona di esclusione) e una soglia al di sopra della quale sicuramente si è in presenza di un mucchio (\textit{core}). I casi compresi tra la prima e la seconda soglia sono incerti e per essi non è possibile stabilire se si abbia o meno un mucchio (\textit{shadow}).
\\\indent Il lavoro descritto nel presente elaborato è il frutto di un tirocinio interno svolto nell'ambito del Dipartimento di Informatica dell'Università degli Studi di Milano. Oggetto del lavoro è lo studio e la sperimentazione di un nuovo algoritmo di induzione di \textit{shadowed set} a partire dai dati. Attraverso lo \textit{shadowed set} indotto dall'algoritmo è possibile operare su insiemi di dati caratterizzati dalla presenza di casi difficili da classificare. Ad esempio, in premessa si è fatto cenno all'utilità di identificare i casi in cui risuta difficile stabilire se un soggetto sia o meno affetto da una particolare patologia. Ebbene, l'algoritmo, una volta appreso lo \textit{shadowed set} corrispondente all'insieme delle persone affette da tale patologia, è in grado di stabilire se un particolare individuo sia effettivamente affetto dalla patologia (\textit{core}), ovvero non sia affetto dalla patologia (zona di esclusione), ovvero ancora sia incerto se sia affetto dalla patologia (\textit{shadow}). I casi incerti possono poi essere analizzati da esperti o attraverso altri algoritmi per approfondire la conoscenza della malattia.      
\\\indent L'elaborato illustra i risultati teorici e sperimentali del nuovo algoritmo di induzione di \textit{shadowed set}. Più in dettaglio, nel Capitolo 1 viene anzitutto fornito un quadro dello stato della tecnica. Dopo una cursoria esposizione dei principali concetti in materia di insiemi \textit{fuzzy}, di cui gli \textit{shadowed set} costituiscono una derivazione, e in materia di \textit{rough set}, che rappresentano una tipologia di insieme affine agli \textit{shadowed set}, viene approfondita la teoria degli \textit{shadowed set} e vengono esaminati alcuni degli algoritmi per la loro induzione noti in letteratura. 
\\\indent Nel Capitolo 2 viene presentato il nuovo algoritmo di induzione di \textit{shadowed set} oggetto del presente lavoro. Tale algoritmo poggia sull'impianto teorico delle tecniche di Support Vector Clustering. Si propongono diverse varianti dell'algoritmo, che vengono esaminate sotto il profilo teorico-matematico.
\\\indent Nel Capitolo 3 vengono discusse le metriche utilizzate per valutare il comportamento dell'algoritmo. Particolari sforzi sono stati dedicati all'ideazione di metriche volte a valorizzare lo \textit{shadow} dello \textit{shadowed set}.
\\\indent Nel Capitolo 4 vengono descritti i risultati di diversi esperimenti condotti sull'algoritmo, per la cui implementazione ci si è avvalsi di librerie come Gurobi. Oltre ad alcuni \textit{dataset} classici per problemi di classificazione e \textit{clustering}, come il \textit{dataset} “Iris”, e oltre a un \textit{dataset} sintetico creato appositamente per scopi di indagine, l'algoritmo è stato valutato su due \textit{dataset} formati da dati empirici: il primo in materia di sinistri stradali, il secondo in materia di persone affette dal coronavirus SARS-CoV-2.      
\\\indent Concludono l'elaborato alcune riflessioni sui risultati ottenuti dall'algoritmo e sugli elementi che ne sanciscono il pregio rispetto alle altre tecniche di induzione di \textit{shadowed set} note in letteratura.

\end{document}