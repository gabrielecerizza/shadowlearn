import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from scipy.spatial.distance import euclidean
from sklearn.decomposition import PCA
from sklearn.model_selection import StratifiedShuffleSplit

from src.datasets import load_iris


def distance(center, x):
    return euclidean(center, x)


def find_centers(X, mu, m, c):
    v = np.zeros(shape=(c, X.shape[1]))
    for i in range(c):
        for j in range(X.shape[1]):
            sum1 = 0
            for k in range(X.shape[0]):
                sum1 += (mu[i][k] ** m) * X[k][j]

            sum2 = 0
            for k in range(X.shape[0]):
                sum2 += mu[i][k] ** m

            v[i][j] = sum1 / sum2
    return np.array(v)


def update_mu(X, m, c, centers):
    distances = np.zeros(shape=(c, X.shape[0]))
    for i in range(c):
        for k in range(len(X)):
            distances[i][k] = distance(centers[i], X[k])
    distances = np.array(distances)

    '''
    I, I_tilde = [], []
    for k in range(len(X)):
        d_k = distances[:, k]
        I_k, = np.where(d_k == 0)
        I_k = set(I_k)
        I_tilde_k = set(range(c)) - I_k
        I[k] = I_k
        I_tilde[k] = I_tilde_k

    new_mu = []
    for k in range(len(X)):
        if len(I[k]) == 0:
            for i in range(c):
                new_mu[i][k] = 1 / sum([(distances[i][k] / distances[j][k]) ** (2 / (m - 1)) for j in range(c)])
        else:
            for i in I[k]:
                new_mu[i][k] = 0
            for i in I_tilde[k]:
    '''
    new_mu = np.zeros(shape=(c, X.shape[0]))
    for i in range(c):
        for k in range(len(X)):
            new_mu[i][k] = 1 / sum([(distances[i][k] / distances[j][k]) ** (2 / (m - 1)) for j in range(c)])

    return new_mu


def fuzzy_c_means(X, c, m, epsilon, n_iter):
    mu = np.random.rand(c, X.shape[0])
    mu = mu / np.sum(mu, axis=0, keepdims=1)

    for num in range(n_iter):
        print(num)
        centers = find_centers(X, mu, m, c)
        print(centers)
        new_mu = update_mu(X, m, c, centers)
        print(new_mu)
        if np.linalg.norm(mu - new_mu) < epsilon:
            break
        mu = new_mu

    return new_mu, centers


X, y, data_labels, target, _ = load_iris(d='all', target='Iris-virginica')
splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=42)
for train_index, test_index in splitter.split(X, data_labels):
    X_train = X[train_index]
    y_train = y[train_index]
    label_train = data_labels[train_index]
    X_test = X[test_index]
    y_test = y[test_index]
    label_test = data_labels[test_index]
    pca = PCA(n_components=2)
    pca.fit(X_train, y_train)
    X_train = pca.transform(X_train)
    X_test = pca.transform(X_test)

ncenters = 2
u, cntr = fuzzy_c_means(X_train, 2, 2, 0.005, 1000)

colors = ['b', 'orange', 'g', 'r', 'c', 'm', 'y', 'k', 'Brown', 'ForestGreen']
fig1, ax = plt.subplots(1, 1, figsize=(8, 8))

xpts = np.array(X_train)[:,0]
ypts = np.array(X_train)[:,1]
cluster_membership = np.argmax(u, axis=0)
for j in range(ncenters):
    ax.plot(xpts[cluster_membership == j],
            ypts[cluster_membership == j], '.', color=colors[j])

# Mark the center of each fuzzy cluster
for pt in cntr:
    ax.plot(pt[0], pt[1], 'rs')

ax.set_title('Centers = {0}'.format(ncenters))
plt.show()
