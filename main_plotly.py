from src.shadow_learn import ShadowedSetEstimator
from src.utils import get_plot_dfs
from src.datasets import load_iris
from src.plot import plotly_shadowed_set

nrows = 2
ncols = 2
title = 'test'
labels = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
label_colors = ['cornflowerblue', 'mediumseagreen', 'crimson']
colorscale_dict = dict(
    virginica=[[0, 'white'], [0.5, 'pink'], [1.0, 'palevioletred']],
    versicolor=[[0, 'white'], [0.5, 'aquamarine'], [1.0, 'lightseagreen']]
)
colorscale_ls = ['virginica', 'virginica', 'versicolor', 'versicolor']
colorscale_show_ls = [True, False, True, False]

dataset_loader = lambda: load_iris(d=2, target='Iris-virginica')
virg_cons = ShadowedSetEstimator(solver='gurobi-two-phases', C0=0.1, C1=0.316228, sigma=1.36)
virg_cons_df_test, virg_cons_df_train, virg_cons_coord_df, virg_cons_score = get_plot_dfs(dataset_loader, virg_cons, 'conservative')

dataset_loader = lambda: load_iris(d=2, target='Iris-virginica')
virg_cons2 = ShadowedSetEstimator(solver='gurobi-two-phases', C0=0.1, C1=0.5, sigma=1.8)
virg_cons_df_test2, virg_cons_df_train2, virg_cons_coord_df2, virg_cons_score2 = get_plot_dfs(dataset_loader, virg_cons2, 'conservative')

dataset_loader = lambda: load_iris(d=2, target='Iris-versicolor')
vers_cons = ShadowedSetEstimator(solver='gurobi-two-phases', C0=0.1, C1=0.316228, sigma=1.36)
vers_cons_df_test, vers_cons_df_train, vers_cons_coord_df, vers_cons_score = get_plot_dfs(dataset_loader, vers_cons, 'conservative')

dataset_loader = lambda: load_iris(d=2, target='Iris-versicolor')
vers_cons2 = ShadowedSetEstimator(solver='gurobi-two-phases', C0=0.1, C1=0.5, sigma=1.8)
vers_cons_df_test2, vers_cons_df_train2, vers_cons_coord_df2, vers_cons_score2 = get_plot_dfs(dataset_loader, vers_cons2, 'conservative')

estimators = [virg_cons, virg_cons2, vers_cons, vers_cons2]
train_dfs = [virg_cons_df_train, virg_cons_df_train2, vers_cons_df_train, vers_cons_df_train2]
test_dfs = [virg_cons_df_test, virg_cons_df_test2, vers_cons_df_test, vers_cons_df_test2]
coord_dfs = [virg_cons_coord_df, virg_cons_coord_df2, vers_cons_coord_df, vers_cons_coord_df2]
scores = [virg_cons_score, virg_cons_score2, vers_cons_score, vers_cons_score2]

plotly_shadowed_set(nrows, ncols, estimators, train_dfs, test_dfs, coord_dfs, scores, title, labels,
                    label_colors, colorscale_ls, colorscale_show_ls, colorscale_dict)