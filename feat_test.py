import numpy as np
import pandas as pd

from boruta import BorutaPy
from sklearn.ensemble import RandomForestClassifier

from src.datasets import load_run_over_dataset

random_state = 42

dataset_loader = load_run_over_dataset
X, y, data_labels, target = dataset_loader(target=0, template='macro-districts', without=['DATA'])
column_names = dataset_loader.column_names_
rf = RandomForestClassifier(n_jobs=2, class_weight='balanced', max_depth=5, random_state=random_state)
feat_selector = BorutaPy(rf, n_estimators='auto', verbose=2, perc=70, random_state=random_state)
feat_selector.fit(X, y)

X_transformed = feat_selector.transform(X)
print(X_transformed)