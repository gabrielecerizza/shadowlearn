import pickle

import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.decomposition import PCA

from src.kernel import GaussianKernel
from src.datasets import load_iris
from src.shadow_learn import ShadowedSetEstimator, CompoundedShadowedSetEstimator
from src.plot import plot_score_surface

if __name__ == '__main__':
    print('PyCharm')

    dumps = pickle.dumps(ShadowedSetEstimator(solver='gurobi-single-phase'))



    #from sklearn.utils.estimator_checks import check_estimator

    #check_estimator(ShadowedSetEstimator(solver='gurobi-single-phase'))

    """
    source = 'https://archive.ics.uci.edu/ml/' \
             'machine-learning-databases/iris/iris.data'
    iris_df = pd.read_csv(source, names=["sepal length", "sepal width", "petal length", "petal width", "class"])

    # iris_df['class'] = iris_df['class'].apply(lambda x: 1 if x == 'Iris-virginica' else 0)

    iris_values = iris_df.iloc[:, 0:4].values
    iris_labels = iris_df.iloc[:, 4].values

    pca_2d = PCA(n_components=2)
    iris_values_2d = pca_2d.fit_transform(iris_values)
    X = iris_values_2d
    y = iris_df['class'].apply(lambda x: 1 if x == 'Iris-virginica' else 0)

    models = (ShadowedSetEstimator(k=GaussianKernel(0.5)),
              ShadowedSetEstimator(k=GaussianKernel(0.25)),
              ShadowedSetEstimator(k=GaussianKernel(0.17)))
    models = (clf.fit(X, y) for clf in models)

    # title for the plots
    titles = ('Gaussian Kernel with sigma=0.5',
              'Gaussian Kernel with sigma=0.25',
              'Gaussian Kernel with sigma=0.17')

    X0, X1 = X[:, 0], X[:, 1]
    xx, yy = make_meshgrid(X0, X1)

    le = preprocessing.LabelEncoder()
    y_l = le.fit_transform(iris_labels)

    # Set-up 2x2 grid for plotting.
    fig, sub = plt.subplots(nrows=1, ncols=3, sharex=True, sharey=True, figsize=(25, 5))
    fig.suptitle('Iris-virginica Shadowed Set Models', fontsize=16)
    # plt.subplots_adjust(wspace=0.1, hspace=0.1)

    for i, clf, title, ax in zip(range(len(titles)), models, titles, sub.flatten()):
        proxy = plot_contours(ax, clf, xx, yy, alpha=0.8)
        for lab, col in zip(('Iris-setosa', 'Iris-versicolor', 'Iris-virginica'),
                            ('cornflowerblue', 'mediumseagreen', 'crimson')):
            ax.scatter(iris_values_2d[iris_labels == lab, 0],
                       iris_values_2d[iris_labels == lab, 1],
                       label=lab,
                       c=col, s=20, edgecolors='k')
        # ax.set_xlim(xx.min(), xx.max())
        # ax.set_ylim(yy.min(), yy.max())
        # ax.set_xlabel('Sepal length')
        # ax.set_ylabel('Sepal width')
        # ax.set_xticks(())
        # ax.set_yticks(())
        ax.set_title(title)

        if i == (len(titles) - 1):
            handles, labels = ax.get_legend_handles_labels()
            fig.legend(handles + proxy[1:],  # The line objects
                       labels + ["uncertain", "core"],  # The labels for each line
                       loc="upper left",  # Position of legend
                       borderaxespad=4,  # Small spacing around legend box
                       title="Legend"  # Title for the legend
                       )
            plt.subplots_adjust(top=0.8, wspace=0.1, hspace=0.1)


    # must disable show plot in tool window in PyCharm: it's bugged
    plt.show()
    """
    """

    X, y, iris_labels = load_iris(d=2)
    sse = ShadowedSetEstimator(solver='tensorflow-two-phases', sigma=0.25, C1=0.1)

    # models = [ShadowedSetEstimator(sigma=0.17, C0=0.3, C1=1)]

    estimator = sse.fit(X, y)

    labels = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
    label_colors = ['cornflowerblue', 'mediumseagreen', 'crimson']

    suptitle = 'Iris-virginica Shadowed Set Models'
    contourf_levels = [-2, -1, 0, 1]
    contourf_colors = ['white', 'pink', 'palevioletred', 'red']
    contour_levels = [-1, 0]

    internal_plot_shadowed_set(X, iris_labels, labels, label_colors, estimator,
                               suptitle, contourf_levels, contourf_colors, contour_levels)
    """